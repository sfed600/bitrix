<?php
namespace Webtu\ExpansionSite;

use \Bitrix\Main;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Webtu\ExpansionSite\Handler;

Loc::loadMessages(__FILE__);


class MetaSeo
{

    public static function setSeoParams($page)
    {
        global $APPLICATION;
        $arOptions = Handler::getOptions();
        $GLOBALS['SEO_DATA'] = array();

        if (
            strlen($page) <= 0
            && $arOptions["META_SEO_FILTER"]["IBLOCK_ID"] <= 0
            && strlen($arOptions["META_SEO_FILTER"]["PAGE"]) <= 0
        )
        {
            return false;
        }

        $explode_page = explode('/', $page);

        $regular_str = '/^\/.*\/filter\/(.*)\/apply\//';

        preg_match($regular_str, $page, $matches);

        if ($explode_page[1] == $arOptions["META_SEO_FILTER"]["PAGE"]) {
            $page  = str_replace('index.php', '', $page);

            $GLOBALS['SEO_DATA'] = self::findSeoData(
                $page,
                $arOptions["META_SEO_FILTER"]["IBLOCK_ID"],
                $_SESSION["CITY_INFO"]["GEOIP"]["NEAREST_DOT"]["ID"]
            );

            if ( strlen($matches[1]) > 0 )
            {
                $GLOBALS['SEO_DATA']['SMART_FILTER_PATH'] = $matches[1];
            }

            if (
                strlen($explode_page[2]) > 0
                && $explode_page[3] == 'index.php'
                && $arOptions["QUEST"]["CATALOG_IBLOCK_ID"] > 0
            )
            {
                $el = new \CIBlockSection ();

                $arFilter = Array(
                    "IBLOCK_ID" => $arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
                    "ACTIVE" => "Y",
                    "CODE" => $explode_page[2],
                );

                $arSelect = Array();

                $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                $url = "";

                while ($arSection = $res->Fetch())
                {
                    if (
                        $arSection["ID"] > 0
                        && $arSection["CODE"] == $explode_page[2]
                    )
                    {
                        $url = "/".$arOptions["META_SEO_FILTER"]["PAGE"]."/filter/quest_genre-is-".$arSection["CODE"]."/apply/";
                    }

                }

                if (strlen($url) > 0)
                {
                    http_response_code(301);
                    header('Location: '.$url );
                    exit;
                }
            }
        }

        if (isset($explode_page[1], $explode_page[2]) && $explode_page[1] === 'personal' && $explode_page[2] === 'user') {
            $page  = str_replace('index.php', '', $page);

            $GLOBALS['SEO_DATA'] = self::findSeoData(
                $page,
                $arOptions["META_SEO_FILTER"]["IBLOCK_ID"],
                $_SESSION["CITY_INFO"]["GEOIP"]["NEAREST_DOT"]["ID"]
            );
        }

        if (strlen($GLOBALS['SEO_DATA']['META_TITLE']) > 0)
        {
            $APPLICATION->SetTitle($GLOBALS['SEO_DATA']['META_TITLE']);
        }

        if (strlen($GLOBALS['SEO_DATA']['META_KEYWORDS']) > 0)
        {
            $APPLICATION->SetPageProperty("keywords", $GLOBALS['SEO_DATA']['META_KEYWORDS']);
        }

        if (strlen($GLOBALS['SEO_DATA']['META_DESCRIPTION']) > 0)
        {
            $APPLICATION->SetPageProperty("description", $GLOBALS['SEO_DATA']['META_DESCRIPTION']);
        }

    }

    /**
     * Find seo data by page,iblock id, city id
     *
     * @param $page
     * @param $iblockId
     * @param $cityId
     *
     * @return array
     */
    protected static function findSeoData($page, $iblockId, $cityId)
    {
        $arFilter = [
            "IBLOCK_ID" => $iblockId,
            "ACTIVE" => "Y",
            "=PROPERTY_ULR_PAGE" => $page,
            "=PROPERTY_CITY" => $cityId,
        ];

        $arSelect = [
            "ID",
            "IBLOCK_ID",
            "NAME",
            "PROPERTY_META_TITLE",
            "PROPERTY_META_H1",
            "PROPERTY_H2",
            "PROPERTY_META_KEYWORDS",
            "PROPERTY_META_DESCRIPTION",
            "PREVIEW_TEXT",
            "PREVIEW_PICTURE",

        ];

        $el = new \CIBlockElement();

        $res = $el->GetList([], $arFilter, false, false, $arSelect);

        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();

            if ($arFields["ID"] > 0) {
                return [
                    "ID" => $arFields["ID"],
                    "NAME" => $arFields["NAME"],
                    "PREVIEW_PICTURE" => $arFields["PREVIEW_PICTURE"],
                    "META_TITLE" => $arFields["PROPERTY_META_TITLE_VALUE"],
                    "META_H1" => $arFields["PROPERTY_META_H1_VALUE"],
                    "H2" => $arFields["PROPERTY_H2_VALUE"],
                    "META_KEYWORDS" => $arFields["PROPERTY_META_KEYWORDS_VALUE"],
                    "META_DESCRIPTION" => $arFields["PROPERTY_META_DESCRIPTION_VALUE"],
                    "PREVIEW_TEXT" => $arFields["PREVIEW_TEXT"],
                    "URL" => $page,
                ];
            }
        }

        return [];
    }
}