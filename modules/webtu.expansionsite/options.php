<?php
use Bitrix\Main\Localization\Loc;
use Bitrix\Main\Config\Option;
use \Webtu\ExpansionSite\Handler;

#объявляем имя переменной именной в таком написании $module_id
#обязательно, иначе права доступа не работают!
$module_id = 'webtu.expansionsite';

#подключаем языковые константы
Loc::loadMessages($_SERVER["DOCUMENT_ROOT"].BX_ROOT."/modules/main/options.php");
Loc::loadMessages(__FILE__);

#Проверяем есть ли доступ к модулю
if ($APPLICATION->GetGroupRight($module_id)<"S"){
    #Метод вызывает форму авторизации
    $APPLICATION->AuthForm(Loc::getMessage("ACCESS_DENIED"));
}

#Подключаем наш модуль
\Bitrix\Main\Loader::includeModule($module_id);

#Получаем данные отправленные пользователем с формы
$request = \Bitrix\Main\HttpApplication::getInstance()->getContext()->getRequest();

#Получить все настройки модуля
$arParams = Handler::getOptions();

#Пользовательски свойтсва
global $USER_FIELD_MANAGER;
$arUserFields = $USER_FIELD_MANAGER->GetUserFields("USER","", LANGUAGE_ID);

if($arUserFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]])
{
    $typeUser = $arUserFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]];
    $typeUser["USER_TYPE"]["CLASS_NAME"] = "\\".$typeUser["USER_TYPE"]["CLASS_NAME"];

    if (is_callable(array($typeUser["USER_TYPE"]["CLASS_NAME"], 'getlist')))
    {
        $rsEnum = call_user_func_array(
            array($typeUser["USER_TYPE"]["CLASS_NAME"], "getlist"),
            array($typeUser,)
        );

        while($arEnum = $rsEnum->GetNext())
        {
            $arTypeUser[$arEnum["ID"]] = $arEnum["VALUE"];
        }
    }
}

if (\Bitrix\Main\Loader::includeModule('sale'))
{
    $paymentCollection = Bitrix\Sale\PaySystem\Manager::getList();

    $arPayment = array();

    foreach ($paymentCollection as $payment)
    {
        $arPayment[$payment["ID"]] = $payment["NAME"]. ' ('.$payment["PSA_NAME"].')';
    }
}

$arUsersAdmins = array();

$arUsers = Bitrix\Main\UserGroupTable::getList(
    array(
        'group'=>array('USER_ID'),
        'select' => array(
            'USER_ID',
            'USER_NAME' => 'USER.NAME',
            'USER_LAST_NAME' => 'USER.LAST_NAME',
            'USER_ACTIVE' => 'USER.ACTIVE'
        ),
        'filter' => array(
            'GROUP_ID' => array(9),
        )

    )
)->fetchAll();

foreach ($arUsers as $arItem)
{
    if ( $arItem["USER_ACTIVE"] == "Y" )
    {
        $arUsersAdmins[$arItem["USER_ID"]] = $arItem["USER_NAME"].' '.$arItem["USER_LAST_NAME"];
    }

}

#Описание опций
$aTabs = array(
    array(
        'DIV' => 'edit1',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB1'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB1_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB1_TITLE_HEADER_1'),
            array('tab1_geoip_data_source_ip', Loc::getMessage('WEBTU_ES_TAB1_DATA_SOURCE_IP'),'LOCAL',array('selectbox',array("LOCAL"=>Loc::getMessage('WEBTU_ES_TAB1_DATA_SOURCE_LOCAL'),"CURL"=>Loc::getMessage('WEBTU_ES_TAB1_DATA_SOURCE_CURL'))), '', ' *1.1'),
            array('tab1_geoip_city_default', Loc::getMessage('WEBTU_ES_TAB2_CITY_DEFAULT'),'',array('text', 30),'',' *1.2'),
            array('tab1_geoip_nearest_dot', Loc::getMessage('WEBTU_ES_TAB1_NEAREST_DOT'),'',array('checkbox', ""),'',' *1.3'),
            array('tab1_geoip_iblock_id', Loc::getMessage('WEBTU_ES_TAB1_IBLOCK_ID'),'',array('text', 30),'',' *1.4'),
            array('tab1_geoip_propperty_map_code', Loc::getMessage('WEBTU_ES_TAB1_PROPERTY_MAP_CODE'),'',array('text', 30),'',' *1.5'),

            array('note'=>Loc::getMessage('WEBTU_ES_TAB1_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit2',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB2'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB2_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB2_TITLE_HEADER_1'),
            array('tab2_quest_catalog_iblock_id', Loc::getMessage('WEBTU_ES_TAB2_CATALOG_IBLOCK_ID'),'',array('text', 30),'',' *2.1'),
            array('tab2_quest_offers_iblock_id', Loc::getMessage('WEBTU_ES_TAB2_OFFERS_IBLOCK_ID'),'',array('text', 30),'',' *2.2'),
            array('tab2_quest_count_weeks', Loc::getMessage('WEBTU_ES_TAB2_COUNT_WEEKS_TO_CREATE_BOOKINGS'),'',array('text', 30),'',' *2.3'),
            array('tab2_quest_status_active_value', Loc::getMessage('WEBTU_ES_TAB2_STATUS_ACTIVE_VALUE'),'',array('text', 30),'',' *2.4'),
            array('tab2_quest_status_disabled_value', Loc::getMessage('WEBTU_ES_TAB2_STATUS_DISABLED_VALUE'),'',array('text', 30),'',' *2.5'),
            array('tab2_quest_status_draft_value', Loc::getMessage('WEBTU_ES_TAB2_STATUS_DRAFT_VALUE'),'',array('text', 30),'',' *2.6'),
            array('tab2_quest_integration_system_mirkvestov_value', Loc::getMessage('WEBTU_ES_TAB2_INTEGRATION_SYSTEM_MIRKVESTOV_VALUE'),'',array('text', 30),'',' *2.7'),
            array('tab2_quest_integration_system_questcompass_value', Loc::getMessage('WEBTU_ES_TAB2_INTEGRATION_SYSTEM_QUESTCOMPASS_VALUE'),'',array('text', 30),'',' *2.8'),
            array('tab2_promo_iblock_id', Loc::getMessage('WEBTU_ES_TAB2_PROMO_IBLOCK_ID'),'',array('text', 30),'',' *2.9'),
            array('tab2_search_team_button', Loc::getMessage('WEBTU_ES_TAB2_SEARCH_TEAM_BUTTON'),'',array('checkbox'),'',' *2.10'),
            array('tab2_certificate_iblock_id', Loc::getMessage('WEBTU_ES_TAB2_CERTIFICATE_IBLOCK_ID'),'',array('text', 30),'',' *2.11'),
            array('tab2_gift_link', Loc::getMessage('WEBTU_ES_TAB2_GIFT_LINK'),'',array('checkbox'),'',' *2.12'),
            Loc::getMessage('WEBTU_ES_TAB2_TITLE_HEADER_2'),
            array('tab2_team_iblock_id', Loc::getMessage('WEBTU_ES_TAB2_TEAM_IBLOCK_ID'),'',array('text', 30),'',' *2.12'),
            array('tab2_team_novice', Loc::getMessage('WEBTU_ES_TAB2_TEAM_NOVICE'),'',array('text', 30),'',' *2.13'),
            array('tab2_team_specialist', Loc::getMessage('WEBTU_ES_TAB2_TEAM_SPECIALIST'),'',array('text', 30),'',' *2.14'),
            array('tab2_team_expert', Loc::getMessage('WEBTU_ES_TAB2_TEAM_EXPERT'),'',array('text', 30),'',' *2.15'),
            array('tab2_part_iblock_id', Loc::getMessage('WEBTU_ES_TAB2_PART_IBLOCK_ID'),'',array('text', 30),'',' *2.16'),
            array('tab2_critique_iblock_id', Loc::getMessage('WEBTU_ES_TAB2_CRITIQUE_IBLOCK_ID'),'',array('text', 30),'',' *2.17'),
            array('tab2_review_iblock_id', Loc::getMessage('WEBTU_ES_TAB2_REVIEW_IBLOCK_ID'),'',array('text', 30),'',' *2.18'),
            array('tab2_review_moderation', Loc::getMessage('WEBTU_ES_TAB2_REVIEW_MODERATION'),'',array('checkbox'),'',' *2.19'),
            array('tab2_team_max_count_members', Loc::getMessage('WEBTU_ES_TAB2_TEAM_MAX_COUNT_MEMBERS'),'',array('text', 30),'',' *2.20'),
            array('tab2_team_max_count', Loc::getMessage('WEBTU_ES_TAB2_TEAM_MAX_COUNT'),'',array('text', 30),'',' *2.21'),


            array('note'=>Loc::getMessage('WEBTU_ES_TAB2_NOTE_2')),
        )
    ),
    array(
        'DIV' => 'edit3',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB3'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB3_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB3_TITLE_HEADER_1'),
            array('tab3_users_access_1', Loc::getMessage('WEBTU_ES_TAB3_USERS_ACCESS_1'),'4',array('selectbox',$arTypeUser), '', ' *3.1'),
            array('tab3_users_access_2', Loc::getMessage('WEBTU_ES_TAB3_USERS_ACCESS_2'),'5',array('selectbox',$arTypeUser), '', ' *3.2'),
            Loc::getMessage('WEBTU_ES_TAB3_TITLE_HEADER_2'),
            array('tab3_users_password', Loc::getMessage('WEBTU_ES_TAB3_USERS_PASSWORD'),'',array('text', 30), '', ' *3.3'),
            Loc::getMessage('WEBTU_ES_TAB3_TITLE_HEADER_3'),
            array('tab3_admin_message', Loc::getMessage('WEBTU_ES_TAB3_ADMIN_MESSAGE'),'',array('selectbox', $arUsersAdmins), '', ' *3.4'),
            Loc::getMessage('WEBTU_ES_TAB3_TITLE_HEADER_4'),
            array('tab3_deferred_payment_iblock_id', Loc::getMessage('WEBTU_ES_TAB3_DEFERRED_PAYMENT_IBLOCK_ID'),'',array('text', 30), '', ' *3.5'),
            array('tab3_deferred_payment_count_days', Loc::getMessage('WEBTU_ES_TAB3_DEFERRED_PAYMENT_COUNT_DAYS'),'',array('text', 30), '', ' *3.6'),
            array('tab3_deferred_payment_sum', Loc::getMessage('WEBTU_ES_TAB3_DEFERRED_PAYMENT_SUM'),'',array('text', 30), '', ' *3.7'),
            array('tab3_deferred_payment_sum_not_available', Loc::getMessage('WEBTU_ES_TAB3_DEFERRED_PAYMENT_SUM_NOT_AVAILABLE'),'',array('text', 30), '', ' *3.8'),
            array('note'=>Loc::getMessage('WEBTU_ES_TAB3_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit4',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB4'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB4_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB4_TITLE_HEADER_1'),
            array('tab4_props_type_user_id', Loc::getMessage('WEBTU_ES_TAB4_PROPS_TYPE_USER_ID'),'',array('text', 30),'',' *4.1'),
            array('tab4_props_type_user_code', Loc::getMessage('WEBTU_ES_TAB4_PROPS_TYPE_USER_CODE'),'',array('text', 30),'',''),
            array('tab4_props_cashback_id', Loc::getMessage('WEBTU_ES_TAB4_PROPS_CASHBACK_ID'),'',array('text', 30),'',' *4.2'),
            array('tab4_props_cashback_code', Loc::getMessage('WEBTU_ES_TAB4_PROPS_CASHBACK_CODE'),'',array('text', 30),'',''),
            Loc::getMessage('WEBTU_ES_TAB4_TITLE_HEADER_2'),
            array('tab4_props_quest_raiting_id', Loc::getMessage('WEBTU_ES_TAB4_PROPS_QUEST_RAITING_ID'),'',array('text', 30),'',' *4.3'),
            array('tab4_props_quest_raiting_code', Loc::getMessage('WEBTU_ES_TAB4_PROPS_QUEST_RAITING_CODE'),'',array('text', 30),'',''),
            array('tab4_props_quest_status_id', Loc::getMessage('WEBTU_ES_TAB4_PROPS_QUEST_STATUS_ID'),'',array('text', 30),'',' *4.4'),
            array('tab4_props_quest_status_code', Loc::getMessage('WEBTU_ES_TAB4_PROPS_QUEST_STATUS_CODE'),'',array('text', 30),'',''),
            array('tab4_props_quest_integration_system_id', Loc::getMessage('WEBTU_ES_TAB4_PROPS_QUEST_INTEGRATION_SYSTEM_ID'),'',array('text', 30),'',' *4.5'),
            array('tab4_props_quest_integration_system_code', Loc::getMessage('WEBTU_ES_TAB4_PROPS_QUEST_INTEGRATION_SYSTEM_CODE'),'',array('text', 30),'',''),
            array('note'=>Loc::getMessage('WEBTU_ES_TAB4_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit5',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB5'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB5_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB5_TITLE_HEADER_1'),
            array('tab5_bonus_type_user_value', Loc::getMessage('WEBTU_ES_TAB5_BONUS_TYPE_USER_VALUE'),'4',array('selectbox', $arTypeUser),'',' *5.1'),
            array('tab5_bonus_partner_hash', Loc::getMessage('WEBTU_ES_TAB5_BONUS_PARTNER_HASH'),'',array('text', 30),'',' *5.2'),
            array('tab5_bonus_currency', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CURRENCY'),'RUB',array('text', 30),'',' *5.3'),
            array('tab5_bonus_enrol_users_all', Loc::getMessage('WEBTU_ES_TAB5_BONUS_ENROL_USERS_ALL'),'',array('checkbox'),'',' *5.4'),
            //array('tab5_bonus_deduct', Loc::getMessage('WEBTU_ES_TAB5_BONUS_DEDUCT'),'',array('checkbox'),'',' *5.5'),
            //array('tab5_bonus_deduct_day', Loc::getMessage('WEBTU_ES_TAB5_BONUS_DEDUCT_DAY'),'',array('text', 30),'',' *5.6'),
            Loc::getMessage('WEBTU_ES_TAB5_TITLE_HEADER_2'),
            array('tab5_bonus_cashback', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CASHBACK'),'',array('checkbox', ""),'',' *5.5'),
            array('tab5_bonus_cashback_min', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CASHBACK_MIN'),'',array('text', 30),'',' *5.6'),
            array('tab5_bonus_cashback_max', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CASHBACK_MAX'),'',array('text', 30),'','*5.7'),
            array('tab5_bonus_cashback_top15', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CASHBACK_TOP15'),'',array('text', 30),'','*5.8'),
            array('tab5_bonus_cashback_top30', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CASHBACK_TOP30'),'',array('text', 30),'','*5.9'),
            array('tab5_bonus_cashback_top40', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CASHBACK_TOP40'),'',array('text', 30),'','*5.10'),
            array('tab5_bonus_cashback_top50', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CASHBACK_TOP50'),'',array('text', 30),'','*5.11'),
            array('tab5_bonus_cashback_top70', Loc::getMessage('WEBTU_ES_TAB5_BONUS_CASHBACK_TOP70'),'',array('text', 30),'','*5.12'),
            Loc::getMessage('WEBTU_ES_TAB5_TITLE_HEADER_3'),
            array('tab5_bonus_event_register', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_REGISTER'),'',array('checkbox'),'',' *5.13'),
            array('tab5_bonus_event_register_sum', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_REGISTER_SUM'),'',array('text', 30),'',' *5.14'),
            array('tab5_bonus_event_register_sum_2', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_REGISTER_SUM_2'),'',array('text', 30),'',' *5.15'),
            array('tab5_bonus_event_register_invite', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_REGISTER_INVITE'),'',array('checkbox'),'',' *5.16'),
            array('tab5_bonus_event_register_invite_sum', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_REGISTER_INVITE_SUM'),'',array('text', 30),'',' *5.17'),
            array('tab5_bonus_event_review_add', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_REVIEW_ADD'),'',array('checkbox'),'',' *5.18'),
            array('tab5_bonus_event_review_add_sum', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_REVIEW_ADD_SUM'),'',array('text', 30),'',' *5.19'),
            array('tab5_bonus_event_team_add', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_TEAM_ADD'),'',array('checkbox'),'',' *5.20'),
            array('tab5_bonus_event_team_add_sum', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_TEAM_ADD_SUM'),'',array('text', 30),'',' *5.21'),
            array('tab5_bonus_event_sunc_social_network', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_SUNC_SOCIAL_NETWORK'),'',array('checkbox'),'',' *5.22'),
            array('tab5_bonus_event_sunc_social_network_sum', Loc::getMessage('WEBTU_ES_TAB5_BONUS_EVENT_SUNC_SOCIAL_NETWORK_SUM'),'',array('text', 30),'',' *5.23'),
            array('note'=>Loc::getMessage('WEBTU_ES_TAB5_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit6',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB6'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB6_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB6_TITLE_HEADER_1'),
            array('tab6_order_auth_user_id', Loc::getMessage('WEBTU_ES_TAB6_ORDER_AUTH_USER_ID'),'1',array('text', 30),'',' *6.1'),
            array('tab6_order_cancel_iblock_id', Loc::getMessage('WEBTU_ES_TAB6_ORDER_CANCEL_IBLOCK_ID'),'',array('text', 30),'',' *6.2'),
            array('tab6_order_quest_completed_iblock_id', Loc::getMessage('WEBTU_ES_TAB6_ORDER_QUEST_COMPLETED_IBLOCK_ID'),'',array('text', 30),'',' *6.3'),
            array('tab6_order_percent_debit', Loc::getMessage('WEBTU_ES_TAB6_ORDER_PERCENT_DEBIT'),'',array('text', 30),'',' *6.4'),
            Loc::getMessage('WEBTU_ES_TAB6_TITLE_HEADER_2'),
            array('tab6_order_level_diff_1', Loc::getMessage('WEBTU_ES_TAB6_ORDER_LEVEL_DIFF_1'),'',array('text', 30),'',' *6.5'),
            array('tab6_order_level_diff_2', Loc::getMessage('WEBTU_ES_TAB6_ORDER_LEVEL_DIFF_2'),'',array('text', 30),'',' *6.6'),
            array('tab6_order_level_diff_3', Loc::getMessage('WEBTU_ES_TAB6_ORDER_LEVEL_DIFF_3'),'',array('text', 30),'',' *6.7'),
            array('tab6_order_level_diff_4', Loc::getMessage('WEBTU_ES_TAB6_ORDER_LEVEL_DIFF_4'),'',array('text', 30),'',' *6.8'),
            array('tab6_order_time_prompt', Loc::getMessage('WEBTU_ES_TAB6_ORDER_TIME_PROMPT'),'',array('text', 30),'',' *6.9'),
            array('note'=>Loc::getMessage('WEBTU_ES_TAB6_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit7',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB7'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB7_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB7_TITLE_HEADER_1'),
            array('tab7_catalog_iblock_id', Loc::getMessage('WEBTU_ES_TAB7_CATALOG_IBLOCK_ID'),'',array('text', 30),'',' *7.1'),
            array('tab7_metatag_logo', Loc::getMessage('WEBTU_ES_TAB7_METATAG_LOGO'),'',array('text', 30),'',' *7.2'),

            array('note'=>Loc::getMessage('WEBTU_ES_TAB7_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit8',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB8'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB8_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB8_TITLE_HEADER_1'),
            array('tab8_seo_filter_iblock_id', Loc::getMessage('WEBTU_ES_TAB8_SEO_FILTER_IBLOCK_ID'),'',array('text', 30),'',' *8.1'),
            array('tab8_seo_filter_page', Loc::getMessage('WEBTU_ES_TAB8_SEO_FILTER_PAGE'),'',array('text', 30),'',' *8.2'),
            array('tab8_seo_tag_count', Loc::getMessage('WEBTU_ES_TAB8_SEO_TAG_COUNT'),'',array('text', 30),'',' *8.3'),

            array('note'=>Loc::getMessage('WEBTU_ES_TAB8_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit9',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB9'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB9_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB9_TITLE_HEADER_1'),
            array('tab9_certificate_payment', Loc::getMessage('WEBTU_ES_TAB9_CERTIFICATE_PAYMENT'),'',array('selectbox', $arPayment),'',' *9.1'),
            array('tab9_booking_payment', Loc::getMessage('WEBTU_ES_TAB9_BOOKING_PAYMENT'),'',array('multiselectbox', $arPayment),'',' *9.2'),
            Loc::getMessage('WEBTU_ES_TAB9_TITLE_HEADER_2'),
            array('tab9_platron_transfer', Loc::getMessage('WEBTU_ES_TAB9_PLATRON_TRANSFER'),'',array('checkbox', ""),'',' *9.3'),
            array('tab9_platron_merchant_id', Loc::getMessage('WEBTU_ES_TAB9_PLATRON_MERCHANT_ID'),'',array('text', 30),'',' *9.4'),
            array('tab9_platron_secret_key', Loc::getMessage('WEBTU_ES_TAB9_PLATRON_SECRET_KEY'),'',array('text', 30),'',' *9.5'),
            array('tab9_platron_contract_id', Loc::getMessage('WEBTU_ES_TAB9_PLATRON_CONTRACT_ID'),'',array('text', 30),'',' *9.6'),
            array('tab9_platron_percent_value', Loc::getMessage('WEBTU_ES_TAB9_PLATRON_PERCENT_VALUE'),'',array('text', 30),'',' *9.7'),
            array('tab9_platron_rub_value', Loc::getMessage('WEBTU_ES_TAB9_PLATRON_RUB_VALUE'),'',array('text', 30),'',' *9.8'),
            array('tab9_commission_payment', Loc::getMessage('WEBTU_ES_TAB9_COMMISSION_PAYMENT'),'',array('selectbox', $arPayment),'',' *9.9'),
            array('note'=>Loc::getMessage('WEBTU_ES_TAB9_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit10',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB10'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB10_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB10_TITLE_HEADER_1'),
            array('tab10_awards_winter_season_date_from', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_DATE_FROM'),'',array('text', 10),'',' *10.1'),
            array('tab10_awards_winter_season_date_to', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_DATE_TO'),'',array('text', 10),'',' *10.2'),
            array('tab10_awards_winter_season_total_rating', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_ELEMENTS_ID_TOTAL_RATING'),'',array('text', 10),'',' *10.3'),
            array('tab10_awards_winter_season_rating_points_season', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_ELEMENTS_ID_RATING_POINTS_SEASON'),'',array('text', 10),'',' *10.4'),
            array('tab10_awards_winter_season_rating_quality_game', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_ELEMENTS_ID_RATING_QUALITY_GAME'),'',array('text', 10),'',' *10.5'),

            Loc::getMessage('WEBTU_ES_TAB10_TITLE_HEADER_2'),
            array('tab10_awards_spring_season_date_from', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_DATE_FROM'),'',array('text', 10),'',' *10.6'),
            array('tab10_awards_spring_season_date_to', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_DATE_TO'),'',array('text', 10),'',' *10.7'),
            array('tab10_awards_spring_season_total_rating', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_ELEMENTS_ID_TOTAL_RATING'),'',array('text', 10),'',' *10.8'),
            array('tab10_awards_spring_season_rating_points_season', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_ELEMENTS_ID_RATING_POINTS_SEASON'),'',array('text', 10),'',' *10.9'),
            array('tab10_awards_spring_season_rating_quality_game', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_ELEMENTS_ID_RATING_QUALITY_GAME'),'',array('text', 10),'',' *10.10'),

            Loc::getMessage('WEBTU_ES_TAB10_TITLE_HEADER_3'),
            array('tab10_awards_summer_season_date_from', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_DATE_FROM'),'',array('text', 10),'',' *10.11'),
            array('tab10_awards_summer_season_date_to', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_DATE_TO'),'',array('text', 10),'',' *10.12'),
            array('tab10_awards_summer_season_total_rating', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_ELEMENTS_ID_TOTAL_RATING'),'',array('text', 10),'',' *10.13'),
            array('tab10_awards_summer_season_rating_points_season', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_ELEMENTS_ID_RATING_POINTS_SEASON'),'',array('text', 10),'',' *10.14'),
            array('tab10_awards_summer_season_rating_quality_game', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_ELEMENTS_ID_RATING_QUALITY_GAME'),'',array('text', 10),'',' *10.15'),

            Loc::getMessage('WEBTU_ES_TAB10_TITLE_HEADER_4'),
            array('tab10_awards_autumn_season_date_from', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_DATE_FROM'),'',array('text', 10),'',' *10.16'),
            array('tab10_awards_autumn_season_date_to', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_DATE_TO'),'',array('text', 10),'',' *10.17'),
            array('tab10_awards_autumn_season_total_rating', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_ELEMENTS_ID_TOTAL_RATING'),'',array('text', 10),'',' *10.18'),
            array('tab10_awards_autumn_season_rating_points_season', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_ELEMENTS_ID_RATING_POINTS_SEASON'),'',array('text', 10),'',' *10.19'),
            array('tab10_awards_autumn_season_rating_quality_game', Loc::getMessage('WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_ELEMENTS_ID_RATING_QUALITY_GAME'),'',array('text', 10),'',' *10.20'),

            array('note'=>Loc::getMessage('WEBTU_ES_TAB10_NOTE')),
        )
    ),
    array(
        'DIV' => 'edit11',
        'TAB' => Loc::getMessage('WEBTU_ES_TAB11'),
        'TITLE' => Loc::getMessage('WEBTU_ES_TAB11_TITLE'),
        'OPTIONS' => array(
            Loc::getMessage('WEBTU_ES_TAB11_TITLE'),
            array('tab11_smtp_login', Loc::getMessage('WEBTU_ES_TAB11_SMTP_LOGIN'),'',array('text', 30)),
            array('tab11_smtp_password', Loc::getMessage('WEBTU_ES_TAB11_SMTP_PASSWORD'),'',array('password', 30)),
            array('tab11_smtp_name', Loc::getMessage('WEBTU_ES_TAB11_SMTP_NAME'),'',array('text', 30)),
            array('tab11_smtp_server', Loc::getMessage('WEBTU_ES_TAB11_SMTP_SERVER'),'',array('text', 30)),
            array('tab11_smtp_port', Loc::getMessage('WEBTU_ES_TAB11_SMTP_PORT'),'',array('text', 30)),
        )
    ),
    array(
        'DIV' => 'edit99',
        'TAB' => Loc::getMessage('MAIN_TAB_RIGHTS'),
        'TITLE' => Loc::getMessage('MAIN_TAB_TITLE_RIGHTS')
    ),
);

#Сохранение
if ($request->isPost() && $request['Update'] && check_bitrix_sessid())
{
    foreach ($aTabs as $aTab)
    {
        #Или можно использовать __AdmSettingsSaveOptions($MODULE_ID, $arOptions);
        foreach ($aTab['OPTIONS'] as $arOption)
        {
            #Строка с подсветкой. Используется для разделения настроек в одной вкладке
            if (!is_array($arOption)){ continue; }
                
            #Уведомление с подсветкой
            if ($arOption['note']){ continue; }
                
            #Или __AdmSettingsSaveOption($MODULE_ID, $arOption);
            $optionName = $arOption[0];

            $optionValue = $request->getPost($optionName);

            Option::set($module_id, $optionName, is_array($optionValue) ? implode(",", $optionValue):$optionValue);
        }
    }
}

#Визуальный вывод
#получаем объект класса от CAdminTabControl где tabControl id формы
$tabControl = new CAdminTabControl('tabControl', $aTabs);

#Открываем форму
$tabControl->Begin(); ?>
<form method='post' action='<?echo $APPLICATION->GetCurPage()?>?mid=<?=htmlspecialcharsbx($request['mid'])?>&amp;lang=<?=$request['lang']?>' name='webtu_salefood_settings'>
    <?
        foreach ($aTabs as $aTab):
            if($aTab['OPTIONS']):
                #открываем вкладку
                $tabControl->BeginNextTab();

                #методо который по переданным данным подгружает данные (опции) из бд и формитрет
                __AdmSettingsDrawList($module_id, $aTab['OPTIONS']); 
            endif;
        endforeach;

        #Добавим вкладку управление правами доступами
        $tabControl->BeginNextTab();
            require_once( $_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/admin/group_rights.php" );
        $tabControl->Buttons(); 
    ?>
    <input type="submit" name="Update" value="<?echo GetMessage('MAIN_SAVE')?>" />
    <input type="reset" name="reset" value="<?echo GetMessage('MAIN_RESET')?>" />
    <?=bitrix_sessid_post();?>
</form>
<? $tabControl->End(); ?>