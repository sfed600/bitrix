<?php
namespace Webtu\ExpansionSite;

use \Bitrix\Main;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Webtu\ExpansionSite\Handler;

Loc::loadMessages(__FILE__);


class Notify
{
    private $arResult = array();

    public function __construct(){}

    /**
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock')){
            throw new Main\LoaderException( Loc::getMessage('NOTIFY_NOT_INSTALLED_MODULE', Array ("#ID#" => "iblock")) );
        }

        if (!Loader::includeModule('webtu.expansionsite'))
        {
            throw new Main\LoaderException(Loc::getMessage('NOTIFY_NOT_INSTALLED_MODULE', Array ("#ID#" => "webtu.expansionsite")) );
        }

        if (!Loader::includeModule('im'))
        {
            throw new Main\LoaderException(Loc::getMessage('NOTIFY_NOT_INSTALLED_MODULE', Array ("#ID#" => "im")) );
        }
    }

    
    /**
     * @param $arFields
     * @throws Main\LoaderException
     */
    public static  function userRegisterNotify(&$arFields)
    {
        $team_id = (int) $_SESSION["INVITE"]["TEAM_ID"];

        if ( $team_id > 0 )
        {
            self::notifyByCaptain($team_id, $arFields["USER_ID"]);
        }
    }

    /**
     * @param $team_id
     * @param $user_id
     * @return bool
     * @throws Main\LoaderException
     */
    public function notifyByCaptain ($team_id, $user_id)
    {
        $this->checkModules();

        global $USER;

        $this->arResult["TEAM_ID"] = $team_id;
        $this->arResult["USER_ID"] = $user_id;

        if(
            $this->arResult["TEAM_ID"] <= 0
            && $this->arResult["USER_ID"] <= 0
        )
        {
            return false;
        }

        $this->arResult["TEAM"] = $this->getTeam( $this->arResult["TEAM_ID"]);

        $rsUser = $USER->GetByID($user_id);
        $this->arResult["USER"] = $rsUser->Fetch();

        if (
            $this->arResult["TEAM"]["CAPTAIN"] > 0
            && $this->arResult["USER"]["ID"] != $this->arResult["TEAM"]["CAPTAIN"]
            && !in_array($this->arResult["USER"]["ID"], $this->arResult["TEAM"]["TEAM_MEMBER"])
            && !in_array($this->arResult["TEAM"]["ID"],  $this->arResult["USER"]["UF_NOTIFY_BY_TEAM"])
            && $this->arResult["TEAM"]["ADD_NEW_PLAYER"] == "Y"
        )
        {
            #Создаем уведомление капитану запрос вступить в команду в команду
            $arMessageFieldsCaptain = array(
                #Получатель
                "TO_USER_ID" => $this->arResult["TEAM"]["CAPTAIN"],
                #Отправитель (может быть >= 0)
                "FROM_USER_ID" => $this->arResult["USER"]["ID"],
                #Тип уведомления
                "NOTIFY_TYPE" => IM_NOTIFY_CONFIRM,
                #Модуль запросивший отправку уведомления
                "NOTIFY_MODULE" => "webtu.expansionsite",
                #Символьный тэг для группировки (будет выведено только одно сообщение), если это не требуется - не задаем параметр
                "NOTIFY_TAG" => "TEAM_INVITE_CAPTAIN|".$this->arResult["TEAM"]["ID"]."|".$this->arResult["USER"]["ID"],
                #Текст уведомления на сайте (доступен html и бб-коды)
                "NOTIFY_MESSAGE" => 'Хочу вступить в команду <a href="'.$this->arResult["TEAM"]["DETAIL_PAGE"].'" target="blank">'. $this->arResult["TEAM"]["NAME"].'</a>',
                #Текст уведомления для отправки на почту (или XMPP), если различий нет - не задаем параметр
                //"NOTIFY_MESSAGE_OUT" => '',
                #Массив описывающий кнопки уведомления
                "NOTIFY_BUTTONS" => Array(
                    #1. название кнопки, 2. значение, 3. шаблон кнопки, 4. переход по адресу после нажатия (не обязательный параметр)
                    Array('TITLE' => 'Принять', 'VALUE' => 'Y', 'TYPE' => 'accept' , 'URL' => '/personal/'),
                    Array('TITLE' => 'Отклонить', 'VALUE' => 'N', 'TYPE' => 'cancel', 'URL' => '/personal/'),
                ),
                #Cимвольный код шаблона отправки письма, если не задавать отправляется шаблоном уведомлений
                //"NOTIFY_EMAIL_TEMPLATE" => "CALENDAR_INVITATION",

            );

            $resID = \CIMNotify::Add($arMessageFieldsCaptain);

            if ($resID > 0)
            {
                $arMessageFieldsUser = array(
                    "TO_USER_ID" => $this->arResult["USER"]["ID"],
                    "FROM_USER_ID" => 0,
                    "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                    "NOTIFY_MODULE" => "im",
                    "NOTIFY_MESSAGE" => 'Отправлен запрос капитану на заявку вступить в команду <a href="'.$this->arResult["TEAM"]["DETAIL_PAGE"].'" target="blank">'. $this->arResult["TEAM"]["NAME"].'</a>',

                );

                \CIMNotify::Add($arMessageFieldsUser);

                $this->arResult["USER"]["UF_NOTIFY_BY_TEAM"][] = $this->arResult["TEAM"]["ID"];

                $fields = Array(
                    "UF_NOTIFY_BY_TEAM" => $this->arResult["USER"]["UF_NOTIFY_BY_TEAM"],
                );

                $USER->Update($this->arResult["USER"]["ID"], $fields);


            }
        }

    }

    /**
     * @param $team_id
     * @param $user_id
     * @param $value
     * @return bool
     * @throws Main\LoaderException
     */
    public function notifyByConfirm ($team_id, $user_id, $value, $tag)
    {
        $this->checkModules();

        global $USER;

        $arOptions = Handler::getOptions();
        $el = new \CIBlockElement();

        $this->arResult["TEAM_ID"] = $team_id;
        $this->arResult["USER_ID"] = $user_id;

        if(
            $this->arResult["TEAM_ID"] <= 0
            && $this->arResult["USER_ID"] <= 0
            && empty($value)
            && empty($tag)
        )
        {
            return false;
        }

        $this->arResult["TEAM"] = $this->getTeam( $this->arResult["TEAM_ID"]);

        $rsUser = $USER->GetByID($user_id);
        $this->arResult["USER"] = $rsUser->Fetch();

        if ( $this->arResult["TEAM"]["ID"] ==  $team_id)
        {
            if ($value == "Y")
            {
                $this->arResult["TEAM"]["TEAM_MEMBER"][] = $this->arResult["USER"]["ID"];

                $el->SetPropertyValuesEx($this->arResult["TEAM"]["ID"], $arOptions["TEAM"]["IBLOCK_ID"], array("TEAM_MEMBER" => $this->arResult["TEAM"]["TEAM_MEMBER"]));

                if ( $tag == "TEAM_INVITE" )
                {
                    $NOTIFY_MESSAGE_CAPTAIN = 'Пользователь <a href="/personal/user/'.$this->arResult["USER"]["ID"].'/">'.$this->arResult["USER"]["NAME"].' '.$this->arResult["USER"] ["LAST_NAME"].'</a> принял(а) ваше приглашение вступить в команду <a href="'.$this->arResult["TEAM"]["DETAIL_PAGE"].'">'.$this->arResult["TEAM"]["NAME"].'</a>';
                    $NOTIFY_MESSAGE_USER = 'Поздравляем, теперь вы участник команды <a href="'.$this->arResult["TEAM"]["DETAIL_PAGE"].'">'.$this->arResult["TEAM"]["NAME"].'</a>';
                }
                else if ( $tag == "TEAM_INVITE_CAPTAIN" )
                {
                    $NOTIFY_MESSAGE_CAPTAIN = '';
                    $NOTIFY_MESSAGE_USER = 'Поздравляем, теперь вы участник команды <a href="'.$this->arResult["TEAM"]["DETAIL_PAGE"].'">'.$this->arResult["TEAM"]["NAME"].'</a>';
                }
                else
                {
                    $NOTIFY_MESSAGE_CAPTAIN = '';
                    $NOTIFY_MESSAGE_USER = '';
                }

                if ( strlen($NOTIFY_MESSAGE_CAPTAIN) > 0 )
                {
                    $arMessageFieldsConfirm = array(
                        "TO_USER_ID" => $this->arResult["TEAM"]["CAPTAIN"],
                        "FROM_USER_ID" => 0,
                        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                        "NOTIFY_MODULE" => "im",
                        "NOTIFY_MESSAGE" => $NOTIFY_MESSAGE_CAPTAIN,
                    );

                    \CIMNotify::Add($arMessageFieldsConfirm);
                }

                if ( strlen($NOTIFY_MESSAGE_USER) > 0 )
                {
                    $arMessageFieldsConfirmUser = array(
                        "TO_USER_ID" => $this->arResult["USER"] ["ID"],
                        "FROM_USER_ID" => $this->arResult["TEAM"]["CAPTAIN"],
                        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                        "NOTIFY_MODULE" => "im",
                        "NOTIFY_MESSAGE" => $NOTIFY_MESSAGE_USER,
                    );

                    \CIMNotify::Add($arMessageFieldsConfirmUser);
                }

                $this->removeNotifyUserProp($this->arResult["TEAM"]["ID"]);
            }
            else
            {
                if ( $tag == "TEAM_INVITE" )
                {
                    $NOTIFY_MESSAGE_CAPTAIN = 'Пользователь <a href="/personal/user/'.$this->arResult["USER"]["ID"].'/">'.$this->arResult["USER"]["NAME"].' '.$this->arResult["USER"]["LAST_NAME"].'</a> отклонил(а) ваше приглашение вступить в команду <a href="'.$this->arResult["TEAM"]["DETAIL_PAGE"].'">'.$this->arResult["TEAM"]["NAME"].'</a>';
                    $NOTIFY_MESSAGE_USER = '';
                }
                else if ( $tag == "TEAM_INVITE_CAPTAIN" )
                {
                    $NOTIFY_MESSAGE_CAPTAIN = '';
                    $NOTIFY_MESSAGE_USER = 'Капитан команды отклонил вашу заявку вспупить в команду <a href="'.$this->arResult["TEAM"]["DETAIL_PAGE"].'">'.$this->arResult["TEAM"]["NAME"].'</a>';
                }
                else
                {
                    $NOTIFY_MESSAGE_CAPTAIN = '';
                    $NOTIFY_MESSAGE_USER = '';
                }

                if ( strlen($NOTIFY_MESSAGE_CAPTAIN) > 0 )
                {
                    $arMessageFieldsCancel = array(
                        "TO_USER_ID" => $this->arResult["TEAM"]["CAPTAIN"],
                        "FROM_USER_ID" => 0,
                        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                        "NOTIFY_MODULE" => "im",
                        "NOTIFY_MESSAGE" => $NOTIFY_MESSAGE_CAPTAIN,

                    );

                    \CIMNotify::Add($arMessageFieldsCancel);
                }

                if ( strlen($NOTIFY_MESSAGE_USER) > 0 )
                {
                    $arMessageFieldsCancel = array(
                        "TO_USER_ID" => $this->arResult["USER"] ["ID"],
                        "FROM_USER_ID" => 0,
                        "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                        "NOTIFY_MODULE" => "im",
                        "NOTIFY_MESSAGE" => $NOTIFY_MESSAGE_USER,

                    );

                    \CIMNotify::Add($arMessageFieldsCancel);

                    $this->removeNotifyUserProp($this->arResult["TEAM"]["ID"]);
                }
            }
        }

    }

    /**
     * @param $team_id
     * @return array|bool
     */
    public function getTeam($team_id)
    {
        $arOptions = Handler::getOptions();

        if (
            $team_id <= 0
            && $arOptions["TEAM"]["IBLOCK_ID"] <= 0
        )
        {
            return false;
        }

        $el = new \CIBlockElement();

        $arTeam = array();

        $arFilter = Array(
            "IBLOCK_ID" => $arOptions["TEAM"]["IBLOCK_ID"],
            "ID" => $team_id,
        );

        $arSelect = Array(
            "ID",
            "IBLOCK_ID",
            "NAME"
        );

        $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();

            $arTeam = array(
                "ID" => $arFields["ID"],
                "NAME" => $arFields["NAME"],
                "DETAIL_PAGE" => '/personal/team/'.$arFields["ID"].'/',
                "CAPTAIN" => $arProps["CAPTAIN"]["VALUE"],
                "TEAM_MEMBER" => $arProps["TEAM_MEMBER"]["VALUE"],
                "ADD_NEW_PLAYER" => ( count($arProps["TEAM_MEMBER"]["VALUE"]) < $arOptions["TEAM"]["MAX_COUNT_MEMBERS"] ) ? "Y" : "N",
            );
        }

        return $arTeam;
    }


    /**
     * @param $element_id
     * @param $iblock_id
     * @param $user_id
     * @param $value
     * @return bool
     * @throws Main\LoaderException
     */
    public function notifyUserJoin($element_id, $iblock_id, $user_id, $value)
    {
        $this->checkModules();

        if(
            $element_id  <= 0
            && $iblock_id <= 0
            && $user_id <= 0
            && empty($value)
        )
        {
            return false;
        }

        global $USER;

        $this->arResult["ITEM"] = $this->getElementInfo($element_id, $iblock_id);

        $rsUser = $USER->GetByID($user_id);
        $this->arResult["USER"] = $rsUser->Fetch();

        if ( $this->arResult["ITEM"]["ID"] ==  $element_id)
        {
            if ($value == "Y")
            {
                $el = new \CIBlockElement();

                $this->arResult["ITEM"]["USERS_JOIN"][] = $this->arResult["USER"]["ID"];

                $el->SetPropertyValuesEx($this->arResult["ITEM"]["ID"], $iblock_id, array("USERS_JOIN" => $this->arResult["ITEM"]["USERS_JOIN"]));

                if ( $this->arResult["ITEM"]["PLAYERS"] >= 0 )
                {
                    $this->arResult["ITEM"]["PLAYERS"] = $this->arResult["ITEM"]["PLAYERS"] + 1;

                    $PLAYERS = $this->arResult["ITEM"]["PROP_ENUM"]["PLAYERS"][$this->arResult["ITEM"]["PLAYERS"]]["ID"];

                }
                else
                {

                    $PLAYERS = false;
                }

                if ( $this->arResult["ITEM"]["PLAYERS_ALL"] > 0 )
                {
                    $this->arResult["ITEM"]["PLAYERS_ALL"] = $this->arResult["ITEM"]["PLAYERS_ALL"] - 1;
                    $PLAYERS_ALL = $this->arResult["ITEM"]["PROP_ENUM"]["PLAYERS_ALL"][$this->arResult["ITEM"]["PLAYERS_ALL"]]["ID"];
                }
                else
                {
                    $PLAYERS_ALL = false;
                }

                $el->SetPropertyValuesEx($this->arResult["ITEM"]["ID"], $iblock_id, array("PLAYERS" => $PLAYERS));
                $el->SetPropertyValuesEx($this->arResult["ITEM"]["ID"], $iblock_id, array("PLAYERS_ALL" => $PLAYERS_ALL));


                if ( $this->arResult["ITEM"]["PLAYERS_ALL"] == 0 )
                {

                    $arUpdateValues = array("ACTIVE"=> "N");

                    $el->Update($this->arResult["ITEM"]["ID"], $arUpdateValues);

                }


                $arMessageFieldsConfirm = array(
                    "TO_USER_ID" => $this->arResult["USER"]["ID"],
                    "FROM_USER_ID" => 0,
                    "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                    "NOTIFY_MODULE" => "im",
                    "NOTIFY_MESSAGE" => 'Ваш запрос на присоединение к походу на '.$this->arResult["ITEM"]["QUEST_NAME"].' '.$this->arResult["ITEM"]["DATE"].' одобрен.',
                );

                \CIMNotify::Add($arMessageFieldsConfirm);

                $this->removeNotifyUserProp($this->arResult["ITEM"]["ID"]);

            }
            else
            {
                $arMessageFieldsCancel = array(
                    "TO_USER_ID" => $this->arResult["USER"]["ID"],
                    "FROM_USER_ID" => 0,
                    "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                    "NOTIFY_MODULE" => "im",
                    "NOTIFY_MESSAGE" =>'Ваш запрос на присоединение к походу на '.$this->arResult["ITEM"]["QUEST_NAME"].' '.$this->arResult["ITEM"]["DATE"].' отклонен.',

                );

                \CIMNotify::Add($arMessageFieldsCancel);

                $this->removeNotifyUserProp($this->arResult["ITEM"]["ID"]);
            }

        }

    }

    /**
     * @param $element_id
     * @param $iblock_id
     * @return array|bool
     */
    protected function getElementInfo($element_id, $iblock_id)
    {
        if(
            $element_id  <= 0
            && $iblock_id <= 0
        )
        {
            return false;
        }

        $el = new \CIBlockElement();

        $arItem = array();

        $arFilter = Array(
            "IBLOCK_ID" => $iblock_id,
            "ID" => $element_id,
            "ACTIVE" => "Y",
            "ACTIVE_DATE" => "Y"
        );

        $res = $el->GetList(Array(), $arFilter, false, false, Array());

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();
            $arProps = $ob->GetProperties();

            $quest_id = $arProps["QUEST_ID"]["VALUE"];
            $quest_name = '';

            if ($quest_id > 0)
            {
                $resQuest = $el->GetByID($quest_id);

                if($arQuest = $resQuest->GetNext())
                {
                    $quest_name = $arQuest["NAME"];
                }
            }

            $arItem = array(
                "ID" => $arFields["ID"],
                "QUEST_NAME" => $quest_name,
                "DATE" => $arProps["DATE"]["VALUE"].' '.$arProps["TIME"]["VALUE"],
                "USERS_JOIN" => $arProps["USERS_JOIN"]["VALUE"],
                "PLAYERS" => ($arProps["PLAYERS"]["VALUE"] > 0) ? $arProps["PLAYERS"]["VALUE"] : 0,
                "PLAYERS_ALL" => ($arProps["PLAYERS_ALL"]["VALUE"] > 0) ? $arProps["PLAYERS_ALL"]["VALUE"] : 0,
            );

        }

        $arPropsEnum = array("PLAYERS", "PLAYERS_ALL",);

        foreach ( $arPropsEnum as $prop_code )
        {
            $property_enums = \CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID" => $iblock_id, "CODE" => $prop_code));

            while($enum_fields = $property_enums->GetNext())
            {
                $arItem["PROP_ENUM"][$prop_code][$enum_fields["VALUE"]] = array(
                    "ID"    => $enum_fields["ID"],
                    "VALUE" => $enum_fields["VALUE"],
                    "XML_ID" => $enum_fields["XML_ID"]
                );
            }
        }

        return $arItem;
    }

    /**
     * @param $id_remove
     */
    protected function removeNotifyUserProp($id_remove)
    {
        if (
            $id_remove > 0
            && $this->arResult["USER"]["ID"] > 0
        )
        {
            global $USER;

            foreach ($this->arResult["USER"]["UF_NOTIFY_BY_TEAM"] as $key => $team)
            {
                if ($team == $id_remove)
                {
                    unset($this->arResult["USER"]["UF_NOTIFY_BY_TEAM"][$key]);
                }
            }

            if ( count($this->arResult["USER"]["UF_NOTIFY_BY_TEAM"]) > 0 )
            {
                $UF_NOTIFY_BY_TEAM = $this->arResult["USER"]["UF_NOTIFY_BY_TEAM"];
            }
            else
            {
                $UF_NOTIFY_BY_TEAM = array();
            }

            $fields = Array(
                "UF_NOTIFY_BY_TEAM" => $UF_NOTIFY_BY_TEAM,
            );

            $USER->Update($this->arResult["USER"]["ID"], $fields);
        }

    }

}