<?php
namespace Webtu\ExpansionSite\Mysql;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class CouponListTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'webtu_coupon_list';
    }

    public static function getUfId()
    {
        return 'WEBTU_COUPON_LIST';
    }

    public static function getMap()
    {
        return array(
            // ID
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            // ID ORDER
            new Entity\IntegerField('ORDER_ID', array(
                'required' => true
            )),
            // ID CERTIFICATE
            new Entity\IntegerField('CERTIFICATE_ID', array(
                'required' => true
            )),
            // COUPON
            new Entity\StringField('COUPON', array(
                'required' => true
            )),
            // DATE CREATE
            new Entity\DatetimeField('DATE_CREATE', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
            // DATE ACTUAL TO
            new Entity\DatetimeField('DATE_ACTUAL_TO', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
            // ACTIVATE
            new Entity\StringField('ACTIVATE', array(
                'required' => true,
                'default_value' => 'N'
            )),
        );
    }
}