<?
$MESS["ORDER_NOT_INSTALLED_MODULE"] = "Модуль #ID# не установлен!";
$MESS["ORDER_ERROR_CANCEL_ORDER"] = "Ошибка. Не удалось отменить бронирование.";
$MESS["ORDER_PAY_SUM_DESCRIPTION"] = "Возврат комиссии: #SUM# руб. за бронирование № #ORDER_ID#.";
$MESS["ORDER_DEDUCT_SUM_DESCRIPTION"] = "Списание комиссии: #SUM# руб. за бронирование № #ORDER_ID#.";
