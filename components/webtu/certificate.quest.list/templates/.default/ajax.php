<?
/** @global \CMain $APPLICATION */
define('STOP_STATISTICS', true);

$siteId = isset($_REQUEST['siteId']) && is_string($_REQUEST['siteId']) ? $_REQUEST['siteId'] : '';
$siteId = substr(preg_replace('/[^a-z0-9_]/i', '', $siteId), 0, 2);
if (!empty($siteId) && is_string($siteId))
{
    define('SITE_ID', $siteId);
}

require_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/prolog_before.php');

$request = \Bitrix\Main\Application::getInstance()->getContext()->getRequest();

$request->addFilter(new \Bitrix\Main\Web\PostDecodeFilter);

if ($request->isAjaxRequest())
{
    foreach ($request->getPostList() as $name => $value)
    {
        if (preg_match('%^PAGEN_(\d+)$%', $name, $m))
        {
            global $NavNum;
            $NavNum = (int)$m[1] - 1;
        }
    }
}

if (!\Bitrix\Main\Loader::includeModule('iblock'))
    return;

$signer = new \Bitrix\Main\Security\Sign\Signer;
try
{
    $template = $signer->unsign($request->get('template'), 'certificate.quest.list');
    $paramString = $signer->unsign($request->get('parameters'), 'certificate.quest.list');
}
catch (\Bitrix\Main\Security\Sign\BadSignatureException $e)
{
    die();
}

$parameters = unserialize(base64_decode($paramString));
if (isset($parameters['PARENT_NAME']))
{
    $parent = new CBitrixComponent();
    $parent->InitComponent($parameters['PARENT_NAME'], $parameters['PARENT_TEMPLATE_NAME']);
    $parent->InitComponentTemplate($parameters['PARENT_TEMPLATE_PAGE']);
}
else
{
    $parent = false;
}

$paramsProtect = array("IBLOCK_ID","NEWS_COUNT");

foreach($parameters as $key=>$paramItem)
{
    #удаляем дубли
    if(strpos($key,"~") !== false)
    {
        unset($parameters[$key]);
    }

    if(trim($paramItem) == 1 and in_array($paramItem, $paramsProtect))
    {
        $parameters[$key] = "Y";
    }
}

global $arrFilter;


$parameters["FILTER_NAME"] = "arrFilterQuest";


$arrFilter = array_merge((array)$arrFilter, (array)$parameters["FILTER_DATA"]);

$APPLICATION->IncludeComponent(
    'webtu:certificate.quest.list',
    $template,
    $parameters,
    $parent
);