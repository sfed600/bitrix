<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    "NAME" => Loc::getMessage("CL_LIST_NAME"),
    "DESCRIPTION" => Loc::getMessage("CL_LIST_DESCRIPTION"),
    "ICON" => "/images/news_list.gif",
    "SORT" => 100,
    "PATH" => array(
        "ID"   => "WEBTU",
        "NAME" => Loc::getMessage("WEBTU_PATH_ADDITIONAL"),
        "CHILD" => array(
            "ID" => "certificate",
            "NAME" => Loc::getMessage("CL_PATH_ADDITIONAL"),
            "SORT" => 500,
            "CHILD" => array(
                "ID" => "certificate_cmpx",
            ),
        ),
    )
);
?>
