<?php
if( !defined( "B_PROLOG_INCLUDED" ) || B_PROLOG_INCLUDED !== true ) die();

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

use \Bitrix\Main;
use \Bitrix\Main\Application;
use \Bitrix\Main\Context;
use \Bitrix\Main\DateTime;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Webtu\ExpansionSite\Handler;

class OrganizationQuestList extends CBitrixComponent
{
    public $MessageError = array();
    public $MessageSend = array();
    public $arrFilter = array();

    protected $navParams = false;
    protected $navigation = false;
    protected $sortFields = array();
    protected $filterFields = array();
    protected $selectFields = array();
    protected $arOptions = array();

    /**
     * @brief Перезаписать параметры
     * @return массив $arParams
     **/
    public function onPrepareComponentParams($arParams)
    {
        global $USER;

        if (!isset($arParams['CURRENT_BASE_PAGE']))
        {
            $uri = new Main\Web\Uri($this->request->getRequestUri());
            $uri->deleteParams(Main\HttpRequest::getSystemParameters());
            $arParams['CURRENT_BASE_PAGE'] = $uri->getUri();
        }

        $arParams['IBLOCK_TYPE'] = isset($arParams['IBLOCK_TYPE']) ? trim($arParams['IBLOCK_TYPE']) : '';
        $arParams['IBLOCK_ID'] = intval($arParams['IBLOCK_ID']) > 0 ? intval($arParams['IBLOCK_ID']) : 0;

        if(strlen($arParams["SORT_BY1"])<=0) $arParams["SORT_BY1"] = "ACTIVE_FROM";
        if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER1"])) $arParams["SORT_ORDER1"] = "DESC";

        if(strlen($arParams["SORT_BY2"])<=0)
        {
            if (strtoupper($arParams["SORT_BY1"]) == 'SORT')
            {
                $arParams["SORT_BY2"] = "ID";
                $arParams["SORT_ORDER2"] = "DESC";
            }
            else
            {
                $arParams["SORT_BY2"] = "SORT";
            }
        }
        if(!preg_match('/^(asc|desc|nulls)(,asc|,desc|,nulls){0,1}$/i', $arParams["SORT_ORDER2"]))
            $arParams["SORT_ORDER2"]="ASC";

        if(strlen($arParams["FILTER_NAME"])<=0 || !preg_match("/^[A-Za-z_][A-Za-z01-9_]*$/", $arParams["FILTER_NAME"]))
        {
            $this->arrFilter = array();
        }
        else
        {
            $this->arrFilter = $arParams["FILTER_DATA"];
            if(!is_array($this->arrFilter))
                $this->arrFilter = array();
        }

        $arParams["DISPLAY_TOP_PAGER"] = $arParams["DISPLAY_TOP_PAGER"]=="Y";
        $arParams["DISPLAY_BOTTOM_PAGER"] = $arParams["DISPLAY_BOTTOM_PAGER"]!="N";

        $arParams["NEWS_COUNT"] = intval($arParams["NEWS_COUNT"]);
        if($arParams["NEWS_COUNT"]<=0)
        {
            $arParams["NEWS_COUNT"] = 10;
        }
        $arParams["PAGER_DESC_NUMBERING"] = $arParams["PAGER_DESC_NUMBERING"]=="Y";
        $arParams["PAGER_DESC_NUMBERING_CACHE_TIME"] = intval($arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]);
        $arParams["PAGER_SHOW_ALL"] = $arParams["PAGER_SHOW_ALL"]=="Y";
        
        $arParams["LAZY_LOAD"] = "Y";

        return $arParams;

    }

    /**
     * @brief Проверка подключаемых модулей
     **/
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock'))
        {
            throw new Main\LoaderException(Loc::getMessage('CQL_NOT_INSTALLED_IBLOCK', Array ("#ID#" => "iblock")) );
        }

        if (!Loader::includeModule('webtu.expansionsite'))
        {
            throw new Main\LoaderException( Loc::getMessage('CQL_NOT_INSTALLED_IBLOCK', Array ("#ID#" => "webtu.expansionsite")) );
        }

    }

    /**
     * @brief Проверка обязательных параметров
     * @return массив
     **/
    protected function checkOptions()
    {
        if( intval($this->arParams["IBLOCK_ID"]) <= 0 )
        {
            array_push($this->MessageError, Loc::getMessage('CQL_NONE_OPTIONS', Array ("#ID#" => "IBLOCK_ID")) );
        }
        else
        {
            $rsIBlock = CIBlock::GetList(array(), array(
                "ACTIVE" => "Y",
                "ID" => $this->arParams["IBLOCK_ID"],
            ));

            $this->arResult = $rsIBlock->GetNext();
            if (!$this->arResult["ID"])
            {
                array_push($this->MessageError, Loc::getMessage('CQL_NONE_IBLOCK_ID') );
            }
        }

        $this->arOptions = Handler::getOptions();
    }
    
    /**
     * @brief Основная логика
     **/
    protected function getResult()
    {
        #Получаем данные о шаблоне
        $this->InitComponentTemplate();
        //инициализируем шаблон, а затем получаем название и расположение
        $template = & $this->GetTemplate();
        $this->arResult["TEMPLATE_FILE"] = $template->GetFile();
        $this->arResult["TEMPLATE_FOLDER"] = $template->GetFolder();
        $this->arResult["PATH"] = $this->GetPath();

        if(
            count($this->MessageError) == 0
            && count($this->arParams["QUEST_LIST"]) > 0
        )
        {
            $this->arrFilter["ID"] = $this->arParams["QUEST_LIST"];

            if (isset($this->arParams['FILTER_DATA']['USER_PRICE'])) {
                $this->arrFilter['USER_PRICE'] = $this->arParams['FILTER_DATA']['USER_PRICE'];
            }
            if (isset($this->arParams['FILTER_DATA']['CERTIFICATE_MIN_COST'])) {
                $this->arrFilter['CERTIFICATE_MIN_COST'] = $this->arParams['FILTER_DATA']['CERTIFICATE_MIN_COST'];
            }

            $this->initElementList();
        }
    }

    /**
     * @brief Формирование Массива с элементами
     * @return массив
     **/
    protected function initElementList()
    {
        $this->initNavParams();
        $this->sortFields = $this->getSort();
        $this->filterFields = $this->getFilter();
        $this->selectFields = $this->getSelect();
        $elementIterator = $this->getElementList();
        $this->getIblockElements($elementIterator);

        if (!empty($elementIterator))
        {
            $this->initNavString($elementIterator);
        }

    }

    /**
     * @brief Формирование сортировки
     * @return массив
     **/
    protected function getSort()
    {
        #arOrder
        $sortFields = array(
            $this->arParams["SORT_BY1"]=>$this->arParams["SORT_ORDER1"],
            $this->arParams["SORT_BY2"]=>$this->arParams["SORT_ORDER2"],
        );
        if(!array_key_exists("ID", $sortFields))
        {
            $sortFields["ID"] = "DESC";
        }

        return $sortFields;
    }

    /**
     * @brief Инициализация Навигации
     * @return массив
     **/
    protected function initNavParams()
    {
        if($this->arParams["DISPLAY_TOP_PAGER"] || $this->arParams["DISPLAY_BOTTOM_PAGER"])
        {
            $this->navParams = array(
                "nPageSize" => $this->arParams["NEWS_COUNT"],
                "bDescPageNumbering" => $this->arParams["PAGER_DESC_NUMBERING"],
                "bShowAll" => $this->arParams["PAGER_SHOW_ALL"],
            );
            $this->navigation = CDBResult::GetNavParams($this->navParams);
            if($this->navigation["PAGEN"]==0 && $this->arParams["PAGER_DESC_NUMBERING_CACHE_TIME"]>0)
                $this->arParams["CACHE_TIME"] = $this->arParams["PAGER_DESC_NUMBERING_CACHE_TIME"];
        }
        else
        {
            $this->navParams = array(
                "nTopCount" => $this->arParams["NEWS_COUNT"],
                "bDescPageNumbering" => $this->arParams["PAGER_DESC_NUMBERING"],
            );
            $this->navigation = false;
        }
    }

    /**
     * @brief Формирование Фильтра
     * @return массив
     **/
    protected function getFilter()
    {
        $filterFields = array (
            "IBLOCK_ID" => $this->arResult["ID"],
            "IBLOCK_LID" => SITE_ID,
            "CHECK_PERMISSIONS" => $this->arParams['CHECK_PERMISSIONS'] ? "Y" : "N",
        );

        if(is_array($this->arrFilter))
        {
            $filterFields = array_merge($filterFields, $this->arrFilter);
        }

        return $filterFields;
    }

    /**
     * @brief Формирование Полей
     * @return массив
     **/
    protected function getSelect()
    {
        $arSelect = array(
            "ID",
            "IBLOCK_ID",
            "IBLOCK_SECTION_ID",
            "NAME",
            "ACTIVE_FROM",
            "TIMESTAMP_X",
            "DETAIL_PAGE_URL",
            "LIST_PAGE_URL",
            "DETAIL_TEXT",
            "DETAIL_TEXT_TYPE",
            "PREVIEW_TEXT",
            "PREVIEW_TEXT_TYPE",
            "PREVIEW_PICTURE",
        );

        $arSelect = array_merge($this->arParams["FIELD_CODE"], $arSelect);

        $bGetProperty = count($this->arParams["PROPERTY_CODE"])>0;
        if($bGetProperty)
        {
            $arSelect[]="PROPERTY_*";
        }

        return $arSelect;
    }

    /**
     * @brief Запрос CIBlockElement::GetList
     * @return массив
     **/
    protected function getElementList()
    {
        $elementIterator = \CIBlockElement::GetList(
            $this->sortFields,
            $this->filterFields,
            false,
            $this->navParams,
            $this->selectFields
        );



        $elementIterator->SetUrlTemplates($this->arParams["DETAIL_URL"], "", $this->arParams["IBLOCK_URL"]);

        return $elementIterator;
    }

    /**
     * @brief Создание маасива $arResult["ITEMS"]
     * @return массив
     **/
    protected function getIblockElements($elementIterator)
    {
        if (!empty($elementIterator))
        {
            while ($element = $elementIterator->GetNextElement()) {
                $this->arResult["ITEMS"][] = $this->processElement($element);
            }
        }
    }

    /**
     * @brief Формирование массива элемента
     * @return массив
     **/
    protected function processElement($obElement)
    {
        $arItem = $obElement->GetFields();

        #Проставим формат даты
        if(strlen($arItem["ACTIVE_FROM"])>0)
            $arItem["DISPLAY_ACTIVE_FROM"] = CIBlockFormatProperties::DateFormat($this->arParams["ACTIVE_DATE_FORMAT"], MakeTimeStamp($arItem["ACTIVE_FROM"], CSite::GetDateFormat()));
        else
            $arItem["DISPLAY_ACTIVE_FROM"] = "";

        #SEO настройки
        $ipropValues = new Iblock\InheritedProperty\ElementValues($arItem["IBLOCK_ID"], $arItem["ID"]);
        $arItem["IPROPERTY_VALUES"] = $ipropValues->getValues();

        #Изображение
        Iblock\Component\Tools::getFieldImageData(
            $arItem,
            array('PREVIEW_PICTURE', 'DETAIL_PICTURE'),
            Iblock\Component\Tools::IPROPERTY_ENTITY_ELEMENT,
            'IPROPERTY_VALUES'
        );

        #Свойства
        $arItem["PROPERTIES"] = $obElement->GetProperties();


        return $arItem;
    }

    /**
     * @brief Создание навигации
     * @return массив
     **/
    protected function initNavString(\CIBlockResult $elementIterator)
    {
        $navComponentParameters = array();

        if ($this->arParams['PAGER_BASE_LINK_ENABLE'] === 'Y')
        {
            $pagerBaseLink = trim($this->arParams['PAGER_BASE_LINK']) ?: $this->arResult['SECTION_PAGE_URL'];

            if ($this->pagerParameters && isset($this->pagerParameters['BASE_LINK']))
            {
                $pagerBaseLink = $this->pagerParameters['BASE_LINK'];
                unset($this->pagerParameters['BASE_LINK']);
            }

            $navComponentParameters['BASE_LINK'] = \CHTTP::urlAddParams($pagerBaseLink, $this->pagerParameters, array('encode' => true));
        }
        else
        {
            $uri = new Main\Web\Uri($this->arParams['CURRENT_BASE_PAGE']);
            $uri->deleteParams(array(
                'PAGEN_'.$elementIterator->NavNum,
                'SIZEN_'.$elementIterator->NavNum,
                'SHOWALL_'.$elementIterator->NavNum,
                'PHPSESSID',
                'clear_cache',
                'bitrix_include_areas'
            ));
            $navComponentParameters['BASE_LINK'] = $uri->getUri();
        }

        $this->arResult['NAV_STRING'] = $elementIterator->GetPageNavStringEx(
            $navComponentObject,
            $this->arParams['PAGER_TITLE'],
            $this->arParams['PAGER_TEMPLATE'],
            $this->arParams['PAGER_SHOW_ALWAYS'],
            $this,
            $navComponentParameters
        );
        $this->arResult['NAV_CACHED_DATA'] = null;
        $this->arResult['NAV_RESULT'] = $elementIterator;
        $this->arResult['NAV_PARAM'] = $navComponentParameters;
    }

    /**
     * @brief Обработка ошибок
     * @return массив
     **/
    protected function actionMessage()
    {
        $this->arResult["MESSAGE_ERROR"] = $this->MessageError;
        $this->arResult["MESSAGE_SEND"] = $this->MessageSend;
    }

    public function executeComponent()
    {
        try{
            CPageOption::SetOptionString("main", "nav_page_in_session", "N");

            $this -> arResult["COMPONENT_ID"] = 'CQL';
            $this -> includeComponentLang("class.php");
            $this -> checkModules();
            $this -> checkOptions();
            $this -> getResult();
            $this -> actionMessage();

            $this->includeComponentTemplate();
        }catch (Exception $e){
            ShowError($e->getMessage());
        }
    }

};