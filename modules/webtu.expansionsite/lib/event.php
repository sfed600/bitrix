<?php

namespace Webtu\ExpansionSite;

use Bitrix\Main;
use Bitrix\Main\Config\Option;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale;
use Webtu\ExpansionSite\Geoip\GeoIpBase;

Loc::loadMessages(__FILE__);

class Event
{

    /**
     * @brief Вызывается перед отправкой сообщения (подменяем данные)
     *
     * @param $arFields
     * @param $arTemplate
     */
    static public function OnBeforeEventSendHandler(&$arFields, &$arTemplate)
    {
        if ($arTemplate["EVENT_NAME"] == "NEW_USER_CONFIRM") {
            $arOptions = Handler::getOptions();

            if ($_SESSION["CITY_INFO"]["sCurCity"] != $arOptions["GEOIP"]["CITY_DEFAULT"]) {
                $server_name = $_SESSION["CITY_INFO"]["sCurCity"] . '.' . $_SESSION["CITY_INFO"]["domain_main"];
                $message_php = $arTemplate["MESSAGE_PHP"];
                $arTemplate["MESSAGE_PHP"] = str_replace('<?=$arParams["SERVER_NAME"];?>', $server_name, $message_php);;
            }
        }
    }

    /**
     * @param $ID
     * @param $VAL
     * @throws Main\ArgumentException
     * @throws Main\ArgumentNullException
     * @throws Main\LoaderException
     * @throws Main\NotImplementedException
     * @throws Main\ObjectException
     * @throws Main\ObjectNotFoundException
     */
    static public function OnSalePayOrderHandler($ID, $VAL)
    {

        if (intval($ID) == 0 or empty($VAL)) {
            return;
        }

        if (!Loader::includeModule("sale")) {
            return;
        }

        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        $order = Sale\Order::load($ID);

        if ($order) {
            #Если заказ оплачен
            if ($VAL == "Y") {
                $certificate = new \Webtu\ExpansionSite\Certificate();
                $result = $certificate->generateCertificate($ID);

                #Уведомление организатору об оплате бронирования
                $order_webtu = new \Webtu\ExpansionSite\Order();
                $order_webtu->overpaymentBooking($ID);
            }

            $listOrders = $order->getFieldValues();

            #Изменим статус заказа
            if ($VAL == "Y" and $listOrders["STATUS_ID"] != "P") {
                \CSaleOrder::StatusOrder($ID, "P");

                #Начисление бонусов
                if (
                    $arParams["BONUS"]["CASHBACK"] == "Y" &&
                    intval($arParams["BONUS"]["CASHBACK_MIN"]) > 0
                ) {
                    Bonus::userPayOrder($ID);
                }


            } else if ($VAL == "N" and $listOrders["STATUS_ID"] != "N") {
                \CSaleOrder::StatusOrder($ID, "N");
            }
        }
    }


    /**
     * @brief Вызывается перед отправкой письма о новом заказе, может быть использовано для модификации данных
     * @param $orderID
     * @param $eventName
     * @param $arFields
     * @throws Main\ArgumentNullException
     * @throws Main\LoaderException
     * @throws Main\NotImplementedException
     */
    static public function OnOrderNewSendEmailHandler($orderID, &$eventName, &$arFields)
    {
        if (!Loader::includeModule("sale") || !Loader::includeModule("iblock")) {
            return;
        }

        if (
            $orderID > 0
            && $eventName == "SALE_NEW_ORDER"
        ) {
            $arParams = Handler::getOptions();

            $order = \Bitrix\Sale\Order::load($orderID);

            if ($order) {

                $orderWebtu = new \Webtu\ExpansionSite\Order();
                $orderWebtu->order = $order;

                $arResult["QUEST_ID"] = $orderWebtu->getOrderProperty("ID_QUEST");
                $arResult["ID_ORGANIZATION"] = $orderWebtu->getOrderProperty("ID_ORGANIZATION");

                if ($arResult["QUEST_ID"] && $arResult["ID_ORGANIZATION"] > 0) {
                    $arFields["BOOKING_DATETIME"] = $orderWebtu->getOrderProperty("DATE_BOOKING");
                    $arFields["COUNT_PLAYERS"] = $orderWebtu->getOrderProperty("PLAYERS_COUNT");
                    $arFields["COMMENT"] = $order->getField("USER_DESCRIPTION");

                    if (empty($arFields["COMMENT"])) {
                        $arFields["COMMENT"] = 'не указан';
                    }

                    $rsUser = \CUser::GetByID($arResult["ID_ORGANIZATION"]);
                    $arUserOrg = $rsUser->Fetch();

                    $arFields["WORK_PHONE"] = ($arUserOrg["WORK_PHONE"]) ? $arUserOrg["WORK_PHONE"] : 'не указан';

                    if ($arParams["QUEST"]["CATALOG_IBLOCK_ID"] > 0) {
                        $el = new \CIBlockElement();
                        $arFilter = Array(
                            "IBLOCK_ID" => $arParams["QUEST"]["CATALOG_IBLOCK_ID"],
                            "ID" => $arResult["QUEST_ID"],
                        );

                        $arSelect = Array(
                            "ID",
                            "NAME",
                            "CODE",
                            "PROPERTY_CITY",
                            "PROPERTY_ADDRESS_CITY",
                            "PROPERTY_ADDRESS_STREET",
                            "PROPERTY_ADDRESS_HOUSE",
                            "PROPERTY_ADDRESS_CORP",
                            "PROPERTY_ADDRESS_OFFICE",
                            "PROPERTY_METRO",
                        );

                        $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                        $arFieldsQuest = $res->Fetch();

                        $arFields["QUEST_NAME"] = ($arFieldsQuest["NAME"]) ? $arFieldsQuest["NAME"] : $arFields["ORDER_LIST"];
                        $arFields["QUEST_URL"] = ($arFieldsQuest["CODE"]) ? '/poisk-kvestov/' . $arFieldsQuest["CODE"] . '/' : '';

                        $address = '';

                        if (strlen($arFieldsQuest["PROPERTY_ADDRESS_CITY_VALUE"]) > 0) {
                            $address .= $arFieldsQuest["PROPERTY_ADDRESS_CITY_VALUE"];
                        }

                        if (strlen($arFieldsQuest["PROPERTY_METRO_VALUE"]) > 0) {
                            $address .= ', ' . $arFieldsQuest["PROPERTY_METRO_VALUE"];
                        }

                        if (strlen($arFieldsQuest["PROPERTY_ADDRESS_STREET_VALUE"]) > 0) {
                            $address .= ', ' . $arFieldsQuest["PROPERTY_ADDRESS_STREET_VALUE"];
                        }

                        if (strlen($arFieldsQuest["PROPERTY_ADDRESS_HOUSE_VALUE"]) > 0) {
                            $address .= ' ' . $arFieldsQuest["PROPERTY_ADDRESS_HOUSE_VALUE"];
                        }

                        if (strlen($arFieldsQuest["PROPERTY_ADDRESS_CORP_VALUE"]) > 0) {
                            $address .= '/' . $arFieldsQuest["PROPERTY_ADDRESS_CORP_VALUE"];
                        }

                        if (strlen($arFieldsQuest["PROPERTY_ADDRESS_OFFICE_VALUE"]) > 0) {
                            $address .= ' офис ' . $arFieldsQuest["PROPERTY_ADDRESS_OFFICE_VALUE"];
                        }

                        $arFields["ADDRESS"] = ($address) ? $address : 'не указан';

                    }
                } else {
                    #Почтовое событие для других заказов (не бронировнаия квестов)
                    $eventName = "SALE_NEW_ORDER_BITRIX";
                }

            }

        }
    }

    /**
     * @brief Вызывается после изменения флага отмены заказа
     **/
    static public function OnSaleCancelOrderHandler($orderId, $value, $description)
    {
        if (!Loader::includeModule("sale")) {
            return;
        }

        #Изменяем статус заказа
        $db_sales = \CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), Array("ID" => $orderId));
        while ($ar_sales = $db_sales->Fetch()) {
            $SaleOrderId = $ar_sales;
        }

        if ($value == "Y" and $SaleOrderId["STATUS_ID"] != "C") {
            \CSaleOrder::StatusOrder($orderId, "C");
        } else if ($value == "N" and $SaleOrderId["STATUS_ID"] != "N") {
            \CSaleOrder::StatusOrder($orderId, "N");
        }
    }

    /**
     * @brief Перед обновлением регистрации полей пользователя
     **/
    static public function OnBeforeUserRegisterHandler(&$arFields)
    {
        $arParams = Handler::getOptions();

        $arFields["LOGIN"] = $arFields["EMAIL"];

        #Если регистрируется простой пользователь
        if ($arFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["BONUS"]["TYPE_USER_VALUE"]) {
            #Статус пользователя новичок
            $arFields["UF_STATUS"] = 6;

            #Если в настройках модуля установлено назначать CashBack
            if ($arParams["BONUS"]["CASHBACK"]) {
                #Назначаем минимальный CashBack пользователю при регистрации
                $arFields[$arParams["PROPS"]["CASHBACK"]["CODE"]] = $arParams["BONUS"]["CASHBACK_MIN"];
            }

            $arFields["WORK_POSITION"] = "Пользователь";

        }

        #Если регистрируется организация
        if ($arFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["USERS"]["ACCESS"]["LEVEL_2"]) {
            $arFields["NAME"] = "Организация";
            $arFields["LAST_NAME"] = $arFields["WORK_DEPARTMENT"];
            $arFields["WORK_POSITION"] = "Организатор квестов";
        }

        return $arFields;
    }

    /**
     * @brief Вызывается перед изменением параметров пользователя.
     **/
    static public function OnBeforeUserUpdateHandler(&$arFields)
    {

        $arFields["LOGIN"] = $arFields["EMAIL"];


        $arParams = Handler::getOptions();

        #Если  пользователь
        if ($arFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["BONUS"]["TYPE_USER_VALUE"]) {
            $arFields["WORK_POSITION"] = "Пользователь";
        }

        #Если тип пользователь - организация
        if ($arFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["USERS"]["ACCESS"]["LEVEL_2"]) {
            $arFields["NAME"] = "Организатор";
            $arFields["LAST_NAME"] = $arFields["WORK_DEPARTMENT"];
            $arFields["WORK_POSITION"] = "Организатор квестов";

            $arParams_trans = array("replace_space" => "-", "replace_other" => "-");
            $trans = \Cutil::translit($arFields["WORK_DEPARTMENT"] . '_' . $arFields["ID"], "ru", $arParams_trans);

            $arFields["UF_ORGANIZATION_CODE"] = $trans;
        }

    }

    /**
     * @brief Вызывается после изменением параметров пользователя.
     **/
    static public function OnAfterUserUpdateHandler(&$arFields)
    {
        $arParams = Handler::getOptions();

        #Если тип пользователь - организация
        if ($arFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["USERS"]["ACCESS"]["LEVEL_2"]) {
            if ($arFields["ACTIVE"] == 'Y' && $arFields['UF_ORG_ALERT'] == '1') {

                $arEventFields = array(
                    "EMAIL" => $arFields["EMAIL"],
                    "LOGIN" => $arFields["LOGIN"],
                    "PASSWORD" => $arParams["USERS"]["PASSWORD"],
                    "NAME" => $arFields["NAME"],
                    "LAST_NAME" => $arFields["LAST_NAME"]
                );

                \CEvent::Send("NEW_ORGANIZATION_CONFIRM", 's1', $arEventFields);
            }
        }

        return $arFields;
    }

    /**
     * @brief Событие "OnPageStartHandler" вызывается части пролога сайта, после подключения всех библиотек и отработки агентов.
     **/
    static public function OnPageStartHandler()
    {
        if (
            strpos($_SERVER['REQUEST_URI'], "/bitrix/admin/") === false &&
            strpos($_SERVER['REQUEST_URI'], "/bitrix/components/") === false
        ) {
            #Получить все настройки модуля
            $arParams = Handler::getOptions();

            #Начисление бонусов за регистрацию
            if (
                $arParams["BONUS"]["EVENT_REGISTER"] == "Y" &&
                intval($arParams["BONUS"]["EVENT_REGISTER_SUM"]) > 0
            ) {
                #Вызывается до попытки зарегистрировать нового пользователя
                \Bitrix\Main\EventManager::getInstance()->addEventHandler('main', 'OnAfterUserRegister', array("\Webtu\ExpansionSite\Bonus", "userRegister"));
            }

            #Начисление бонусов за приглашение друга
            if (
                $arParams["BONUS"]["EVENT_REGISTER_INVITE"] == "Y" &&
                intval($arParams["BONUS"]["EVENT_REGISTER_INVITE_SUM"]) > 0 &&
                intval($_SESSION["BONUS_REGISTER"]["USER_ID"]) > 0
            ) {
                #Вызывается до попытки зарегистрировать нового пользователя
                \Bitrix\Main\EventManager::getInstance()->addEventHandler('main', 'OnAfterUserRegister', array("\Webtu\ExpansionSite\Bonus", "userRegisterInvite"));
            }

            #Заявка вступить в команду
            if (intval($_SESSION["INVITE"]["TEAM_ID"]) > 0) {
                #Вызывается до попытки зарегистрировать нового пользователя
                \Bitrix\Main\EventManager::getInstance()->addEventHandler('main', 'OnAfterUserRegister', array("\Webtu\ExpansionSite\Notify", "userRegisterNotify"));
            }

        }
    }

    /**
     * @brief Событие "OnProlog" вызывается в начале визуальной части пролога сайта.
     **/
    static public function OnPrologHandler()
    {
        #ГЕО, ACCESS Mobile_Detect
        if (
            strpos($_SERVER['REQUEST_URI'], "/bitrix/admin/") === false &&
            strpos($_SERVER['REQUEST_URI'], "/bitrix/components/") === false
        ) {
            global $USER;
            global $APPLICATION;

            $GLOBALS['isUserAgent'] = Handler::isUserAgentPageSpeed();

            $arResult = array();

            if (!Loader::IncludeModule("iblock")) {
                return;
            }

            #Получить все настройки модуля
            $arParams = Handler::getOptions();
            #получаем основной домен
            $_SESSION["CITY_INFO"]["domain_main"] = Option::get("main", "server_name");
            #установим глобальную переменную cur_city = код города (из сабдомена)
            $_SESSION["CITY_INFO"]["sCurCity"] = preg_replace('/^(?:([^\.]+)\.)?' . str_replace('.', '\.', $_SESSION["CITY_INFO"]["domain_main"]) . '$/', '\1', $_SERVER['SERVER_NAME']);

            if ($arParams["GEOIP"]["IBLOCK_ID"] > 0 && strlen($_SESSION["CITY_INFO"]["sCurCity"]) > 0) {
                $el = new \CIBlockElement;
                $arFilter = array(
                    "IBLOCK_ID" => $arParams["GEOIP"]["IBLOCK_ID"],
                    "ACTIVE" => "Y",
                    "=CODE" => $_SESSION["CITY_INFO"]["sCurCity"]

                );

                $resCount = $el->GetList(Array(), $arFilter, true, false, array());

                if ($resCount == 0 || $_SESSION["CITY_INFO"]["sCurCity"] == 'moscow') {
                    http_response_code(301);
                    header('Location: https://' . $_SESSION["CITY_INFO"]["domain_main"] . $_SERVER['REQUEST_URI']);
                    exit;
                }
            }


            #установим в cur_city город по умолчанию (если нет сабдомена)
            if (empty($_SESSION["CITY_INFO"]["sCurCity"]) && !empty($arParams["GEOIP"]["CITY_DEFAULT"])) {
                $_SESSION["CITY_INFO"]["sCurCity"] = $arParams["GEOIP"]["CITY_DEFAULT"];
            }

            $GeoIpBase = new GeoIpBase();
            #Если данные не установлены
            if (!$_SESSION["CITY_INFO"]["GEOIP"]) {
                #Запросим ГЕО данные
                $arGeoIpBase = $GeoIpBase->getResult();

                #Установим значение сессии
                Handler::setCitySession($arGeoIpBase);
            }

            #Если другой город
            if ($_SESSION["CITY_INFO"]["sCurCity"] != $_SESSION["CITY_INFO"]["GEOIP"]["NEAREST_DOT"]["CODE"]) {
                #Запросим ГЕО данные
                $arGeoIpBase = $GeoIpBase->getResult($_SESSION["CITY_INFO"]["sCurCity"]);
                if (!$arGeoIpBase["NEAREST_DOT"]) {
                    $arGeoIpBase = $GeoIpBase->getResult();
                }
                #Установим значение сессии
                Handler::setCitySession($arGeoIpBase);
            }

            #Установим Id города в глобальную переменную для фильтра
            $GLOBALS['arrFilterCity'] = array('PROPERTY_CITY' => $_SESSION["CITY_INFO"]["GEOIP"]["NEAREST_DOT"]["ID"]);

            #Установим фильтр для квестов
            $GLOBALS['arrFilterQuest'] = array(
                'PROPERTY_CITY' => $_SESSION["CITY_INFO"]["GEOIP"]["NEAREST_DOT"]["ID"],
                array(
                    array('=PROPERTY_STATUS_PUBLISHED' => $arParams["QUEST"]["STATUS"]["ACTIVE"]),
                    //array('=PROPERTY_STATUS_PUBLISHED' => $arParams["QUEST"]["STATUS"]["DISABLED"]),
                ),

            );

            if ($USER->IsAuthorized() && intval($_SESSION["USER_ACCESS"]["TYPE_USER"]["ACCESS_MAX_LEVEL"]) == 0) {
                #Установим доступ пользователя
                Handler::setAccessUser();
            } else if (!$USER->IsAuthorized() && intval($_SESSION["USER_ACCESS"]["TYPE_USER"]["ACCESS_MAX_LEVEL"]) > 0) {
                #Удалим доступ пользователя
                Handler::onSetAccessUser();
            }

            #Переход по реферальной ссылке
            $parts = parse_url($_SERVER["HTTP_REFERER"]);
            parse_str($parts['query'], $query);

            if (strlen($query['invite']) > 0) {
                $user_id_with_hash = base64_decode($query['invite']);

                $user_id = (int)str_replace($arParams['BONUS']['PARTNER_HASH'], '', $user_id_with_hash);

                if (
                    !$USER->IsAuthorized()
                    && $user_id > 0
                ) {
                    $_SESSION["BONUS_REGISTER"]["USER_ID"] = $user_id;
                }
            }

            #Приглашение вспупить в команду
            if (strlen($query['invite_team']) > 0) {
                $team_id_with_hash = base64_decode($query['invite_team']);

                $team_id = (int)str_replace($arParams['BONUS']['PARTNER_HASH'], '', $team_id_with_hash);

                if (
                    $team_id > 0
                    && $arParams["TEAM"]["IBLOCK_ID"] > 0
                ) {

                    if ($USER->IsAuthorized()) {
                        $current_user_id = $USER->GetID();

                        $notify = new Notify();

                        $notify->notifyByCaptain($team_id, $current_user_id);

                    } else {
                        $el = new \CIBlockElement();

                        $arFilter = Array(
                            "IBLOCK_ID" => $arParams["TEAM"]["IBLOCK_ID"],
                            "ID" => $team_id,
                        );

                        $arSelect = Array();

                        $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                        $captain_id = 0;

                        while ($ob = $res->GetNextElement()) {
                            $arProps = $ob->GetProperties();
                            $captain_id = $arProps["CAPTAIN"]["VALUE"];
                        }


                        $_SESSION["INVITE"]["TEAM_ID"] = $team_id;

                        if ($captain_id > 0) {
                            $_SESSION["BONUS_REGISTER"]["USER_ID"] = $captain_id;
                        }
                    }
                }
            }

            $page = \Bitrix\Main\Context::getCurrent()->getRequest()->getRequestedPage();

            if (strlen($page) > 0) {
                $metaSeo = new MetaSeo();
                $metaSeo::setSeoParams($page);
            }
        }
    }

    /**
     * @brief Событие "OnEpilog" вызывается в конце визуальной части эпилога сайта.
     **/
    static public function OnEpilogHandler()
    {
        global $APPLICATION;

        if (strlen($GLOBALS['SEO_DATA']['META_TITLE']) > 0) {
            $APPLICATION->SetPageProperty("title", $GLOBALS['SEO_DATA']['META_TITLE']);
        }

        if (strlen($GLOBALS['SEO_DATA']['META_KEYWORDS']) > 0) {
            $APPLICATION->SetPageProperty("keywords", $GLOBALS['SEO_DATA']['META_KEYWORDS']);
        }

        if (strlen($GLOBALS['SEO_DATA']['META_DESCRIPTION']) > 0) {
            $APPLICATION->SetPageProperty("description", $GLOBALS['SEO_DATA']['META_DESCRIPTION']);
        }

        if (strlen($GLOBALS['SEO_DATA']['META_H1']) > 0) {
            if (strlen($GLOBALS['SEO_DATA']['URL']) > 0) {
                $APPLICATION->AddChainItem($GLOBALS['SEO_DATA']['META_H1'], $GLOBALS['SEO_DATA']['URL']);
            }
        } else {
            if (
                strlen($GLOBALS['SEO_DATA']['URL']) > 0
                && strlen($GLOBALS['SEO_DATA']['META_TITLE']) > 0
            ) {
                $APPLICATION->AddChainItem($GLOBALS['SEO_DATA']['META_TITLE'], $GLOBALS['SEO_DATA']['URL']);
            }
        }


        return true;
    }

    /**
     * @param $module
     * @param $tag
     * @param $value
     * @param $arNotify
     * @return bool
     * @throws Main\LoaderException
     */
    static public function OnBeforeConfirmNotifyHandler($module, $tag, $value, $arNotify)
    {
        if ($module == "webtu.expansionsite") {
            $arOptions = Handler::getOptions();

            $arTag = explode("|", $tag);

            if ($arTag[0] == "TEAM_INVITE" || $arTag[0] == "TEAM_INVITE_CAPTAIN") {
                #ID пользователя, которого приглашаем в команду
                $user_id = (int)$arTag[2];
                #ID команды
                $team_id = (int)$arTag[1];

                if (
                    $user_id > 0
                    && $team_id > 0
                    && $arOptions["TEAM"]["IBLOCK_ID"] > 0
                ) {
                    $notify = new Notify();
                    $notify->notifyByConfirm($team_id, $user_id, $value, $arTag[0]);

                    return true;
                }
            } else if ($arTag[0] == "USER_JOIN") {
                #ID инбоблока
                $iblock_id = (int)$arTag[2];
                #ID заявки
                $element_id = (int)$arTag[1];
                #ID пользователя
                $user_id = (int)$arTag[3];

                if (
                    $user_id > 0
                    && $iblock_id > 0
                    && $element_id > 0
                ) {
                    $notify = new Notify();
                    $notify->notifyUserJoin($element_id, $iblock_id, $user_id, $value);

                    return true;
                }

            } else {
                return true;
            }

        } else {
            return true;
        }
    }

    /**
     *
     *
     * @param $accountId
     * @return bool
     * @throws Main\LoaderException
     */
    static public function OnAfterUserAccountUpdate($accountId)
    {
        if (!Loader::includeModule("sale")) {
            return false;
        }

        $arParams = Handler::getOptions();

        $arUserAccount = \CSaleUserAccount::GetByID($accountId);

        $rsUser = \CUser::GetByID($arUserAccount["USER_ID"]);
        $arUser = $rsUser->Fetch();

        #Если тип пользователь - организация
        if ($arUser[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["USERS"]["ACCESS"]["LEVEL_2"]) {
            $currentBudget = floatval($arUserAccount["CURRENT_BUDGET"]);
            
            if ($currentBudget < 0) {
                Bonus::resetQuestPopularityRating($arUser["ID"]);
            } else {
                Bonus::returnQuestPopularityRating($arUser["ID"]);
            }
        }
    }

}