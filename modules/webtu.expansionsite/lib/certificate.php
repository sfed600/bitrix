<?php

namespace Webtu\ExpansionSite;

require_once $_SERVER['DOCUMENT_ROOT'].'/local/modules/webtu.expansionsite/plugins/dompdf/autoload.inc.php';

use \Bitrix\Main;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Webtu\ExpansionSite\Handler;

use \Dompdf\Dompdf;
use \Dompdf\Options;

Loc::loadMessages(__FILE__);


class Certificate
{
    private $arResult = array();

    public function __construct(){}

    /**
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('sale'))
        {
            throw new Main\LoaderException( Loc::getMessage('CERTIFICATE_NOT_INSTALLED_MODULE', Array ("#ID#" => "sale")) );
        }

        if (!Loader::includeModule('iblock'))
        {
            throw new Main\LoaderException( Loc::getMessage('CERTIFICATE_NOT_INSTALLED_MODULE', Array ("#ID#" => "iblock")) );
        }

        if (!Loader::includeModule('webtu.expansionsite'))
        {
            throw new Main\LoaderException(Loc::getMessage('CERTIFICATE_NOT_INSTALLED_MODULE', Array ("#ID#" => "webtu.expansionsite")) );
        }
    }


    /**
     * @param int $orderID
     * @return Main\Entity\AddResult|bool
     * @throws Main\ArgumentException
     * @throws Main\ArgumentNullException
     * @throws Main\LoaderException
     * @throws Main\NotImplementedException
     * @throws Main\ObjectException
     */
    public function generateCertificate($orderID = 0)
    {
        $this->checkModules();

        $arOptions = Handler::getOptions();

        if (
            $orderID <= 0
            && $arOptions["CERTIFICATE"]["IBLOCK_ID"] <= 0
        )
        {
            return false;
        }

        $el = new \CIBlockElement();

        $filter = array(
            "ORDER_ID" => $orderID
        );

        $this->arResult["ITEM"] = $this->getCoupon($filter);

        if (
            $this->arResult["ITEM"]["ID"] <= 0
            && strlen($this->arResult["ITEM"]["COUPON"]) == 0
            && $this->arResult["ITEM"]["ORDER_ID"] != $orderID
            && $this->arResult["ITEM"]["CERTIFICATE_ID"] <= 0
        )
        {
            return false;
        }

        $orderObj = \Bitrix\Sale\Order::load($orderID);
        $propertyCollection = $orderObj->getPropertyCollection();

        $emailProperty = $this->getPropertyByCode($propertyCollection, 'EMAIL');

        $this->arResult["PDF"]["EMAIL_TO"] = $emailProperty->getValue();

        $arFilter = array(
            "IBLOCK_ID" => $arOptions["CERTIFICATE"]["IBLOCK_ID"],
            "ID" => $this->arResult["ITEM"]["CERTIFICATE_ID"],
            "ACTIVE" => "Y",
        );

        $arSelect = Array(
            "ID",
            "NAME",
            "PREVIEW_PICTURE",
            "PROPERTY_COLOR_TEXT",
        );

        $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();

            $this->arResult["PDF"]["NAME"] = $arFields["NAME"];
            $this->arResult["PDF"]["BACKGROUND_IMG"] = \CFile::GetPath($arFields["PREVIEW_PICTURE"]);
            $this->arResult["PDF"]["COLOR_TEXT"] = $arFields["PROPERTY_COLOR_TEXT_VALUE"];
        }

        $this->arResult["PDF"]["COUPON"] = $this->arResult["ITEM"]["COUPON"];

        $objDateTime = new \Bitrix\Main\Type\DateTime($this->arResult["ITEM"]["DATE_ACTUAL_TO"]);
        $this->arResult["PDF"]["DATE_ACTUAL_TO"] = \FormatDate('d F Y', $objDateTime->getTimestamp());

        $this->arResult["SRC"] = $this->createPDF( $this->arResult["PDF"]);

        $result_event = \Bitrix\Main\Mail\Event::send(array(
            "EVENT_NAME" => "WEBTU_CERTIFICATE",
            "LID" => "s1",
            "C_FIELDS" => $this->arResult["PDF"],
            "FILE" => array($this->arResult["SRC"]),
        ));

        return $result_event->getId();

    }

    /**
     * @param array $filter
     * @return array|bool|mixed
     * @throws Main\ArgumentException
     * @throws Main\LoaderException
     */
    public function getCoupon($filter = array())
    {
        $this->checkModules();

        if (count($filter) == 0) { return false; }

        $arResult = array();

        #Получим значение сертификата
        $resultList = \Webtu\ExpansionSite\Mysql\CouponListTable::getList(array(
            'select'  => array("*"),
            'filter'  => array($filter),
            'group'   => array(),
            'order'   => array('ID'=>'DESC'),
            'limit'   => "",
            'offset'  => "",
        ));

        if ($arResult = $resultList->fetch())
        {
            return $arResult;
        }
    }

    /**
     * @param $propertyCollection
     * @param $code
     * @return mixed
     */
    protected function getPropertyByCode($propertyCollection, $code)
    {
        foreach ($propertyCollection as $property)
        {
            if($property->getField('CODE') == $code)
                return $property;
        }
    }

    protected function createPDF($arFields = array())
    {
        if (empty($arFields)) { return false; }

        $options = new Options();
        $dompdf = new Dompdf($options);

        $html_pdf = '<!doctype html><html lang="ru">';
        $html_pdf .= '<head>';
        $html_pdf .= '<meta http-equiv="Content-Type" content="text/html; charset=utf-8">';
        $html_pdf .= '<style>';
        $html_pdf .= '@page { size: 980px 1020px; margin: 0 0 0 0; }';
        $html_pdf .= '</style>';
        $html_pdf .= '</head>';
        $html_pdf .= '<body>';
        $html_pdf .= '<div style="background: url('.$arFields["BACKGROUND_IMG"].'); height: 1020px; position: relative; width: 980px;">';
        $html_pdf .= '<div style="color: #15151e; font: bold 26px/1 sans-serif; left: 333px; position: absolute; right: 333px; top: 557px; text-align: center;">';
        $html_pdf .= $arFields["COUPON"];
	    $html_pdf .= '</div>';
	    $html_pdf .= '<div style="color: '.$arFields["COLOR_TEXT"].'; font: bold 18px/1 sans-serif; left: 364px; position: absolute; right: 364px; top: 627px; text-align: center;">';
        $html_pdf .= $arFields["DATE_ACTUAL_TO"];
        $html_pdf .= '</div>';
        $html_pdf .= '</div>';
        $html_pdf .= '</body>';
        $html_pdf .= '</html>';

        $dompdf->loadHtml($html_pdf, 'UTF-8');

        $dompdf->render();

        $output = $dompdf->output();

        $filename = date("Y-m-d-H-i-s").'-'.uniqid().'.pdf';

        $filepath = $_SERVER['DOCUMENT_ROOT'].'/upload/sale/pdf/'.$filename;

        file_put_contents( $filepath, $output);

        return $filepath;


    }
}