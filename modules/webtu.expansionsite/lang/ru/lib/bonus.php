<?
$MESS["WEBTU_ES_MESSAGE_EVENT_REGISTER"] = "Начисление #SUMM# бонусов за регистрацию на сайте.";
$MESS["WEBTU_ES_MESSAGE_EVENT_REGISTER_INVITE"] = "Начисление #SUMM# бонусов за регистрацию на сайте по приглашению.";
$MESS["WEBTU_ES_MESSAGE_DEDUCT_REGISTER"] = "Сгорание #SUMM# бонусов за регистрацию на сайте.";
$MESS["WEBTU_ES_MESSAGE_DEDUCT_REGISTER_INVITE"] = "Сгорание #SUMM# бонусов за регистрацию на сайте по приглашению.";
$MESS["WEBTU_ES_MESSAGE_EVENT_PAY_ORDER"] = "Начисление #SUMM# бонусов за оплаченный заказа №#ORDER_ID#.";
$MESS["WEBTU_ES_MESSAGE_DEDUCT_PAY_ORDER"] = "Сгорание #SUMM# бонусов за оплаченный заказа №#ORDER_ID#.";
$MESS["WEBTU_ES_MESSAGE_EVENT_DEFAULT"] = "Начисление #SUMM# бонусов.";
$MESS["WEBTU_ES_MESSAGE_DEDUCT_DEFAULT"] = "Сгорание #SUMM# бонусов.";
$MESS["WEBTU_ES_ERROR_CLASS"] = "/Webtu/ExpansionSite/Bonus::#METOD# ERROR #MESSAGE#";
$MESS["WEBTU_ES_MESSAGE_EVENT_USER_REGISTER_INVITE"] = "Начисление #SUMM# бонусов за регистрацию на сайте по приглашению пользователя #NAME#.";
$MESS["WEBTU_ES_MESSAGE_DEDUCT_USER_REGISTER_INVITE"] = "Сгорание #SUMM# бонусов за регистрацию на сайте по приглашению пользователя #NAME#.";
$MESS["WEBTU_ES_MESSAGE_EVENT_SUNC_SOCIAL_NETWORK"] = "Начисление #SUMM# бонусов за привязку к аккаунту социальных сетей.";
$MESS["WEBTU_ES_MESSAGE_DEDUCT_EVENT_SUNC_SOCIAL_NETWORK"] = "Сгорание #SUMM# бонусов за привязку к аккаунту социальных сетей.";

?>