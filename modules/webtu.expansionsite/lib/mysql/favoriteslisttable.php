<?php
namespace Webtu\ExpansionSite\Mysql;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class FavoritesListTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'webtu_favorites_list';
    }

    public static function getUfId()
    {
        return 'WEBTU_FAVORITES_LIST';
    }

    public static function getMap()
    {
        return array(
            // ID
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            // ID user
            new Entity\StringField('USER_ID', array(
                'required' => true
            )),
            // ID infoblock
            new Entity\StringField('IBLOCK_ID', array(
                'required' => true
            )),
            // ID element
            new Entity\StringField('ELEMENT_ID', array(
                'required' => true
            )),
            // date
            new Entity\DatetimeField('DATE_INSERT', array(
                'required' => true,
                'default_value' => new Type\DateTime
            )),
        );
    }
}