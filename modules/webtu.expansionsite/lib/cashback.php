<?php

namespace Webtu\ExpansionSite;

use \Bitrix\Main;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Webtu\ExpansionSite\Handler;

Loc::loadMessages(__FILE__);

class CashBack
{
    private $arResult = array();
    public  $arOptions = array();

    public function __construct(){}

    /**
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock')){
            throw new Main\LoaderException( Loc::getMessage('CASHBACK_NOT_INSTALLED_MODULE', Array ("#ID#" => "iblock")) );
        }

        if (!Loader::includeModule('webtu.expansionsite'))
        {
            throw new Main\LoaderException(Loc::getMessage('CASHBACK_NOT_INSTALLED_MODULE', Array ("#ID#" => "webtu.expansionsite")) );
        }
    }

    /**
     * @throws Main\LoaderException
     */
    public function setUserCashBack()
    {
        $this->checkModules();

        $this->arOptions = Handler::getOptions();

        $el = new \CIBlockElement;

        if (
            $this->arOptions["BONUS"]["CASHBACK"] == "Y"
            && $this->arOptions["TEAM"]["IBLOCK_ID"] > 0
        )
        {
            $arFilter = Array(
                "IBLOCK_ID" => $this->arOptions["TEAM"]["IBLOCK_ID"],
                "ACTIVE" => "Y",
            );

            $arSelect = Array();

            $this->arResult["COUNT_TEAM_ALL"] = $el->GetList(Array(), $arFilter, true , false, $arSelect);

            if ( $this->arResult["COUNT_TEAM_ALL"] > 0 )
            {
                $this->arResult["USERS_CASHBACK"] = $this->getTeamByTotalRating();

                $user = new \CUser();

                foreach ($this->arResult["USERS_CASHBACK"] as $cashback_value => $arUsers)
                {
                    if ($cashback_value <= 0)
                    {
                        $cashback_value = $this->arOptions["BONUS"]["CASHBACK_MIN"];
                    }

                    if ($cashback_value > $this->arOptions["BONUS"]["CASHBACK_MAX"])
                    {
                        $cashback_value = $this->arOptions["BONUS"]["CASHBACK_MAX"];
                    }

                    foreach ($arUsers as $user_id)
                    {
                        if ($user_id > 0 && $cashback_value > 0)
                        {
                            $fields = Array(
                                "UF_CASHBACK" => $cashback_value
                            );
                            $user->Update($user_id, $fields);
                        }
                    }
                }
            }
        }
    }

    /**
     * @return array|bool
     */
    protected function getTeamByTotalRating()
    {
        if ($this->arOptions["TEAM"]["IBLOCK_ID"] <= 0)
        {
            return false;
        }

        $el = new \CIBlockElement;

        $arTeam = $arUsers = $arUsersCashBack = array();

        $arFilter = Array(
            "IBLOCK_ID" => $this->arOptions["TEAM"]["IBLOCK_ID"],
            "ACTIVE" => "Y",
            ">PROPERTY_TOTAL_RATING" => 0
        );

        $arSelect = Array(
            "ID",
            "PROPERTY_CAPTAIN",
            "PROPERTY_TOTAL_RATING"
        );

        $res = $el->GetList(Array("PROPERTY_TOTAL_RATING" => "DESC"), $arFilter, false , false, $arSelect);
        $this->arResult["COUNT_TEAM_RATING"] = $res->SelectedRowsCount();


        if ($this->arResult["COUNT_TEAM_RATING"] > 0)
        {
            $count_team = 0;
            if ($this->arOptions["BONUS"]["CASHBACK_TOP15"] > 0 && $count_team <= $this->arResult["COUNT_TEAM_RATING"] )
            {
                $count_percent_team = round( $this->arResult["COUNT_TEAM_ALL"] * 15 / 100, 0, PHP_ROUND_HALF_UP);

                if ($count_percent_team > 0)
                {
                    $this->arResult["RATING"]["TOP15"]["COUNT_TEAM"] = $count_percent_team;
                    $this->arResult["RATING"]["TOP15"]["CASHBACK"] = $this->arOptions["BONUS"]["CASHBACK_TOP15"];
                    $count_team = $count_team + $this->arResult["RATING"]["TOP15"]["COUNT_TEAM"];
                }

            }

            if ($this->arOptions["BONUS"]["CASHBACK_TOP30"] > 0 && $count_team <= $this->arResult["COUNT_TEAM_RATING"])
            {
                $count_percent_team = round( $this->arResult["COUNT_TEAM_ALL"] * 30 / 100, 0, PHP_ROUND_HALF_UP);

                if ($this->arResult["RATING"]["TOP15"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP15"]["COUNT_TEAM"];
                }

                if ($count_percent_team > 0)
                {
                    $this->arResult["RATING"]["TOP30"]["COUNT_TEAM"] = $count_percent_team;
                    $this->arResult["RATING"]["TOP30"]["CASHBACK"] = $this->arOptions["BONUS"]["CASHBACK_TOP30"];
                    $count_team = $count_team + $this->arResult["RATING"]["TOP30"]["COUNT_TEAM"];
                }


            }

            if ($this->arOptions["BONUS"]["CASHBACK_TOP40"] > 0 && $count_team <= $this->arResult["COUNT_TEAM_RATING"])
            {
                $count_percent_team = round( $this->arResult["COUNT_TEAM_ALL"] * 40 / 100, 0, PHP_ROUND_HALF_UP);

                if ($this->arResult["RATING"]["TOP15"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP15"]["COUNT_TEAM"];
                }

                if ($this->arResult["RATING"]["TOP30"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP30"]["COUNT_TEAM"];
                }

                if ($count_percent_team > 0)
                {
                    $this->arResult["RATING"]["TOP40"]["COUNT_TEAM"] = $count_percent_team;
                    $this->arResult["RATING"]["TOP40"]["CASHBACK"] = $this->arOptions["BONUS"]["CASHBACK_TOP40"];
                    $count_team = $count_team + $this->arResult["RATING"]["TOP40"]["COUNT_TEAM"];
                }

            }

            if ($this->arOptions["BONUS"]["CASHBACK_TOP50"] > 0 && $count_team <= $this->arResult["COUNT_TEAM_RATING"])
            {
                $count_percent_team = round( $this->arResult["COUNT_TEAM_ALL"] * 50 / 100, 0, PHP_ROUND_HALF_UP);

                if ($this->arResult["RATING"]["TOP15"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP15"]["COUNT_TEAM"];
                }

                if ($this->arResult["RATING"]["TOP30"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP30"]["COUNT_TEAM"];
                }

                if ($this->arResult["RATING"]["TOP40"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP40"]["COUNT_TEAM"];
                }

                if ($count_percent_team > 0)
                {
                    $this->arResult["RATING"]["TOP50"]["COUNT_TEAM"] = $count_percent_team;
                    $this->arResult["RATING"]["TOP50"]["CASHBACK"] = $this->arOptions["BONUS"]["CASHBACK_TOP50"];
                    $count_team = $count_team + $this->arResult["RATING"]["TOP50"]["COUNT_TEAM"];
                }

            }

            if ($this->arOptions["BONUS"]["CASHBACK_TOP70"] > 0 && $count_team <= $this->arResult["COUNT_TEAM_RATING"])
            {
                $count_percent_team = round( $this->arResult["COUNT_TEAM_ALL"] * 70 / 100, 0, PHP_ROUND_HALF_UP);

                if ($this->arResult["RATING"]["TOP15"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP15"]["COUNT_TEAM"];
                }

                if ($this->arResult["RATING"]["TOP30"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP30"]["COUNT_TEAM"];
                }

                if ($this->arResult["RATING"]["TOP40"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP40"]["COUNT_TEAM"];
                }

                if ($this->arResult["RATING"]["TOP50"]["COUNT_TEAM"] > 0)
                {
                    $count_percent_team = $count_percent_team - $this->arResult["RATING"]["TOP50"]["COUNT_TEAM"];
                }

                if ($count_percent_team > 0)
                {
                    $this->arResult["RATING"]["TOP70"]["COUNT_TEAM"] = $count_percent_team;
                    $this->arResult["RATING"]["TOP70"]["CASHBACK"] = $this->arOptions["BONUS"]["CASHBACK_TOP70"];
                    $count_team = $count_team + $this->arResult["RATING"]["TOP70"]["COUNT_TEAM"];
                }

            }

            while($ob = $res->GetNextElement())
            {
                $arFields = $ob->GetFields();

                if (!in_array($arFields["PROPERTY_CAPTAIN_VALUE"], $arUsers))
                {
                    $arUsers[] = $arFields["PROPERTY_CAPTAIN_VALUE"];

                    $arTeam[] = array(
                        "ID" => $arFields["ID"],
                        "USER_ID" => $arFields["PROPERTY_CAPTAIN_VALUE"],
                    );
                }

            }

            foreach ($this->arResult["RATING"] as $arRating)
            {
                for ($i=1; $i <= $arRating["COUNT_TEAM"]; $i++)
                {
                    if ($arTeam[$i - 1]["USER_ID"] > 0)
                    {
                        $arUsersCashBack[$arRating["CASHBACK"]][] = $arTeam[$i - 1]["USER_ID"];
                        unset($arTeam[$i - 1]);
                    }

                }

                $arTeam = array_values($arTeam);
            }

            return $arUsersCashBack;
        }
    }
}