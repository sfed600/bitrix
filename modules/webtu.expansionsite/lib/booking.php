<?php

namespace Webtu\ExpansionSite;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main;
use \Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Text\Encoding;
use \Webtu\ExpansionSite\Handler;
use KvestBattle\Booking\QuestSchedule;
use QuestModel;


Loc::loadMessages(__FILE__);

class Booking
{
    private $arResult = array("MESSAGE" => array("ERROR" => array(), "OK" => array(), "RESULT_LOAD" => array()));

    public function __construct()
    {
    }

    /**
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new Main\LoaderException(Loc::getMessage('BOOKING_NOT_INSTALLED_MODULE', Array("#ID#" => "iblock")));
        }

        if (!Loader::includeModule('webtu.expansionsite')) {
            throw new Main\LoaderException(Loc::getMessage('BOOKING_NOT_INSTALLED_MODULE', Array("#ID#" => "webtu.expansionsite")));
        }

        if (!Loader::includeModule('catalog')) {
            throw new Main\LoaderException(Loc::getMessage('BOOKING_NOT_INSTALLED_MODULE', Array("#ID#" => "catalog")));
        }
    }

    /**
     * @param $product_id
     * @param $array
     * @param bool $message
     * @return array|bool
     * @throws Main\LoaderException
     */
    public function getResult($product_id, $array, $message = true)
    {
        $this->checkModules();

        $this->arResult["PRODUCT_ID"] = $product_id;
        $this->arResult["BOOKING"] = $array;


        if (
            !$this->arResult["PRODUCT_ID"] and
            count($this->arResult["BOOKING"]) == 0
        ) {
            return false;
        }

        $arParams = Handler::getOptions();

        #Фильтрация расписания (убираем дубли, прошедшие временные слоты)
        $this->arResult["BOOKING_FILTER"] = $this->filterBookingData($message);

        if (count($this->arResult["BOOKING_FILTER"]) > 0) {
            #Сохранение расписания
            $this->saveBookingData($this->arResult["BOOKING_FILTER"], $message);

            \Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex($arParams["QUEST"]["CATALOG_IBLOCK_ID"], $this->arResult["PRODUCT_ID"]);
        }


        return $this->arResult;

    }

    /**
     * @param $url
     * @param $price
     * @param $product_id
     * @return array|bool
     * @throws Main\LoaderException
     */
    public function getDataCurl($url, $product_id)
    {
        $this->checkModules();

        $this->arResult["URL"] = $url;
        $this->arResult["PRODUCT_ID"] = $product_id;

        if (
            !$this->arResult["URL"] and
            !$this->arResult["PRODUCT_ID"]

        ) {
            array_push($this->arResult["MESSAGE"]["ERROR"], Loc::getMessage('BOOKING_ERROR_NONE_PARAMS'));
        }

        if (count($this->arResult["MESSAGE"]["ERROR"]) == 0) {
            $this->arResult["JSON"] = $this->GetJson();

            if (count($this->arResult["JSON"]) > 0) {
                #Деактивируем все активные временные слоты
                self::bookingInactive($this->arResult["PRODUCT_ID"]);
            }
        }

        return $this->arResult;

    }

    /**
     * @return bool|mixed
     * @throws \Exception
     */
    protected function GetJson()
    {
        if (self::InitBots()) {
            return false;
        }

        $model = (new QuestModel())->getQuestData($this->arResult["PRODUCT_ID"]);
        $slots = new QuestSchedule($model, null);
        $arData = $slots->getSlots();

        return $arData;

    }

    /**
     * @param $quest_id
     * @param bool $is_sale
     * @return bool
     */
    public static function bookingInactive($quest_id, $is_sale = true)
    {
        if ($quest_id <= 0) {
            return false;
        }

        $arParams = Handler::getOptions();

        $el = new \CIBlockElement();

        $arBookingInActive = array();

        #Получим
        $arSelect = Array(
            "ID",
        );

        $arFilter = Array(
            "IBLOCK_ID" => $arParams["QUEST"]["OFFERS_IBLOCK_ID"],
            "ACTIVE" => "Y",
            "ACTIVE_DATE" => "Y",
            "=PROPERTY_CML2_LINK" => $quest_id,
        );

        if ($is_sale) {
            $arFilter["=PROPERTY_IN_SALE"] = false;
            $arFilter["=PROPERTY_RESERVED"] = false;
        }

        $res = $el->GetList(Array(), $arFilter, false, Array(), $arSelect);

        while ($arFields = $res->Fetch()) {
            $arBookingInActive[] = $arFields["ID"];
        }

        $booking_values = '';
        foreach ($arBookingInActive as $key => $booking_id) {
            if ($booking_id > 0) {
                $booking_values .= $booking_id;
                if (count($arBookingInActive) != $key + 1) {
                    $booking_values .= ', ';
                }
            }
        }

        if (count($arBookingInActive) > 0 && strlen($booking_values) > 0) {
            $con = \Bitrix\Main\Application::getConnection();
            $sql = "UPDATE `b_iblock_element` SET `ACTIVE` = 'N' WHERE `ID` IN ({$booking_values})";
            $con->query($sql);
        }
    }

    /**
     * @param $message
     * @return array|bool
     */
    protected function filterBookingData($message)
    {
        if (count($this->arResult["BOOKING"]) == 0) {
            return false;
        }


        $arBookingUnix = array();
        $arBookingsReturn = array();
        $arBookings = array();

        $current_date = date('d.m.Y H:i:s');
        $current_date_unix = strtotime($current_date);

        foreach ($this->arResult["BOOKING"] as $key => $arBooking) {
            $booking_date = $arBooking['date'] . ' ' . $arBooking['time'];
            $booking_date_unix = strtotime($booking_date);

            #Если дата бронирования еще удовлетворяет текущей
            if ($booking_date_unix >= $current_date_unix) {
                $arBookingUnix[$key] = $booking_date_unix;

                $booking_date = date('d.m.Y', $booking_date_unix);
                $booking_time = date('H:i', $booking_date_unix);
                #Цена расписания
                $price = $arBooking['price'];


                if (is_bool($arBooking['is_free'])) {

                    if ($arBooking['is_free']) {
                        $is_free = 'Y';
                    } else {
                        $is_free = 'N';
                    }

                } else {
                    if ($arBooking['is_free'] == 'true' || intval($arBooking['is_free']) == 1) {
                        $is_free = 'Y';
                    } else {
                        $is_free = 'N';
                    }
                }


                $arBookings[$key] = array(
                    'TIMESTAMP' => $booking_date_unix,
                    'DATE' => $booking_date,
                    'TIME' => $booking_time,
                    'PRICE' => $price,
                    'PRODUCT_ID' => $this->arResult["PRODUCT_ID"],
                    'ID_BOOKING_MIRKVESTOV' => $arBooking['quest_time_id'],
                    'IS_FREE' => $is_free,
                    'PRICE_TYPE' => $arBooking['price_type'],
                    'SLOT_ID' => $arBooking['slot_id'] ? $arBooking['slot_id'] : null,
                    'PRICE_GROUPS' => (count($arBooking['price_groups']) > 0) ? json_encode($arBooking['price_groups']) : null,
                    'PRICE_GROUPS_TIMESTAMP' => (isset($arBooking['price_groups_timestamp']) && $arBooking['price_groups_timestamp'] > 0) ? $arBooking['price_groups_timestamp'] : null,
                );

            } else {
                if ($message) {
                    array_push($this->arResult["MESSAGE"]["RESULT_LOAD"], "Расписание на " . date('d.m.Y H:i', $booking_date_unix) . " c ценой " . $arBooking['price'] . " руб. просрочено");
                }
            }
        }

        $arBookingUnixUniq = array_unique($arBookingUnix);

        foreach ($arBookingUnixUniq as $key => $val) {
            $arBookingsReturn[] = $arBookings[$key];
        }

        return $arBookingsReturn;
    }

    /**
     * @param $arBooking
     * @return bool
     */
    public function saveBookingData($arBooking, $message)
    {
        global $USER;
        $arBookingNotActive = array();

        if (count($arBooking) == 0) {
            return false;
        }

        $arParams = Handler::getOptions();

        $el = new \CIBlockElement();

        if ($arParams["QUEST"]["OFFERS_IBLOCK_ID"] > 0) {

            #Полуим все неактивные (прошедшие) расписания
            $arSelect = Array("ID");
            $arFilter = Array(
                "IBLOCK_ID" => $arParams["QUEST"]["OFFERS_IBLOCK_ID"],
                "!ACTIVE_DATE" => "Y",
                "!PROPERTY_DATE" => date("Y-m-d"),
            );

            $res = $el->GetList(Array(), $arFilter, false, Array(), $arSelect);

            while ($arFields = $res->Fetch()) {
                $arBookingNotActive[] = $arFields["ID"];
            }

            #Получим все элементы данного квеста
            $arSelect = Array(
                "ID",
                "ACTIVE",
                "CODE",
                "PROPERTY_IN_SALE",
                "PROPERTY_RESERVED",
                "PROPERTY_SLOT_ID",
                "PROPERTY_PRICE_GROUPS",
                "PROPERTY_PRICE_GROUPS_TIMESTAMP",
            );

            $arFilter = Array(
                "IBLOCK_ID" => $arParams["QUEST"]["OFFERS_IBLOCK_ID"],
                "=PROPERTY_CML2_LINK" => $this->arResult["PRODUCT_ID"],
            );

            $res = \CIBlockElement::GetList(Array(), $arFilter, false, Array(), $arSelect);
            $arItems = array();
            while ($ob = $res->GetNextElement()) {
                $inSale = "N";
                //$inReservSite = "N";

                $arFields = $ob->GetFields();

                if ($arFields["PROPERTY_IN_SALE_ENUM_ID"] == 149) {
                    $inSale = "Y";
                }

                /*if ($arFields["PROPERTY_RESERVED_ENUM_ID"] == 105) {
                    $inReservSite = "Y";
                }*/

                $arItems[$arFields["CODE"]] = array(
                    "ID" => (int)$arFields["ID"],
                    "CODE" => $arFields["CODE"],
                    "ACTIVE" => $arFields["ACTIVE"],
                    "IN_SALE" => $inSale,
                    //"IN_RESERV_SITE" => $inReservSite,
                    "SLOT_ID" => $arFields["PROPERTY_SLOT_ID_VALUE"],
                    "PRICE_GROUPS" => $arFields["~PROPERTY_PRICE_GROUPS_VALUE"],
                    "PRICE_GROUPS_TIMESTAMP" => $arFields["PROPERTY_PRICE_GROUPS_TIMESTAMP_VALUE"],
                );
            }

            foreach ($arBooking as $booking) {
                $arItem = null;

                if ($booking["SLOT_ID"] > 0) {
                    $key = array_search($booking["SLOT_ID"], $arItems);
                    $arItem = $arItems[$key];
                }

                if (is_null($arItem)) {
                    $arItem = $arItems[$booking["TIMESTAMP"]];
                }

                if (!is_null($arItem) && $arItem["ID"] > 0 && $arItem["IN_SALE"] == "N") {

                    if (is_null($booking['SLOT_ID'])) {
                        $booking['SLOT_ID'] = $arItem['SLOT_ID'];
                    }

                    if (is_null($booking['PRICE_GROUPS'])) {
                        $booking['PRICE_GROUPS'] = $arItem['PRICE_GROUPS'];
                    }

                    if (is_null($booking['PRICE_GROUPS_TIMESTAMP'])) {
                        $booking['PRICE_GROUPS_TIMESTAMP'] = $arItem['PRICE_GROUPS_TIMESTAMP'];
                    }

                    if ($arItem["CODE"] == $booking["TIMESTAMP"]) {
                        if ($arItem["ACTIVE"] == "N") {
                            $connection = \Bitrix\Main\Application::getConnection();
                            $sql = "UPDATE `b_iblock_element` SET `ACTIVE` = 'Y' WHERE `ID` = {$arItem["ID"]}";
                            $connection->query($sql);
                        }
                        $resUpdate = true;

                    } else {
                        $addOfferArray = array(
                            "IBLOCK_ID" => $arParams["QUEST"]["OFFERS_IBLOCK_ID"],
                            "ACTIVE" => "Y",
                            "NAME" => $booking["TIME"],
                            "CODE" => $booking["TIMESTAMP"],
                            "ACTIVE_FROM" => date("d.m.Y H:i:s"),
                            "ACTIVE_TO" => date('d.m.Y H:i:s', $booking["TIMESTAMP"]),
                        );
                        $resUpdate = $el->Update($arItem["ID"], $addOfferArray);
                    }

                    if ($resUpdate) {
                        $arPropsFields = array();

                        if ($arItem["CODE"] != $booking["TIMESTAMP"]) {
                            $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("START_TIME" => $booking['TIME']));
                            $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("DATE" => $booking['DATE']));
                        }

                       /* if ($arItem["IN_RESERV_SITE"] == 'N') {*/
                            if ($booking['IS_FREE'] == 'N') {
                                $arPropsFields["RESERVED"] = 105;
                            } else {
                                $arPropsFields["RESERVED"] = false;
                            }

                            $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("RESERVED" => $arPropsFields["RESERVED"]));

                        /*}*/


                        if (strlen($booking['ID_BOOKING_MIRKVESTOV']) > 0) {
                            $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("ID_BOOKING_MIRKVESTOV" => $booking['ID_BOOKING_MIRKVESTOV']));
                        } else {
                            $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("ID_BOOKING_MIRKVESTOV" => ''));
                        }

                        if (strlen($booking['PRICE_TYPE']) > 0) {
                            $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("PRICE_TYPE" => $booking['PRICE_TYPE']));
                        }


                        $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("SLOT_ID" => $booking['SLOT_ID']));
                        $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("PRICE_GROUPS" => $booking['PRICE_GROUPS']));
                        $el->SetPropertyValuesEx($arItem["ID"], $arParams["QUEST"]["OFFERS_IBLOCK_ID"], array("PRICE_GROUPS_TIMESTAMP" => $booking['PRICE_GROUPS_TIMESTAMP']));

                        if ($booking["PRICE"] > 0) {
                            $BASE_PRICE_ID = 1;

                            $res = \CPrice::GetList(
                                array(),
                                array(
                                    "PRODUCT_ID" => $arItem["ID"],
                                    "CATALOG_GROUP_ID" => $BASE_PRICE_ID
                                )
                            );

                            if ($arr = $res->Fetch()) {
                                if ($arr["PRICE"] != $booking["PRICE"]) {
                                    \CPrice::Update(
                                        $arr["ID"],
                                        array(
                                            "PRODUCT_ID" => $arItem["ID"],
                                            "CATALOG_GROUP_ID" => $BASE_PRICE_ID,
                                            "PRICE" => $booking["PRICE"],
                                            "CURRENCY" => 'RUB'
                                        )
                                    );
                                }

                            } else {
                                \CPrice::Add(
                                    array(
                                        "PRODUCT_ID" => $arItem["ID"],
                                        "CATALOG_GROUP_ID" => $BASE_PRICE_ID,
                                        "PRICE" => $booking["PRICE"],
                                        "CURRENCY" => 'RUB'
                                    )
                                );
                            }
                        }

                        if ($message) {
                            array_push($this->arResult["MESSAGE"]["RESULT_LOAD"], "Расписание на " . $booking["DATE"] . ' ' . $booking["TIME"] . ' обновлено');
                        }
                    }

                } else if (!is_null($arItem) && $arItem["ID"] > 0 && $arItem["IN_SALE"] == "Y") {
                    if ($message) {
                        array_push($this->arResult["MESSAGE"]["RESULT_LOAD"], "Расписание на " . $booking["DATE"] . ' ' . $booking["TIME"] . ' не изменено, участвует в акции');
                    }
                } else {
                    $title = $booking["TIME"];
                    $arParams_trans = array("replace_space" => "-", "replace_other" => "-");
                    $trans = \Cutil::translit($booking["TIMESTAMP"], "ru", $arParams_trans);
                    $date = date("d.m.Y H:i:s");

                    $addOfferArray = array(
                        "IBLOCK_ID" => $arParams["QUEST"]["OFFERS_IBLOCK_ID"],
                        "ACTIVE" => "Y",
                        "CREATED_BY" => $USER->GetID(),
                        "NAME" => $title,
                        "CODE" => $trans,
                        "ACTIVE_FROM" => $date,
                        "ACTIVE_TO" => date('d.m.Y H:i:s', $booking["TIMESTAMP"]),
                        "PROPERTY_VALUES" => array(
                            "CML2_LINK" => $booking["PRODUCT_ID"],
                            "START_TIME" => $booking["TIME"],
                            "DATE" => $booking["DATE"],
                            "RESERVED" => ($booking['IS_FREE'] == 'N') ? 105 : false,
                            "ID_BOOKING_MIRKVESTOV" => $booking["ID_BOOKING_MIRKVESTOV"],
                            "OLD_PRICE" => "",
                            "IN_SALE" => false,
                            "PRICE_TYPE" => $booking['PRICE_TYPE'],
                            "SLOT_ID" => $booking['SLOT_ID'],
                            "PRICE_GROUPS" => $booking["PRICE_GROUPS"],
                            "PRICE_GROUPS_TIMESTAMP" => $booking["PRICE_GROUPS_TIMESTAMP"],
                        ),
                    );

                    if (count($arBookingNotActive) > 0) {
                        $offerID = $arBookingNotActive[0];
                        $resUpdate = $el->Update($offerID, $addOfferArray);

                        if ($resUpdate) {
                            unset($arBookingNotActive[0]);
                            $arBookingNotActive = array_values($arBookingNotActive);
                            if ($booking["PRICE"] > 0) {
                                $BASE_PRICE_ID = 1;

                                $res = \CPrice::GetList(
                                    array(),
                                    array(
                                        "PRODUCT_ID" => $offerID,
                                        "CATALOG_GROUP_ID" => $BASE_PRICE_ID
                                    )
                                );

                                if ($arr = $res->Fetch()) {
                                    if ($arr["PRICE"] != $booking["PRICE"]) {
                                        \CPrice::Update(
                                            $arr["ID"],
                                            array(
                                                "PRODUCT_ID" => $offerID,
                                                "CATALOG_GROUP_ID" => $BASE_PRICE_ID,
                                                "PRICE" => $booking["PRICE"],
                                                "CURRENCY" => 'RUB'
                                            )
                                        );
                                    }
                                } else {
                                    \CPrice::Add(
                                        array(
                                            "PRODUCT_ID" => $offerID,
                                            "CATALOG_GROUP_ID" => $BASE_PRICE_ID,
                                            "PRICE" => $booking["PRICE"],
                                            "CURRENCY" => 'RUB'
                                        )
                                    );
                                }

                                if ($message) {
                                    array_push($this->arResult["MESSAGE"]["RESULT_LOAD"], "Добавлено новое расписание на " . $booking["DATE"] . ' ' . $booking["TIME"] . ' c ценой ' . $booking["PRICE"] . ' руб.');
                                }
                            } else {
                                array_push($this->arResult["MESSAGE"]["ERROR"], $el->LAST_ERROR);
                            }
                        }
                    } else {
                        $offerID = $el->Add($addOfferArray, false, true, true);

                        if ($offerID) {
                            $arFields = array(
                                "ID" => $offerID,
                                "VAT_ID" => 1,
                                "VAT_INCLUDED" => "Y"
                            );

                            if (\CCatalogProduct::Add($arFields)) {
                                if ($booking["PRICE"] > 0) {
                                    #Код базовой цены
                                    $BASE_PRICE_ID = 1;

                                    \CPrice::Add(
                                        array(
                                            "PRODUCT_ID" => $offerID,
                                            "CATALOG_GROUP_ID" => $BASE_PRICE_ID,
                                            "PRICE" => $booking["PRICE"],
                                            "CURRENCY" => 'RUB'
                                        )
                                    );

                                    if ($message) {
                                        array_push($this->arResult["MESSAGE"]["RESULT_LOAD"], "Добавлено новое расписание на " . $booking["DATE"] . ' ' . $booking["TIME"] . ' c ценой ' . $booking["PRICE"] . ' руб.');
                                    }
                                }
                            }
                        } else {
                            array_push($this->arResult["MESSAGE"]["ERROR"], $el->LAST_ERROR);
                        }
                    }
                }
            }
        }
    }

    /**
     * @param $text
     * @return null|string|string[]
     */
    public function remove_utf8_bom($text)
    {
        $bom = pack('H*', 'EFBBBF');
        $text = preg_replace("/^$bom/", '', $text);
        return $text;
    }

    /**
     * @param $url
     * @param $arParams
     * @return array|bool
     * @throws Main\LoaderException
     */
    public function sendBooking($url, $arParams)
    {
        $this->checkModules();

        $this->arResult["URL"] = $url;
        $this->arResult["PARAMS"] = $arParams;

        if (
            !$this->arResult["URL"] and
            count($this->arResult["PARAMS"]) == 0
        ) {
            return false;
        }

        $arData = $this->sendBookingSystem();

        if ($arData["success"] == true) {
            if (intval($arData["book_id"]) > 0) {
                $this->arResult["BOOK_ID"] = intval($arData["book_id"]);
            }

        } else {
            array_push($this->arResult["MESSAGE"]["ERROR"], $arData["message"]);
        }

        return $this->arResult;

    }

    /**
     * @return bool|mixed
     */
    protected function sendBookingSystem()
    {
        if (self::InitBots()) {
            return false;
        }

        if (!function_exists('curl_init')) {
            if (!$text = file_get_contents($this->arResult["URL"])) {
                return false;
            }
        } else {
            $handle = curl_init();

            $post_string = '';

            foreach ($this->arResult["PARAMS"] as $name => $value) {
                if (strlen($post_string) > 0) {
                    $post_string .= '&' . $name . '=' . $value;
                } else {
                    $post_string .= $name . '=' . $value;
                }
            }

            curl_setopt($handle, CURLOPT_URL, $this->arResult["URL"]);
            curl_setopt($handle, CURLOPT_HEADER, false);
            curl_setopt($handle, CURLOPT_POST, 1);
            curl_setopt($handle, CURLOPT_POSTFIELDS, $post_string);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);

            $resData = curl_exec($handle);
            $codeData = curl_getinfo($handle, CURLINFO_HTTP_CODE);
            $errno = curl_errno($handle);

            curl_close($handle);

            if ($codeData == 200) {
                $resData = $this->remove_utf8_bom($resData);
                $resData = Encoding::convertEncoding($resData, 'utf-8', SITE_CHARSET);
                $arData = json_decode($resData, true);

            } else {
                array_push($this->arResult["MESSAGE"]["ERROR"], Loc::getMessage('BOOKING_ERROR_CODE', array("#CODE#" => $codeData)));
                return false;
            }

        }

        return $arData;
    }

    /**
     * @return bool|mixed
     */
    static function InitBots()
    {
        $bots = array(
            'rambler', 'googlebot', 'ia_archiver', 'Wget', 'WebAlta', 'MJ12bot', 'aport', 'yahoo', 'msnbot', 'mail.ru',
            'alexa.com', 'Baiduspider', 'Speedy Spider', 'abot', 'Indy Library'
        );

        foreach ($bots as $bot) {
            if (stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false) {
                return $bot;
            }
        }

        return false;
    }

}