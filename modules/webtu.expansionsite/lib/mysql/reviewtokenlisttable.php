<?php
namespace Webtu\ExpansionSite\Mysql;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class ReviewTokenListTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'webtu_review_token_list';
    }

    public static function getUfId()
    {
        return 'WEBTU_REVIEW_TOKEN_LIST';
    }

    public static function getMap()
    {
        return array(
            // ID
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            // ID ORDER
            new Entity\IntegerField('ORDER_ID', array(
                'required' => true
            )),
            // TOKEN
            new Entity\StringField('TOKEN', array(
                'required' => true
            )),
            // ACTIVE
            new Entity\StringField('ACTIVE', array(
                'required' => true,
                'default_value' => 'Y'
            )),
        );
    }
}