<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

if ( count($arResult["ITEMS"]) > 0 )
{
    echo '<div class="page-block pattern-block">';
        echo '<div class="page-container">';

            echo '<h2 class="page-title page-title--h2">Выберите сертификат</h2>';

            echo '<div class="gift-switches switch-items">';

                foreach ($arResult["ITEMS"] as $key => $arItem)
                {
                    $mainId = $this->GetEditAreaId($arItem['ID']);
                    $idElement = 'element-'.$mainId;

                    $obName = 'ob_'.preg_replace('/[^a-zA-Z0-9_]/', 'x', $mainId);

                    $jsParams = array(
                        'CONFIG' => array(
                            'ELEMENT_ID'  => $idElement,
                            'PATH'        => $arResult["PATH"],
                            "PAGE_BUY"    => $arItem["PAGE_BUY"],
                            "QUEST_LIST"  => $arItem["QUEST_LIST"],
                            "BASE_PRICE"  => $arItem["BASE_PRICE"],
                            "CERTIFICATE_ABILITY_COST"  => $arItem["CERTIFICATE_ABILITY_COST"],
                            "CERTIFICATE_MIN_COST"  => $arItem["CERTIFICATE_MIN_COST"],
                        ),
                    );

                    if ($key == 0)
                    {
                        $class = 'is-active';
                    }
                    else
                    {
                        $class = '';
                    }

                    echo '<label class="switch-items__item js-tab-toggle '.$class.'" id='.$idElement.'>';
                        echo '<span class="switch-items__content">';
                            echo '<span class="aligner">';
                                echo '<span class="switch-items__type">«'.$arItem["TYPE"].'»</span>';
                                echo '<strong class="switch-items__title">'.$arItem["NAME"].'</strong>';

                                if ( $arItem["PRICE"] > 0 ) {
                                    if ( $arItem["CERTIFICATE_ABILITY_COST"] != "Y" ) {
                                        echo '<span class="switch-items__caption">Стоимость:</span>';
                                        echo '<span class="switch-items__result"> '.CCurrencyLang::CurrencyFormat($arItem["PRICE"], "RUB", true).'</span>';
                                    } else {
                                        echo '<span class="switch-items__caption">Введите стоимость (руб.):</span>';
                                        echo '<input name="CERTIFICATE_MIN_COST" data-min_cost="'.$arItem["CERTIFICATE_MIN_COST"].'" class="switch-items__result" value="'.$arItem["CERTIFICATE_MIN_COST"].'">';
                                    }
                                }

                            echo '</span>';
                        echo '</span>';
                        ?>
                        <script>
                            BX.message({});
                            var <?=$obName?> = new CLComponent(<? echo CUtil::PhpToJSObject($jsParams, false, true); ?>);
                        </script>
                        <?
                    echo '</label>';
                }
            echo '</div>';

            echo '<div class="show-more">';
                echo '<a id="buy-certificate" href="'.$arResult["ITEMS"][0]["PAGE_BUY"].'" class="button button--large button--red">Купить сертификат</a>';
            echo '</div>';

        echo '</div>';
    echo '</div>';
}
?>

