<?php
$MESS["WEBTU_ES_TAB1"] = "GEOIP";
$MESS["WEBTU_ES_TAB1_TITLE"] = "Настройки GEOIP";
$MESS["WEBTU_ES_TAB1_TITLE_HEADER_1"] = "Источник данных";
$MESS["WEBTU_ES_TAB1_DATA_SOURCE_IP"] = "Источник данных";
$MESS["WEBTU_ES_TAB1_DATA_SOURCE_LOCAL"] = "Локальная база";
$MESS["WEBTU_ES_TAB1_DATA_SOURCE_CURL"] = "Запрос на сайте ipgeobase.ru";
$MESS["WEBTU_ES_TAB2_CITY_DEFAULT"] = "CODE города по умолчанию";
$MESS["WEBTU_ES_TAB1_NEAREST_DOT"] = "Ближайшая точка к определённому городу по IP";
$MESS["WEBTU_ES_TAB1_IBLOCK_ID"] = "Инфоблок ID";
$MESS["WEBTU_ES_TAB1_PROPERTY_MAP_CODE"] = "Код свойства с координатами (Тип: Привязка к яндекс карте)";

$MESS["WEBTU_ES_TAB1_NOTE"] = '<p style="text-align: left;">
1.1 Источник данных, откуда будут браться данные.<br />
1.2 CODE элемента с городом по умолчанию.<br />
1.3 Ближайшая точка к определённому городу по IP, позволяет определить ближайшую доступную точку к вам (если не указано значение 1.2).<br />
1.4 Инфоблок ID, инфоблок с городами где есть координаты.<br />
1.5 Код свойства с координатами (Тип: Привязка к яндекс карте), код свойства из настроек инфоблока.<br />
</p>';


$MESS["WEBTU_ES_TAB2"] = "Квесты";
$MESS["WEBTU_ES_TAB2_TITLE"] = "Параметры";
$MESS["WEBTU_ES_TAB2_TITLE_HEADER_1"] = "Параметры квестов";
$MESS["WEBTU_ES_TAB2_CATALOG_IBLOCK_ID"] = "Инфоблок ID с квестами";
$MESS["WEBTU_ES_TAB2_OFFERS_IBLOCK_ID"] = "Инфоблок ID с элементами для брони квестов";
$MESS["WEBTU_ES_TAB2_COUNT_WEEKS_TO_CREATE_BOOKINGS"] = "Кол-во недель на создание броней (по умолчанию 3)";
$MESS["WEBTU_ES_TAB2_STATUS_ACTIVE_VALUE"] = "ID значения свойства активные квесты";
$MESS["WEBTU_ES_TAB2_STATUS_DISABLED_VALUE"] = "ID значения свойства закрытые квесты";
$MESS["WEBTU_ES_TAB2_STATUS_DRAFT_VALUE"] = "ID значения свойства черновые квесты";
$MESS["WEBTU_ES_TAB2_INTEGRATION_SYSTEM_MIRKVESTOV_VALUE"] = "ID значения свойства Мир Квестов";
$MESS["WEBTU_ES_TAB2_INTEGRATION_SYSTEM_QUESTCOMPASS_VALUE"] = "ID значения свойства Компас";
$MESS["WEBTU_ES_TAB2_SEARCH_TEAM_BUTTON"] = "Скрыть кнопку 'Найти команду' в детальной карточке Квеста";
$MESS["WEBTU_ES_TAB2_TITLE_HEADER_2"] = "Параметры команд";
$MESS["WEBTU_ES_TAB2_TEAM_IBLOCK_ID"] = "Инфоблок ID с командами";
$MESS["WEBTU_ES_TAB2_TEAM_NOVICE"] = "ID значения свойства статус команды: Новичок";
$MESS["WEBTU_ES_TAB2_TEAM_SPECIALIST"] = "ID значения свойства статус команды: Специалист";
$MESS["WEBTU_ES_TAB2_TEAM_EXPERT"] = "ID значения свойства статус команды: Эксперт";
$MESS["WEBTU_ES_TAB2_PART_IBLOCK_ID"] = "Инфоблок ID принять участие в Квест Батл";
$MESS["WEBTU_ES_TAB2_CRITIQUE_IBLOCK_ID"] = "Инфоблок ID с рецензиями";
$MESS["WEBTU_ES_TAB2_REVIEW_IBLOCK_ID"] = "Инфоблок ID с отзывами";
$MESS["WEBTU_ES_TAB2_REVIEW_MODERATION"] = "Добалять отзыв/рецензию на модерацию";
$MESS["WEBTU_ES_TAB2_PROMO_IBLOCK_ID"] = "Инфоблок ID с акциями";
$MESS["WEBTU_ES_TAB2_TEAM_MAX_COUNT_MEMBERS"] = "Максимальное количество участников в команде";
$MESS["WEBTU_ES_TAB2_CERTIFICATE_IBLOCK_ID"] = "Инфоблок ID с сертификатами";
$MESS["WEBTU_ES_TAB2_TEAM_MAX_COUNT"] = "Максимальное количество команд для одного пользователя";
$MESS["WEBTU_ES_TAB2_GIFT_LINK"] = "Показывать виджет Квест в подарок";

$MESS["WEBTU_ES_TAB3"] = "Пользователи";
$MESS["WEBTU_ES_TAB3_TITLE"] = "Параметры";
$MESS["WEBTU_ES_TAB3_TITLE_HEADER_1"] = "Уровень доступа к Frontend";
$MESS["WEBTU_ES_TAB3_USERS_ACCESS_1"] = "Уровень доступа 1";
$MESS["WEBTU_ES_TAB3_USERS_ACCESS_2"] = "Уровень доступа 2";
$MESS["WEBTU_ES_TAB3_TITLE_HEADER_2"] = "Временный пароль";
$MESS["WEBTU_ES_TAB3_USERS_PASSWORD"] = "Пароль";
$MESS["WEBTU_ES_TAB3_TITLE_HEADER_3"] = "Чат с администратором сайта";
$MESS["WEBTU_ES_TAB3_ADMIN_MESSAGE"] = "Пользователь";
$MESS["WEBTU_ES_TAB3_TITLE_HEADER_4"] = "Отсроченный платеж";
$MESS["WEBTU_ES_TAB3_DEFERRED_PAYMENT_IBLOCK_ID"] = "Инфоблок ID с отсроченными платежами";
$MESS["WEBTU_ES_TAB3_DEFERRED_PAYMENT_COUNT_DAYS"] = "Кол-во дней до списания";
$MESS["WEBTU_ES_TAB3_DEFERRED_PAYMENT_SUM"] = "Сумма";
$MESS["WEBTU_ES_TAB3_DEFERRED_PAYMENT_SUM_NOT_AVAILABLE"] = "Не доступен при балансе ниже";


$MESS["WEBTU_ES_TAB3_NOTE"] = '<p style="text-align: left;">
3.1 Позволяет видеть контент доступный пользователю<br />
3.2 Позволяет видеть контент доступный организации<br />
3.3 Временный пароль для заявки партнера<br />
3.4 Чат с администратором сайта, выбор пользователя<br />
3.5 Инфоблок ID с отсроченными платежами<br />
3.6 Кол-во дней действия отсроченного платежа<br />
3.7 Сумма отсроченного платежа<br />
3.8 Сумма баланса пользователя, при котором отсроченный платеж не доступен
</p>';

$MESS["WEBTU_ES_TAB4"] = "Свойства";
$MESS["WEBTU_ES_TAB4_TITLE"] = "Сопоставления свойств";
$MESS["WEBTU_ES_TAB4_TITLE_HEADER_1"] = "Свойства пользователя";
$MESS["WEBTU_ES_TAB4_PROPS_TYPE_USER_ID"] = "Тип пользователя ID";
$MESS["WEBTU_ES_TAB4_PROPS_TYPE_USER_CODE"] = "Тип пользователя CODE";
$MESS["WEBTU_ES_TAB4_PROPS_CASHBACK_ID"] = "CashBack ID";
$MESS["WEBTU_ES_TAB4_PROPS_CASHBACK_CODE"] = "CashBack CODE";
$MESS["WEBTU_ES_TAB4_TITLE_HEADER_2"] = "Свойства Квестов";
$MESS["WEBTU_ES_TAB4_PROPS_QUEST_RAITING_ID"] = "Рейтинг КвестБатла ID";
$MESS["WEBTU_ES_TAB4_PROPS_QUEST_RAITING_CODE"] = "Рейтинг КвестБатла CODE";
$MESS["WEBTU_ES_TAB4_PROPS_QUEST_STATUS_ID"] = "Статус квеста";
$MESS["WEBTU_ES_TAB4_PROPS_QUEST_STATUS_CODE"] = "Статус квеста CODE";
$MESS["WEBTU_ES_TAB4_PROPS_QUEST_INTEGRATION_SYSTEM_ID"] = "Система бронирования";
$MESS["WEBTU_ES_TAB4_PROPS_QUEST_INTEGRATION_SYSTEM_CODE"] = "Система бронирования CODE";
$MESS["WEBTU_ES_TAB4_NOTE"] = '<p style="text-align: left;">
4.1 Тип пользователя<br />
4.2 CashBack<br />
4.3 Рейтинг КвестБатла<br />
4.4 Статус квеста<br />
4.5 Система бронирования<br />   
</p>';


$MESS["WEBTU_ES_TAB5"] = "Бонусы";
$MESS["WEBTU_ES_TAB5_TITLE"] = "Настройки бонусов";
$MESS["WEBTU_ES_TAB5_TITLE_HEADER_1"] = "Параметры";
$MESS["WEBTU_ES_TAB5_BONUS_TYPE_USER_VALUE"] = "Тип пользователя";
$MESS["WEBTU_ES_TAB5_BONUS_PARTNER_HASH"] = "Контрольная строка";
$MESS["WEBTU_ES_TAB5_BONUS_CURRENCY"] = "Валюта суммы";
$MESS["WEBTU_ES_TAB5_BONUS_ENROL_USERS_ALL"] = "Начислять бонусы всем пользователям по событию";
$MESS["WEBTU_ES_TAB5_BONUS_DEDUCT"] = "Включить списание бонусов";
$MESS["WEBTU_ES_TAB5_BONUS_DEDUCT_DAY"] = "За какой период списывать бонусы (дней)";
$MESS["WEBTU_ES_TAB5_TITLE_HEADER_2"] = "CashBack";
$MESS["WEBTU_ES_TAB5_BONUS_CASHBACK"] = "Назначать CashBack";
$MESS["WEBTU_ES_TAB5_BONUS_CASHBACK_MIN"] = "Минимальный CashBack, %";
$MESS["WEBTU_ES_TAB5_BONUS_CASHBACK_MAX"] = "Максимальный CashBack, %";
$MESS["WEBTU_ES_TAB5_BONUS_CASHBACK_TOP15"] = "CashBack, % капитану команды из ТОП 15% команд";
$MESS["WEBTU_ES_TAB5_BONUS_CASHBACK_TOP30"] = "CashBack, % капитану команды из ТОП 30% команд";
$MESS["WEBTU_ES_TAB5_BONUS_CASHBACK_TOP40"] = "CashBack, % капитану команды из ТОП 40% команд";
$MESS["WEBTU_ES_TAB5_BONUS_CASHBACK_TOP50"] = "CashBack, % капитану команды из ТОП 50% команд";
$MESS["WEBTU_ES_TAB5_BONUS_CASHBACK_TOP70"] = "CashBack, % капитану команды из ТОП 70% команд";
$MESS["WEBTU_ES_TAB5_TITLE_HEADER_3"] = "События";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_REGISTER"] = "Начислять бонусы за регистраци";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_REGISTER_SUM"] = "Количество бонусов за регистрацию";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_REGISTER_SUM_2"] = "Количество бонусов за регистрацию (по приглашению)";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_REGISTER_INVITE"] = "Начислять бонусы за приглашение друга";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_REGISTER_INVITE_SUM"] = "Количество бонусов за приглашение друга";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_REVIEW_ADD"] = "Начислять бонусы за оставленный отзыв";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_REVIEW_ADD_SUM"] = "Количество бонусов за оставленный отзыв";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_TEAM_ADD"] = "Начислять бонусы за добавление новой команды";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_TEAM_ADD_SUM"] = "Количество бонусов за добавление новой команды";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_SUNC_SOCIAL_NETWORK"] = "Начислять бонусы за привязку аккаунта соц. сети";
$MESS["WEBTU_ES_TAB5_BONUS_EVENT_SUNC_SOCIAL_NETWORK_SUM"] = "Количество бонусов за привязку аккаунта соц. сети";

$MESS["WEBTU_ES_TAB5_NOTE"] = '<p style="text-align: left;">
5.1 Тип пользователя для начисления бонусов.<br />
5.2 Контрольная строка для формирования реферальной ссылки.<br />
5.3 Валюта суммы сайта.<br />
5.4 Начислять бонусы всем пользователям по событию. При включенной опции всем пользователям идёт начисление бонусов по наступлению активного события.<br />
5.5 Система CashBack<br />
5.6 Минимальный CashBack назначается пользователю при регистрации<br />
5.7 Манимальный CashBack, который может быть назначен<br />
5.8 CashBack, % капитану команды из ТОП 15% команд по итоговому рейтингу<br />
5.9 CashBack, % капитану команды из ТОП 30% команд по итоговому рейтингу<br />
5.10 CashBack, % капитану команды из ТОП 40% команд по итоговому рейтингу<br />
5.11 CashBack, % капитану команды из ТОП 50% команд по итоговому рейтингу<br />
5.12 CashBack, % капитану команды из ТОП 70% команд по итоговому рейтингу<br />
5.13 Начислять бонусы за регистраци. При включённой опции за событие регистрации идёт начисление бонусов.<br />
5.14 Количество бонусов за регистрацию.<br />
5.15 Количество бонусов за регистрацию по приглашению.<br />
5.16 Начислять бонусы за приглашение друга. При включённой опции за приглашение друга идёт начисление бонусов.<br />
5.17 Количество бонусов за приглашение друга.<br />
5.18 Начислять бонусы за оставленный отзыв после бронирования. При включённой опции за оставленный отзыв идёт начисление бонусов.<br />
5.19 Количество бонусов за оставленный отзыв после бронирования.<br />
5.20 Начислять бонусы за добавление новой команды. При включённой опции за добавление новой команды идёт начисление бонусов.<br />
5.21 Количество бонусов за добавление новой команды.<br />
5.22 Начислять бонусы за привязку аккаунта соц. сети.<br />
5.23 Количество бонусов за привязку аккаунта соц. сети.<br />';

$MESS["WEBTU_ES_TAB6"] = "Бронирование";
$MESS["WEBTU_ES_TAB6_TITLE"] = "Настройки бронирования";
$MESS["WEBTU_ES_TAB6_TITLE_HEADER_1"] = "Параметры";
$MESS["WEBTU_ES_TAB6_ORDER_AUTH_USER_ID"] = "ID пользователя";
$MESS["WEBTU_ES_TAB6_ORDER_CANCEL_IBLOCK_ID"] = "ID инфоблока (с причинами отмены бронирования)";
$MESS["WEBTU_ES_TAB6_ORDER_QUEST_COMPLETED_IBLOCK_ID"] = "ID инфоблока (с пройденными квестами)";
$MESS["WEBTU_ES_TAB6_ORDER_PERCENT_DEBIT"] = "Процент от стоимости бронирования";
$MESS["WEBTU_ES_TAB6_TITLE_HEADER_2"] = "Параметры для начисления очков за пройденный квест";
$MESS["WEBTU_ES_TAB6_ORDER_LEVEL_DIFF_1"] = "Сложность 1";
$MESS["WEBTU_ES_TAB6_ORDER_LEVEL_DIFF_2"] = "Сложность 2";
$MESS["WEBTU_ES_TAB6_ORDER_LEVEL_DIFF_3"] = "Сложность 3";
$MESS["WEBTU_ES_TAB6_ORDER_LEVEL_DIFF_4"] = "Сложность 4";
$MESS["WEBTU_ES_TAB6_ORDER_TIME_PROMPT"] = "Время за одну подсказку";


$MESS["WEBTU_ES_TAB6_NOTE"] = '<p style="text-align: left;">
6.1 ID пользователя (для неавторизованных пользователей), под которым будет создоваться заказ<br />
6.2 ID инфоблока (с причинами отмены бронирования)<br />
6.3 ID инфоблока (с пройденными квестами)<br />
6.4 Процент от стоимости бронирования (снятие суммы с личного счета организатора, при подтверждении бронирования)<br />
6.5 Очки за прохождение квеста (сложность 1)<br />
6.6 Очки за прохождение квеста (сложность 2)<br />
6.7 Очки за прохождение квеста (сложность 3)<br />
6.8 Очки за прохождение квеста (сложность 4)<br />
6.9 Дополнительное кол-во минут за 1 подсказку<br />
</p>';

$MESS["WEBTU_ES_TAB7"] = "Мета Теги";
$MESS["WEBTU_ES_TAB7_TITLE"] = "Настройки Мета Тегов";
$MESS["WEBTU_ES_TAB7_TITLE_HEADER_1"] = "Параметры";
$MESS["WEBTU_ES_TAB7_CATALOG_IBLOCK_ID"] = "Инфоблок с каталогом";
$MESS["WEBTU_ES_TAB7_METATAG_LOGO"] = "Путь к картинке";


$MESS["WEBTU_ES_TAB7_NOTE"] = '<p style="text-align: left;">
7.1 ID инфоблока с каталогом.<br />
7.2 Путь к картинке относительно корня сайта.<br />
</p>';

$MESS["WEBTU_ES_TAB8"] = "Мета-данные для SEO";
$MESS["WEBTU_ES_TAB8_TITLE"] = "Настройки Мета-данных для SEO";
$MESS["WEBTU_ES_TAB8_TITLE_HEADER_1"] = "Параметры";
$MESS["WEBTU_ES_TAB8_SEO_FILTER_IBLOCK_ID"] = "Инфоблок с настройками";
$MESS["WEBTU_ES_TAB8_SEO_FILTER_PAGE"] = "Страница каталога";
$MESS["WEBTU_ES_TAB8_SEO_TAG_COUNT"] = "Кол-во выводимых тэгов";


$MESS["WEBTU_ES_TAB8_NOTE"] = '<p style="text-align: left;">
8.1 ID инфоблока с настройками(h1, title, description, keywords).<br />
8.2 Страница каталога.<br />
8.3 Количество выводимых тэгов.<br />
</p>';


$MESS["WEBTU_ES_TAB9"] = "Оплата";
$MESS["WEBTU_ES_TAB9_TITLE"] = "Настройки оплаты на сайте";
$MESS["WEBTU_ES_TAB9_TITLE_HEADER_1"] = "Параметры";
$MESS["WEBTU_ES_TAB9_CERTIFICATE_PAYMENT"] = "Способ оплаты при покупке сертификата";
$MESS["WEBTU_ES_TAB9_BOOKING_PAYMENT"] = "Способы оплаты при бронировании квеста";
$MESS["WEBTU_ES_TAB9_TITLE_HEADER_2"] = "Маркетплейс Platron";
$MESS["WEBTU_ES_TAB9_PLATRON_TRANSFER"] = "Автоматический перевод средтсв партнерам";
$MESS["WEBTU_ES_TAB9_PLATRON_MERCHANT_ID"] = "Merchant ID";
$MESS["WEBTU_ES_TAB9_PLATRON_SECRET_KEY"] = "Секретный ключ";
$MESS["WEBTU_ES_TAB9_PLATRON_CONTRACT_ID"] = "Номер контракта";
$MESS["WEBTU_ES_TAB9_PLATRON_PERCENT_VALUE"] = "Комиссия за оплату, %";
$MESS["WEBTU_ES_TAB9_PLATRON_RUB_VALUE"] = "Комиссия за перевод, руб.";
$MESS["WEBTU_ES_TAB9_COMMISSION_PAYMENT"] = "Способ оплаты для комиссии";

$MESS["WEBTU_ES_TAB9_NOTE"] = '<p style="text-align: left;">
9.1 Способ оплаты при покупке сертификата на квест.<br />
9.2 Способ оплаты при покупке бронировании квеста . Список будет отображаться пользователю для выбора.<br />
9.3 Автоматический перевод средтсв партнерам при оплате бонусными баллами или банковской картой".<br />
9.4 Merchant ID магазина в ЛК.<br />
9.5 Секретный ключ магазина в ЛК.<br />
9.6 Номер контракта магазина в ЛК, по которому будет произведена выплата.<br />
9.7 Комиссия платежной системы за оплату, в %.<br />
9.8 Комиссия платежной системы за перевод, в руб.<br />
9.9 Способ оплаты для вычета комиссии.<br />
</p>';

$MESS["WEBTU_ES_TAB10"] = "Награждения";
$MESS["WEBTU_ES_TAB10_TITLE"] = "Награждения по сезонам";
$MESS["WEBTU_ES_TAB10_TITLE_HEADER_1"] = "Зимний сезон";
$MESS["WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_DATE_FROM"] = "Дата проведения (от)";
$MESS["WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_DATE_TO"] = "Дата проведения (до)";
$MESS["WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_ELEMENTS_ID_TOTAL_RATING"] = "Итоговый рейтинг (ID элемента)";
$MESS["WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_ELEMENTS_ID_RATING_POINTS_SEASON"] = "По кол-ву очков (ID элемента)";
$MESS["WEBTU_ES_TAB10_AWARDS_WINTER_SEASON_ELEMENTS_ID_RATING_QUALITY_GAME"] = "По качеству игры (ID элемента)";

$MESS["WEBTU_ES_TAB10_TITLE_HEADER_2"] = "Весенний сезон";
$MESS["WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_DATE_FROM"] = "Дата проведения (от)";
$MESS["WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_DATE_TO"] = "Дата проведения (до)";
$MESS["WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_ELEMENTS_ID_TOTAL_RATING"] = "Итоговый рейтинг (ID элемента)";
$MESS["WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_ELEMENTS_ID_RATING_POINTS_SEASON"] = "По кол-ву очков (ID элемента)";
$MESS["WEBTU_ES_TAB10_AWARDS_SPRING_SEASON_ELEMENTS_ID_RATING_QUALITY_GAME"] = "По качеству игры (ID элемента)";

$MESS["WEBTU_ES_TAB10_TITLE_HEADER_3"] = "Летний сезон";
$MESS["WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_DATE_FROM"] = "Дата проведения (от)";
$MESS["WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_DATE_TO"] = "Дата проведения (до)";
$MESS["WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_ELEMENTS_ID_TOTAL_RATING"] = "Итоговый рейтинг (ID элемента)";
$MESS["WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_ELEMENTS_ID_RATING_POINTS_SEASON"] = "По кол-ву очков (ID элемента)";
$MESS["WEBTU_ES_TAB10_AWARDS_SUMMER_SEASON_ELEMENTS_ID_RATING_QUALITY_GAME"] = "По качеству игры (ID элемента)";

$MESS["WEBTU_ES_TAB10_TITLE_HEADER_4"] = "Осенний сезон";
$MESS["WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_DATE_FROM"] = "Дата проведения (от)";
$MESS["WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_DATE_TO"] = "Дата проведения (до)";
$MESS["WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_ELEMENTS_ID_TOTAL_RATING"] = "Итоговый рейтинг (ID элемента)";
$MESS["WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_ELEMENTS_ID_RATING_POINTS_SEASON"] = "По кол-ву очков (ID элемента)";
$MESS["WEBTU_ES_TAB10_AWARDS_AUTUMN_SEASON_ELEMENTS_ID_RATING_QUALITY_GAME"] = "По качеству игры (ID элемента)";


$MESS["WEBTU_ES_TAB10_NOTE"] = '<p style="text-align: left;">
10.1 Дата (от) проведения зимнего сезона (формат ДД.ММ).<br />
10.2 Дата (до) проведения зимнего сезона (формат ДД.ММ).<br />
10.3 ID элемента с итоговым рейтингом (зимний сезон).<br />
10.4 ID элемента с рейтингом по кол-ву очков (зимний сезон).<br />
10.5 ID элемента с рейтингом по качеству игры (зимний сезон).<br /><br />
10.6 Дата (от) проведения весеннего сезона (формат ДД.ММ).<br />
10.7 Дата (до) проведения весеннего сезона (формат ДД.ММ).<br />
10.8 ID элемента с итоговым рейтингом (весенний сезон).<br />
10.9 ID элемента с рейтингом по кол-ву очков (весенний сезон).<br />
10.10 ID элемента с рейтингом по качеству игры (весенний сезон).<br /><br />
10.11 Дата (от) проведения летнего сезона (формат ДД.ММ).<br />
10.12 Дата (до) проведения летнего сезона (формат ДД.ММ).<br />
10.13 ID элемента с итоговым рейтингом (летний сезон).<br />
10.14 ID элемента с рейтингом по кол-ву очков (летний сезон).<br />
10.15 ID элемента с рейтингом по качеству игры (летний сезон).<br /><br />
10.16 Дата (от) проведения осеннего сезона (формат ДД.ММ).<br />
10.17 Дата (до) проведения осеннего сезона (формат ДД.ММ).<br />
10.18 ID элемента с итоговым рейтингом (осенний сезон).<br />
10.19 ID элемента с рейтингом по кол-ву очков (осенний сезон).<br />
10.20 ID элемента с рейтингом по качеству игры (осенний сезон).<br />
</p>';


$MESS["WEBTU_ES_TAB11"] = "Настройки SMTP";
$MESS["WEBTU_ES_TAB11_TITLE"] = "Настройки SMTP";
$MESS["WEBTU_ES_TAB11_SMTP_LOGIN"] = "Логин";
$MESS["WEBTU_ES_TAB11_SMTP_PASSWORD"] = "Пароль";
$MESS["WEBTU_ES_TAB11_SMTP_NAME"] = "Имя отправителя";
$MESS["WEBTU_ES_TAB11_SMTP_PORT"] = "Номер порта";
$MESS["WEBTU_ES_TAB11_SMTP_SERVER"] = "Сервер";

?>