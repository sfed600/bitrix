<?php
namespace Webtu\ExpansionSite;

use \Bitrix\Main;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Webtu\ExpansionSite\Handler;

Loc::loadMessages(__FILE__);

class MetaTag
{
    public function __construct(){}

    /**
     * @brief Инициализация MetaTag
     * @param Type = Article - Статьи детальная
     * @param Type = ListItem - Статьи список
     * @param Type = ListItemLight - Статьи список упрощённый
     * @param Type = Product - Товар детальная
     * @param Type = ProductItem - Товар список
     * @param Type = Organization - Организация
     * @param $arResult = массив данных
     * @return Буферизированный контент
     **/
	public function init($type, $arResult)
	{
        global $APPLICATION;

        if($type == "Article")
        {
            $resMetaTag = self::MetaTagArticle($arResult);
        }
        else if($type == "ListItem")
        {
            $resMetaTag = self::MetaTagListItem($arResult);
        }
        else if($type == "ListItemLight")
        {
            $resMetaTag = self::MetaTagListItemLight($arResult);
        }
        else if($type == "Product")
        {
            $resMetaTag = self::MetaTagProduct($arResult);
        }
        else if($type == "ProductItem")
        {
            $resMetaTag = self::MetaTagProductItem($arResult);
        }
        else if($type == "Organization")
        {
            $resMetaTag = self::MetaTagOrganization($arResult);
        }

        if($resMetaTag)
        {
          $APPLICATION->AddViewContent('MetaTag',$resMetaTag);
        }
        
    }

    /**
     * @brief Для детальной страницы 
     * @param $arResult = массив данных
     * @return Разметка
     **/
	public function MetaTagArticle($arResult)
	{
        #Получить все настройки модуля
        $arParams = Handler::getOptions();
       
        $serverName = SITE_SERVER_NAME;
        $protocol = (\CMain::IsHTTPS()) ? "https://" : "http://"; 
        $seoDetailPage = $protocol.$serverName.$arResult["DETAIL_PAGE_URL"];
        $seoTitle = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arResult['NAME'];
        $datePublished = ( ($arResult["DATE_CREATE"]) ? $arResult["DATE_CREATE"] : $arResult["TIMESTAMP_X"] );
        $datePublished = \FormatDate("Y-m-d", strtotime($datePublished) );
        $dateModified = $arResult["TIMESTAMP_X"];
        $dateModified = \FormatDate("Y-m-d", strtotime($dateModified) );
        $siteLogo = $protocol.$serverName.$arParams["META_TAG"]["LOGO"];

        if(mb_strlen($arResult["DETAIL_TEXT"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["~DETAIL_TEXT"]), 150);
        }
        else if(mb_strlen($arResult["PREVIEW_TEXT"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["~PREVIEW_TEXT"]), 150);
        }
        else if(mb_strlen($arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"]) > 0)
        {
            $seoDescription = $arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"];
        }
        else
        {
            $seoDescription = $arResult["NAME"];
        }
        $seoDescription= preg_replace("/\s{2,}/"," ",$seoDescription);

        if( $arResult["PREVIEW_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["PREVIEW_PICTURE"]["SRC"];
        }
        else if( $arResult["DETAIL_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["DETAIL_PICTURE"]["SRC"];
        }
        else if( $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
        {
            $arMorePhoto = \CFile::GetFileArray(   $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]  ); 
            $seoImage = $protocol.$serverName.$arMorePhoto["SRC"];
        }
        else
        {
            $seoImage = $siteLogo;
        }

        $jsMetaTag = array(
            "@context" => "https://schema.org",
            "@type" => "Article",
            "mainEntityOfPage" => array(
                "@type" => "WebPage",
                "@id" => $seoDetailPage
            ),
            "author" => $serverName,
            "url" => $seoDetailPage,
            "datePublished" => $datePublished,
            "dateModified" => $dateModified,
            "headline" => $seoTitle,
            "image" => array(
                "@type" => "ImageObject",
                "url" => $seoImage,
                "height" => "200",
                "width" => "200"
            ),
            "articleBody" => $seoDescription,
            "publisher" => array(
                "@type" => "Organization",
                "name" => $serverName,
                "logo" => array(
                    "@type" => "ImageObject",
                    "url" => $siteLogo,
                    "height" => "100",
                    "width" => "200"
                )
            )
        );

        $jsMetaTag = json_encode($jsMetaTag,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
 
        $html = '';
        $html .= '<meta property="og:title" content="'.$seoTitle.'" />'."\r\n";
        $html .= '<meta property="og:description" content="'.$seoDescription.'" />'."\r\n";
        $html .= '<meta property="og:type" content="article" />'."\r\n";
        $html .= '<meta property="og:url" content="'.$seoDetailPage.'" />'."\r\n";
        $html .= '<meta property="og:image" content="'.$seoImage.'" />'."\r\n";
        $html .= '<script type="application/ld+json">'.$jsMetaTag.'</script>';

        return $html;
    }

    /**
     * @brief Для страницы со списком статей
     * @param $arResult = массив данных
     * @return Разметка
     **/
	public function MetaTagListItem($arResult)
	{
        global $APPLICATION;

        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        $serverName = SITE_SERVER_NAME;
        $protocol = (\CMain::IsHTTPS()) ? "https://" : "http://"; 
        $currentPage = $APPLICATION->GetCurPage();
        $seoDetailPage = $protocol.$serverName.$currentPage;
        $seoTitle = !empty($arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'] : $arResult['NAME'];
        $siteLogo = $protocol.$serverName.$arParams["META_TAG"]["LOGO"];

        if(mb_strlen($arResult["SECTION"]["PATH"][0]["DESCRIPTION"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["SECTION"]["PATH"][0]["DESCRIPTION"]), 150);
        }
        else if(mb_strlen($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]), 150);
        }
        else
        {
            $seoDescription = $arResult["NAME"];
        }
        $seoDescription= preg_replace("/\s{2,}/"," ",$seoDescription);

        if( $arResult["PREVIEW_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["PREVIEW_PICTURE"]["SRC"];
        }
        else if( $arResult["DETAIL_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["DETAIL_PICTURE"]["SRC"];
        }
        else if( $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
        {
            $arMorePhoto = \CFile::GetFileArray(   $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]  ); 
            $seoImage = $protocol.$serverName.$arMorePhoto["SRC"];
        }
        else
        {
            $seoImage = $siteLogo;
        }

        if($arResult["ITEMS"])
        {
            $jsMetaTag = array(
                "@context" => "https://schema.org",
                "@type" => "ItemList",
                "url" => $seoDetailPage,
                "name" => $seoTitle,
                "numberOfItems" => $arResult["NAV_RESULT"]->NavRecordCount,
                "itemListElement" => array()
            );

            $shemaOrgCount = $arResult["NAV_RESULT"]->NavPageSize * $arResult["NAV_RESULT"]->NavPageNomer - 1;
            foreach($arResult["ITEMS"] as $arItem)
            {
                $seoItenPage = $protocol.$serverName.$arItem["DETAIL_PAGE_URL"];
                $seoItemTitle = !empty($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME'];
                $dateItemPublished = ( ($arItem["DATE_CREATE"]) ? $arItem["DATE_CREATE"] : $arItem["TIMESTAMP_X"] );
                $dateItemPublished = \FormatDate("Y-m-d", strtotime($dateItemPublished) );
                $dateItemModified = $arItem["TIMESTAMP_X"];
                $dateItemModified = \FormatDate("Y-m-d", strtotime($dateItemModified) );
                if( $arItem["PREVIEW_PICTURE"]["SRC"] )
                {
                    $seoItemImage = $protocol.$serverName.$arItem["PREVIEW_PICTURE"]["SRC"];
                }
                else if( $arItem["DETAIL_PICTURE"]["SRC"] )
                {
                    $seoItemImage = $protocol.$serverName.$arItem["DETAIL_PICTURE"]["SRC"];
                }
                else if( $arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
                {
                    $arMorePhoto = \CFile::GetFileArray($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]); 
                    $seoItemImage = $protocol.$serverName.$arMorePhoto["SRC"];
                }
                else
                {
                    $seoItemImage = $siteLogo;
                }
            
                if(mb_strlen($arItem['PREVIEW_TEXT']) > 0)
                {
                    $seoItemDescription = \TruncateText(strip_tags($arItem['~PREVIEW_TEXT']), 150);
                }
                else if(mb_strlen($arItem['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION']) > 0)
                {
                    $seoItemDescription = \TruncateText(strip_tags($arItem['IPROPERTY_VALUES']['ELEMENT_META_DESCRIPTION']), 150);
                }
                else
                {
                    $seoItemDescription = $arItem["NAME"];
                }
                $seoItemDescription= preg_replace("/\s{2,}/"," ",$seoItemDescription);
            
                $jsMetaTag["itemListElement"][] = array(
                    "@type" => "ListItem",
                    "position" => $shemaOrgCount,
                    "item" => array(
                        "@context" => "https://schema.org",
                        "@type" => "Article",
                        "mainEntityOfPage" => array(
                            "@type" => "WebPage",
                            "@id" => $seoItenPage
                        ),
                        "author" => $serverName,
                        "url" => $seoItenPage,
                        "datePublished" => $dateItemPublished,
                        "dateModified" => $dateItemModified,
                        "headline" => $seoItemTitle,
                        "image" => array(
                            "@type" => "ImageObject" ,
                            "url" => $seoItemImage,
                            "height" => "200",
                            "width" => "200"
                        ),
                        "articleBody" => $seoItemDescription,
                        "publisher" => array(
                            "@type" => "Organization ",
                            "name" => $serverName,
                            "logo" => array(
                                "@type" => "ImageObject",
                                "url" => $siteLogo,
                                "height" => "100",
                                "width" => "200"
                            )
                        )
                    )
                );
            
                $shemaOrgCount++;
            }
        
            $jsMetaTag = json_encode($jsMetaTag,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        $html = '';
        $html .= '<meta property="og:title" content="'.$seoTitle.'" />'."\r\n";
        $html .= '<meta property="og:description" content="'.$seoDescription.'" />'."\r\n";
        $html .= '<meta property="og:type" content="article" />'."\r\n";
        $html .= '<meta property="og:url" content="'.$seoDetailPage.'" />'."\r\n";
        $html .= '<meta property="og:image" content="'.$seoImage.'" />'."\r\n";
        
        if($arResult["ITEMS"])
        {
            $html .= '<script type="application/ld+json">'.$jsMetaTag.'</script>';
        }
        
        return $html;
    }

    /**
     * @brief Для страницы со списком статей на минималках
     * @param $arResult = массив данных
     * @return Разметка
     **/
	public function MetaTagListItemLight($arResult)
	{
        global $APPLICATION;
        
        #Получить все настройки модуля
        $arParams = Handler::getOptions();
        
        $serverName = SITE_SERVER_NAME;
        $protocol = (\CMain::IsHTTPS()) ? "https://" : "http://"; 
        $currentPage = $APPLICATION->GetCurPage();
        $seoDetailPage = $protocol.$serverName.$currentPage;
        $seoTitle = !empty($arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'] : $arResult['NAME'];
        $siteLogo = $protocol.$serverName.$arParams["META_TAG"]["LOGO"];

        if(mb_strlen($arResult["SECTION"]["PATH"][0]["DESCRIPTION"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["SECTION"]["PATH"][0]["DESCRIPTION"]), 150);
        }
        else if(mb_strlen($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]), 150);
        }
        else
        {
            $seoDescription = $arResult["NAME"];
        }
        $seoDescription= preg_replace("/\s{2,}/"," ",$seoDescription);

        if( $arResult["PREVIEW_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["PREVIEW_PICTURE"]["SRC"];
        }
        else if( $arResult["DETAIL_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["DETAIL_PICTURE"]["SRC"];
        }
        else if( $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
        {
            $arMorePhoto = \CFile::GetFileArray(   $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]  ); 
            $seoImage = $protocol.$serverName.$arMorePhoto["SRC"];
        }
        else
        {
            $seoImage = $siteLogo;
        }

        if($arResult["ITEMS"])
        {
            $jsMetaTag = array(
                "@context" => "https://schema.org",
                "@type" => "ItemList",
                "url" => $seoDetailPage,
                "name" => $seoTitle,
                "numberOfItems" => $arResult["NAV_RESULT"]->NavRecordCount,
                "itemListElement" => array()
            );

            $shemaOrgCount = $arResult["NAV_RESULT"]->NavPageSize * $arResult["NAV_RESULT"]->NavPageNomer - 1;
            foreach($arResult["ITEMS"] as $arItem)
            {
                $seoItenPage = $protocol.$serverName.$arItem["DETAIL_PAGE_URL"];
                $seoItemTitle = !empty($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME'];
                $dateItemPublished = ( ($arItem["DATE_CREATE"]) ? $arItem["DATE_CREATE"] : $arItem["TIMESTAMP_X"] );
                $dateItemPublished = \FormatDate("Y-m-d", strtotime($dateItemPublished) );
                $dateItemModified = $arItem["TIMESTAMP_X"];
                $dateItemModified = \FormatDate("Y-m-d", strtotime($dateItemModified) );
                if( $arItem["PREVIEW_PICTURE"]["SRC"] )
                {
                    $seoItemImage = $protocol.$serverName.$arItem["PREVIEW_PICTURE"]["SRC"];
                }
                else if( $arItem["DETAIL_PICTURE"]["SRC"] )
                {
                    $seoItemImage = $protocol.$serverName.$arItem["DETAIL_PICTURE"]["SRC"];
                }
                else if( $arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
                {
                    $arMorePhoto = \CFile::GetFileArray($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]); 
                    $seoItemImage = $protocol.$serverName.$arMorePhoto["SRC"];
                }
                else
                {
                    $seoItemImage = $siteLogo;
                }
            
                $jsMetaTag["itemListElement"][] = array(
                  "@type" => "ListItem",
                  "image" => $seoItemImage,
                  "url" => $seoItenPage,
                  "name" => $seoItemTitle,
                  "position" => $shemaOrgCount
                );

                $shemaOrgCount++;
                //break;
            }

            $jsMetaTag = json_encode($jsMetaTag,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        $html = '';
        $html .= '<meta property="og:title" content="'.$seoTitle.'" />'."\r\n";
        $html .= '<meta property="og:description" content="'.$seoDescription.'" />'."\r\n";
        $html .= '<meta property="og:type" content="article" />'."\r\n";
        $html .= '<meta property="og:url" content="'.$seoDetailPage.'" />'."\r\n";
        $html .= '<meta property="og:image" content="'.$seoImage.'" />'."\r\n";
        
        if($arResult["ITEMS"])
        {
            $html .= '<script type="application/ld+json">'.$jsMetaTag.'</script>';
        }
        
        return $html;
    }

    /**
     * @brief Для детальной страницы продукта
     * @param $arResult = массив данных
     * @return Разметка
     **/
	public function MetaTagProduct($arResult)
	{
        #Получить все настройки модуля
        $arParams = Handler::getOptions();
       
        $serverName = SITE_SERVER_NAME;
        $protocol = (\CMain::IsHTTPS()) ? "https://" : "http://"; 
        $seoDetailPage = $protocol.$serverName.$arResult["DETAIL_PAGE_URL"];
        $seoTitle = !empty($arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arResult['NAME'];
        $siteLogo = $protocol.$serverName.$arParams["META_TAG"]["LOGO"];

        if(mb_strlen($arResult["DETAIL_TEXT"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["~DETAIL_TEXT"]), 150);
        }
        else if(mb_strlen($arResult["PREVIEW_TEXT"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["~PREVIEW_TEXT"]), 150);
        }
        else if(mb_strlen($arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"]) > 0)
        {
            $seoDescription = $arResult["IPROPERTY_VALUES"]["ELEMENT_META_DESCRIPTION"];
        }
        else
        {
            $seoDescription = $arResult["NAME"];
        }
        $seoDescription= preg_replace("/\s{2,}/"," ",$seoDescription);

        if( $arResult["PREVIEW_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["PREVIEW_PICTURE"]["SRC"];
        }
        else if( $arResult["DETAIL_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["DETAIL_PICTURE"]["SRC"];
        }
        else if( $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
        {
            $arMorePhoto = \CFile::GetFileArray(   $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]  ); 
            $seoImage = $protocol.$serverName.$arMorePhoto["SRC"];
        }
        else
        {
            $seoImage = $siteLogo;
        }

        $jsMetaTag = array(
            "@context" => "https://schema.org",
            "@type" => "Product",
            "description" => $seoDescription, 
            "name" => $seoTitle,
            "image" => array(
                "@type" => "ImageObject",
                "url" => $seoImage,
                "height" => "200",
                "width" => "200"
            ),
            "offers" => array(
			    "@type" => "Offer",
			    "availability" => "http://schema.org/InStock",
			    "price" => $arResult['MIN_PRICE']['VALUE'],
			    "priceCurrency" => "RUB"
			)
        );

        $jsMetaTag = json_encode($jsMetaTag,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
 
        $html = '';
        $html .= '<meta property="og:title" content="'.$seoTitle.'" />'."\r\n";
        $html .= '<meta property="og:description" content="'.$seoDescription.'" />'."\r\n";
        $html .= '<meta property="og:type" content="product" />'."\r\n";
        $html .= '<meta property="og:url" content="'.$seoDetailPage.'" />'."\r\n";
        $html .= '<meta property="og:image" content="'.$seoImage.'" />'."\r\n";
        $html .= '<script type="application/ld+json">'.$jsMetaTag.'</script>';

        return $html;
    }

    /**
     * @brief Для страницы список продуктов
     * @param $arResult = массив данных
     * @return Разметка
     **/
	public function MetaTagProductItem($arResult)
	{
        global $APPLICATION;

        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        $serverName = SITE_SERVER_NAME;
        $protocol = (\CMain::IsHTTPS()) ? "https://" : "http://"; 
        $currentPage = $APPLICATION->GetCurPage();
        $seoDetailPage = $protocol.$serverName.$currentPage;
        $seoTitle = !empty($arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE']) ? $arResult['IPROPERTY_VALUES']['SECTION_PAGE_TITLE'] : $arResult['NAME'];
        $siteLogo = $protocol.$serverName.$arParams["META_TAG"]["LOGO"];

        if(mb_strlen($arResult["SECTION"]["PATH"][0]["DESCRIPTION"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["SECTION"]["PATH"][0]["DESCRIPTION"]), 150);
        }
        else if(mb_strlen($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]) > 0)
        {
            $seoDescription = \TruncateText(strip_tags($arResult["IPROPERTY_VALUES"]["SECTION_META_DESCRIPTION"]), 150);
        }
        else
        {
            $seoDescription = $arResult["NAME"];
        }
        $seoDescription= preg_replace("/\s{2,}/"," ",$seoDescription);

        if( $arResult["PREVIEW_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["PREVIEW_PICTURE"]["SRC"];
        }
        else if( $arResult["DETAIL_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["DETAIL_PICTURE"]["SRC"];
        }
        else if( $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
        {
            $arMorePhoto = \CFile::GetFileArray(   $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]  ); 
            $seoImage = $protocol.$serverName.$arMorePhoto["SRC"];
        }
        else
        {
            $seoImage = $siteLogo;
        }

        if($arResult["ITEMS"])
        {
            $jsMetaTag = array(
                "@context" => "https://schema.org",
                "@type" => "ListItem",
                "position" => 1,
                "item" => array()
            );

            $shemaOrgCount = $arResult["NAV_RESULT"]->NavPageSize * $arResult["NAV_RESULT"]->NavPageNomer - 1;
            foreach($arResult["ITEMS"] as $arItem)
            {
                $seoItenPage = $protocol.$serverName.$arItem["DETAIL_PAGE_URL"];
                $seoItemTitle = !empty($arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE']) ? $arItem['IPROPERTY_VALUES']['ELEMENT_PAGE_TITLE'] : $arItem['NAME'];

                if( $arItem["PREVIEW_PICTURE"]["SRC"] )
                {
                    $seoItemImage = $protocol.$serverName.$arItem["PREVIEW_PICTURE"]["SRC"];
                }
                else if( $arItem["DETAIL_PICTURE"]["SRC"] )
                {
                    $seoItemImage = $protocol.$serverName.$arItem["DETAIL_PICTURE"]["SRC"];
                }
                else if( $arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
                {
                    $arMorePhoto = \CFile::GetFileArray($arItem["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]); 
                    $seoItemImage = $protocol.$serverName.$arMorePhoto["SRC"];
                }
                else
                {
                    $seoItemImage = $siteLogo;
                }

            	$haveOffers = !empty($arItem['OFFERS']);
            	if ($haveOffers)
            	{
            		$actualItem = isset($arItem['OFFERS'][$arItem['OFFERS_SELECTED']])
            			? $arItem['OFFERS'][$arItem['OFFERS_SELECTED']]
            			: reset($arItem['OFFERS']);
            	}
            	else
            	{
            		$actualItem = $arItem;
            	}
            
                $price = $actualItem['ITEM_PRICES'][$actualItem['ITEM_PRICE_SELECTED']];
   
                $jsMetaTag["item"][] = array(
                    "@type" => "Product",
                    "url" => $seoItenPage,
                    "name" => $seoItemTitle,
                    "image" => array(
                        "@type" => "ImageObject" ,
                        "url" => $seoItemImage,
                        "height" => "200",
                        "width" => "200"
                    ),
                    "offers" => array(
                       "@type" => "Offer",
                       "price" => $price["PRICE"],
                       "priceCurrency" => $price["CURRENCY"]
                    )
                );
            }

            $jsMetaTag = json_encode($jsMetaTag,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        }

        $html = '';
        $html .= '<meta property="og:title" content="'.$seoTitle.'" />'."\r\n";
        $html .= '<meta property="og:description" content="'.$seoDescription.'" />'."\r\n";
        $html .= '<meta property="og:type" content="product" />'."\r\n";
        $html .= '<meta property="og:url" content="'.$seoDetailPage.'" />'."\r\n";
        $html .= '<meta property="og:image" content="'.$seoImage.'" />'."\r\n";
        
        if($arResult["ITEMS"])
        {
            $html .= '<script type="application/ld+json">'.$jsMetaTag.'</script>';
        }
        
        return $html;
    }

    /**
     * @brief Для страницы Контакты
     * @param $arResult = массив данных
     * @return Разметка
     **/
	public function MetaTagOrganization($arResult)
	{
        #Получить все настройки модуля
        $arParams = Handler::getOptions();
        
        $serverName = SITE_SERVER_NAME;
        $protocol = (\CMain::IsHTTPS()) ? "https://" : "http://"; 
        $seoDetailPage = $protocol.$serverName.$arResult["DETAIL_PAGE_URL"];
        $seoTitle = !empty($arResult["NAME"]) ? $arResult["NAME"] : $serverName;
        $siteLogo = $protocol.$serverName.$arParams["META_TAG"]["LOGO"];
        $seoImage = $siteLogo;
        $seoDescription = "";

        if( $arResult["PREVIEW_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["PREVIEW_PICTURE"]["SRC"];
        }
        else if( $arResult["DETAIL_PICTURE"]["SRC"] )
        {
            $seoImage = $protocol.$serverName.$arResult["DETAIL_PICTURE"]["SRC"];
        }
        else if( $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0] )
        {
            $arMorePhoto = \CFile::GetFileArray(   $arResult["PROPERTIES"]["MORE_PHOTO"]["VALUE"][0]  ); 
            $seoImage = $protocol.$serverName.$arMorePhoto["SRC"];
        }
        else
        {
            $seoImage = $siteLogo;
        }

        $jsMetaTag = array(
        	"@context" => array(
                "@vocab" => "https://schema.org/"
            ),
        	"@graph" => array()
        );
        
       foreach($arResult as $arItem)
       {
            $arOrganization = array();
            
            $arOrganization["@type"] = "LocalBusiness";
            $arOrganization["name"] = ( ($arItem["NAME"]) ? $arItem["NAME"] : $serverName );
            $arOrganization["image"] = $siteLogo;
            $arOrganization["description"] = ( ($arItem["DESCRIPTION"]) ? $arItem["DESCRIPTION"] : $serverName );
            $seoDescription .= $arItem["DESCRIPTION"].' / ';
            
            if($arItem["PRICE_RANGE"])
            {
                $arOrganization["priceRange"] = $arItem["PRICE_RANGE"];
            }
            
            if($arItem["FACRBOOK"])
            {
                $arOrganization["sameAs"] = array($arItem["FACRBOOK"]);
            }
            
            if($arItem["GEO_LATITUDE"] && $arItem["GEO_LONGITUDE"])
            {
                $arOrganization["geo"] = array(
    	            "@type" => "GeoCoordinates",
	                "latitude" => $arItem["GEO_LATITUDE"],
	                "longitude" => $arItem["GEO_LONGITUDE"],
                );
            }
            
            $arOrganization["address"] = array("@type" => "PostalAddress");
            
            if($arItem["STREET_ADDRESS"])
            {
                $arOrganization["address"]["streetAddress"] = $arItem["STREET_ADDRESS"];
            }

            if($arItem["ADDRESS_LOCALITY"])
            {
                $arOrganization["address"]["addressLocality"] = $arItem["ADDRESS_LOCALITY"];
            }

            if($arItem["POSTAL_CODE"])
            {
                $arOrganization["address"]["postalCode"] = $arItem["POSTAL_CODE"];
            }

            if($arItem["TELEPHONE"])
            {
                $arOrganization["address"]["telephone"] = $arItem["TELEPHONE"];
            }

            if($arItem["FAX_NUMBER"])
            {
                $arOrganization["address"]["faxNumber"] = $arItem["FAX_NUMBER"];
            }

            if($arItem["EMAIL"])
            {
                $arOrganization["address"]["email"] = $arItem["EMAIL"];
            }
            
            
            $jsMetaTag["@graph"][] = $arOrganization;
       }

        $jsMetaTag = json_encode($jsMetaTag,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);
        
        $html = '';
        $html .= '<meta property="og:title" content="'.$seoTitle.'" />'."\r\n";
        $html .= '<meta property="og:description" content="'.trim($seoDescription).'" />'."\r\n";
        $html .= '<meta property="og:type" content="article" />'."\r\n";
        $html .= '<meta property="og:url" content="'.$seoDetailPage.'" />'."\r\n";
        $html .= '<meta property="og:image" content="'.$seoImage.'" />'."\r\n";
        
        if($arResult)
        {
            $html .= '<script type="application/ld+json">'.$jsMetaTag.'</script>';
        }
        
        return $html;
    }
}