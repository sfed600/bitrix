<?php
namespace Webtu\ExpansionSite\Mysql;

use \Bitrix\Main\Entity;
use \Bitrix\Main\Type;

class IntegrationStatusTable extends Entity\DataManager
{
    public static function getTableName()
    {
        return 'webtu_integration_status';
    }

    public static function getUfId()
    {
        return 'WEBTU_INTEGRATION_STATUS';
    }

    public static function getMap()
    {
        return array(
            // ID
            new Entity\IntegerField('ID', array(
                'primary' => true,
                'autocomplete' => true
            )),
            // ID quest
            new Entity\StringField('QUEST_ID', array(
                'required' => true
            )),
            // TYPE error
            new Entity\StringField('TYPE', array(
                'required' => true
            )),
            // STATUS error
            new Entity\StringField('STATUS', array(
                'required' => true
            )),
        );
    }
}