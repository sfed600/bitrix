<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Application;

// sort by
$request = Application::getInstance()->getContext()->getRequest();

$sort = 'TIMESTAMP_X';
$by = 'DESC';
if (stristr($request->getQuery("SORT"), 'SHOWS')) {
	$arResult['SORT_SELECTED'] = 'selected';
}

// to url sections
$r = CIBlockSection::GetList(
	array(),
	array("IBLOCK_ID" => $arParams["IBLOCK_ID"]),
	false,
	array("ID","NAME", "CODE", "IBLOCK_ID")
);
while ($i = $r->GetNext()) {
	$arResult['SECTION_LIST'][$i['CODE']]['NAME'] = $i['NAME'];
	if (stristr($APPLICATION->GetCurPage(), $i['CODE'])) {
		$arResult['SECTION_LIST'][$i['CODE']]['SELECTED'] = 'selected';
	}
}
