(function() {
	'use strict';

    if (!!window.CLComponent)
        return;

    window.CLComponent = function(arParams) {

        this.config = {};
        this.data = {};
        this.errorCode = 0;

        this.element = null;

        if (typeof arParams === 'object')
        {
            this.params = arParams;
            this.initConfig();

            if (this.errorCode === 0)
            {
                BX.ready(BX.delegate(this.init, this));
            }

            this.params = {};
        }
    };

    window.CLComponent.prototype =
	{
		initConfig: function()
		{
			if (
				!this.params.CONFIG ||
				typeof this.params.CONFIG !== 'object' ||
				!this.params.CONFIG.PATH ||
				!this.params.CONFIG.ELEMENT_ID ||
				!this.params.CONFIG.PAGE_BUY
			)
			{
				this.errorCode = -1;
				return;
			}
			this.config = this.params.CONFIG;

		},
		init: function()
		{   
			this.element = document.getElementById(this.config.ELEMENT_ID);

			if( this.element )
			{
                if ( $(this.element).find('input[name="CERTIFICATE_MIN_COST"]').length == 0 ) {
                    BX.bind(this.element , 'click', BX.delegate(this.elementBtnAction, this));
                } else {
                    BX.bind(this.element , 'keyup', BX.delegate(this.elementBtnActionKeyUp, this));
                    BX.bind(this.element , 'click', BX.delegate(this.elementBtnActionUserPrice, this));
                }
			}
		},

        elementBtnActionUserPrice: function() {
            if ($(this.element).hasClass('is-active')) {
                return false;
            } else {
                this.elementBtnActionFirstClick();
            }
        },

        elementBtnActionFirstClick: function() {
            this.elementBtnAction($(this.element).find('input[name="CERTIFICATE_MIN_COST"]').val());
        },

        elementBtnActionKeyUp: function() {
            var userPrice = $(this.element).find('input[name="CERTIFICATE_MIN_COST"]').val();
            var minCost = $(this.element).find('input[name="CERTIFICATE_MIN_COST"]').data('min_cost');

            if (parseInt(userPrice) > parseInt(minCost)) {
                this.elementBtnAction(userPrice);
            }
        },

        elementBtnAction: function(userPrice)
		{
            var element = BX.proxy_context;

            if (element)
			{
                if (parseInt(userPrice) > 0) {
                    this.data.PROP = {
                        QUEST_LIST: this.config.QUEST_LIST,
                        BASE_PRICE: this.config.BASE_PRICE,
                        ACTION: "QUEST_LIST",
                        CERTIFICATE_MIN_COST: this.config.CERTIFICATE_MIN_COST,
                        USER_PRICE: $('input[name="CERTIFICATE_MIN_COST"]').val()
                    };
                } else {
                    this.data.PROP = {
                        QUEST_LIST: this.config.QUEST_LIST,
                        ACTION: "QUEST_LIST",
                    };
                }

                this.loadingPage();

                var url = ( (this.config.PATH) ? this.config.PATH+'/ajax.php' : "/local/components/webtu/certificate.list/ajax.php" );
                
                BX.ajax({
                    url: url,
                    method: 'POST',
                    dataType: 'json',
                    timeout: 1000,
                    data: this.data,
                    onsuccess: BX.delegate(function(result){
                        if(!result){ return; }

                        this.loadingPageDelete();

                        if(result.MESSAGE_ERROR.length === 0)
                        {
                            if ( $(element).find('input[name="CERTIFICATE_MIN_COST"]').length == 0 )
                                document.getElementById('buy-certificate').setAttribute('href', this.config.PAGE_BUY);
                            else
                                document.getElementById('buy-certificate').setAttribute('href', this.config.PAGE_BUY + '?ability_cost=' + $('[name="CERTIFICATE_MIN_COST"]').val());

                            if (result.RES_HTML)
                            {
                                var processed = BX.processHTML(result.RES_HTML, false);


                                document.getElementById('quest-list').innerHTML = processed.HTML;

                                BX.ajax.processScripts(processed.SCRIPT);
                            }
                        }
                        else
						{

						}
                    }, this),
                    onfailure: function(){

                    }
                });

			}
		},
		loadingPage: function()
		{
			$('.js-preloader').addClass('is-active');
		},
		loadingPageDelete: function()
		{
			$('.js-preloader').removeClass('is-active');
		},

	};

    /*$('[name="CERTIFICATE_MIN_COST"]').on('keyup', function () {
        if ($(this).val().length >= 4) {
            if (parseInt($(this).val()) < parseInt($(this).data('min_cost'))) {
                $(this).val($(this).data('min_cost'));
                return false;
            }
        }
    });*/

})();