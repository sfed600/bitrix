<?
$MESS["BOOKING_NOT_INSTALLED_MODULE"] = "Модуль #ID# не установлен!";
$MESS["BOOKING_ERROR_NONE_PARAMS"] = "Отсутсвуют параметры";
$MESS["BOOKING_ERROR_CURL_NONE_INSTALLER"] = "Не возможно запросить данные по CURL (Не установлен)";
$MESS["BOOKING_ERROR_CODE"] = "Ошибка запроса данных код ошибки #CODE#";
$MESS["BOOKING_JSON_VALID"] = "JSON файл корректен";
$MESS["BOOKING_JSON_ERROR_DEPTH"] = "Достигнута максимальная глубина стека";
$MESS["BOOKING_JSON_ERROR_STATE_MISMATCH"] = "Некорректные разряды или несоответствие режимов";
$MESS["BOOKING_JSON_ERROR_CTRL_CHAR"] = "Некорректный управляющий символ";
$MESS["BOOKING_JSON_ERROR_SYNTAX"] = "Синтаксическая ошибка, некорректный JSON";
$MESS["BOOKING_JSON_ERROR_UTF8"] = "Некорректные символы UTF-8, возможно неверно закодирован";
$MESS["BOOKING_JSON_ERROR_EMPTY"] = "Неизвестная ошибка JSON файла";
$MESS[""] = "";








?>