<?php

namespace Webtu\ExpansionSite;

use Bitrix\Main;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale;

Loc::loadMessages(__FILE__);

class Bonus
{
    private static $messageError = array();

    /**
     * @brief Инициализация
     **/
    public static function init()
    {
    }

    /**
     * @param $userId
     * @return string
     * @throws Main\ArgumentException
     */
    public static function setPartnerLink($userId)
    {
        #Получить все настройки модуля
        $arOptions = Handler::getOptions();

        $domain = $_SERVER['HTTP_HOST'];
        $http = (\CMain::IsHTTPS()) ? "https://" : "http://";

        $r = \Bitrix\Main\SiteTable::getList(array('select' => array('SERVER_NAME')));
        if ($s = $r->Fetch()) {
            $domain = $s['SERVER_NAME'];
        }
        $link = $http . $domain . '?invite=' . base64_encode($userId . $arOptions['BONUS']['PARTNER_HASH']);

        return $link;
    }

    /**
     * @param $teamId
     * @return string
     * @throws Main\ArgumentException
     */
    public static function setTeamInviteLink($teamId)
    {
        #Получить все настройки модуля
        $arOptions = Handler::getOptions();

        $domain = $_SERVER['HTTP_HOST'];
        $http = (\CMain::IsHTTPS()) ? "https://" : "http://";

        $r = \Bitrix\Main\SiteTable::getList(array('select' => array('SERVER_NAME')));
        if ($s = $r->Fetch()) {
            $domain = $s['SERVER_NAME'];
        }
        $link = $http . $domain . '?invite_team=' . base64_encode($teamId . $arOptions['BONUS']['PARTNER_HASH']);

        return $link;
    }

    /**
     * @brief Зачисление при привязке соц сети
     * @param $user_id
     * @throws Main\LoaderException
     */

    public static function userSuncSocialNetwork($user_id)
    {
        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        if (
            $arParams["BONUS"]["EVENT_SUNC_SOCIAL_NETWORK"] == "Y"
            && intval($arParams["BONUS"]["EVENT_SUNC_SOCIAL_NETWORK_SUM"]) > 0
            && intval($user_id) > 0
        ) {
            $arTransact = array();

            $user = new \CUser();
            $rsUser = $user->GetByID($user_id);
            $arUser = $rsUser->Fetch();

            if (
                $arUser["UF_SUNC_SOCNET_BONUS"] != 1
                && $arUser[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["BONUS"]["TYPE_USER_VALUE"]
            ) {
                $sum = $arParams["BONUS"]["EVENT_SUNC_SOCIAL_NETWORK_SUM"];
                $currency = (($arParams["BONUS"]["CURRENCY"]) ? $arParams["BONUS"]["CURRENCY"] : "RUB");

                $description = Loc::getMessage('WEBTU_ES_MESSAGE_EVENT_SUNC_SOCIAL_NETWORK', Array("#SUMM#" => $sum));
                $deductDescription = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_EVENT_SUNC_SOCIAL_NETWORK', Array("#SUMM#" => $sum));

                $notes = serialize(array("DEDUCT" => "N", "DEDUCT_TRANSACT_ID" => "", "DEDUCT_DESCRIPTION" => $deductDescription));

                $arTransact = self::userUpdateAccount($user_id, $sum, $currency, $description, 0, $notes);

                if ($arTransact["TRANSACT_ID"] > 0) {
                    $user->Update($user_id, array("UF_SUNC_SOCNET_BONUS" => 1));
                }

            }
        }
    }

    /**
     * @brief Зачисление при регистрации нового пользователя
     * @param $arFields
     * @throws Main\LoaderException
     */
    public static function userRegister(&$arFields)
    {
        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        if (
            $arParams["BONUS"]["EVENT_REGISTER_INVITE"] == "Y"
            && intval($arParams["BONUS"]["EVENT_REGISTER_SUM_2"]) > 0
            && intval($_SESSION["BONUS_REGISTER"]["USER_ID"]) > 0

        ) {
            $sum = $arParams["BONUS"]["EVENT_REGISTER_SUM_2"];
        } else {
            $sum = $arParams["BONUS"]["EVENT_REGISTER_SUM"];
        }


        if (intval($sum) > 0 && $arFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["BONUS"]["TYPE_USER_VALUE"]) {
            $userId = $arFields["USER_ID"];
            $currency = (($arParams["BONUS"]["CURRENCY"]) ? $arParams["BONUS"]["CURRENCY"] : "RUB");

            if (intval($_SESSION["BONUS_REGISTER"]["USER_ID"]) > 0) {
                $description = Loc::getMessage('WEBTU_ES_MESSAGE_EVENT_REGISTER_INVITE', Array("#SUMM#" => $sum));
            } else {
                $description = Loc::getMessage('WEBTU_ES_MESSAGE_EVENT_REGISTER', Array("#SUMM#" => $sum));
            }


            if (mb_strlen($description) <= 5) {
                $description = Loc::getMessage('WEBTU_ES_MESSAGE_EVENT_DEFAULT', Array("#SUMM#" => $sum));
            }

            if (intval($_SESSION["BONUS_REGISTER"]["USER_ID"]) > 0) {
                $deductDescription = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_REGISTER_INVITE', Array("#SUMM#" => $sum));
            } else {
                $deductDescription = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_REGISTER', Array("#SUMM#" => $sum));
            }

            if (mb_strlen($deductDescription) <= 5) {
                $deductDescription = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_DEFAULT', Array("#SUMM#" => $sum));
            }

            $notes = serialize(array("DEDUCT" => "N", "DEDUCT_TRANSACT_ID" => "", "DEDUCT_DESCRIPTION" => $deductDescription));

            self::userUpdateAccount($userId, $sum, $currency, $description, 0, $notes);
        }
    }

    public function userRegisterInvite(&$arFields)
    {
        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        $sum = $arParams["BONUS"]["EVENT_REGISTER_INVITE_SUM"];
        $user_id = (int)$_SESSION["BONUS_REGISTER"]["USER_ID"];

        if (
            $user_id > 0
            && intval($sum) > 0
        ) {
            $currency = (($arParams["BONUS"]["CURRENCY"]) ? $arParams["BONUS"]["CURRENCY"] : "RUB");

            $description = Loc::getMessage('WEBTU_ES_MESSAGE_EVENT_USER_REGISTER_INVITE', Array("#SUMM#" => $sum, "#NAME#" => $arFields["NAME"] . ' ' . $arFields["LAST_NAME"]));

            if (mb_strlen($description) <= 5) {
                $description = Loc::getMessage('WEBTU_ES_MESSAGE_EVENT_DEFAULT', Array("#SUMM#" => $sum));
            }

            $deductDescription = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_USER_REGISTER_INVITE', Array("#SUMM#" => $sum, "#NAME#" => $arFields["NAME"] . ' ' . $arFields["LAST_NAME"]));

            if (mb_strlen($deductDescription) <= 5) {
                $deductDescription = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_DEFAULT', Array("#SUMM#" => $sum));
            }


            $notes = serialize(array("DEDUCT" => "N", "DEDUCT_TRANSACT_ID" => "", "DEDUCT_DESCRIPTION" => $deductDescription));

            self::userUpdateAccount($user_id, $sum, $currency, $description, 0, $notes);

        }
    }

    /**
     * @brief Зачисление при оплате заказа
     * @param $ID
     * @return bool
     * @throws Main\ArgumentNullException
     * @throws Main\LoaderException
     * @throws Main\NotImplementedException
     */
    public static function userPayOrder($ID)
    {
        if (!Loader::includeModule("sale")) {
            return false;
        }

        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        if (
            intval($arParams["BONUS"]["CASHBACK_MIN"]) > 0
        ) {
            $order = Sale\Order::load($ID);
            if ($order) {
                $listOrders = $order->getFieldValues();

                $userId = $listOrders["USER_ID"];
                $obUser = \CUser::GetByID($userId);
                $arUser = $obUser->Fetch();

                $orderPrice = round($listOrders["PRICE"]);
                $cashBackMin = ((round($arUser["UF_CASHBACK"]) > 0) ? round($arUser["UF_CASHBACK"]) : round($arParams["BONUS"]["CASHBACK_MIN"]));
                $sum = round($orderPrice / 100 * $cashBackMin);

                $currency = (($arParams["BONUS"]["CURRENCY"]) ? $arParams["BONUS"]["CURRENCY"] : "RUB");

                $description = Loc::getMessage('WEBTU_ES_MESSAGE_EVENT_PAY_ORDER', Array("#SUMM#" => $sum, "#ORDER_ID#" => $ID));
                if (mb_strlen($description) <= 5) {
                    $description = Loc::getMessage('WEBTU_ES_MESSAGE_EVENT_DEFAULT', Array("#SUMM#" => $sum));
                }

                $deductDescription = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_PAY_ORDER', Array("#SUMM#" => $sum, "#ORDER_ID#" => $ID));
                if (mb_strlen($deductDescription) <= 5) {
                    $deductDescription = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_DEFAULT', Array("#SUMM#" => $sum));
                }

                $notes = serialize(array("DEDUCT" => "N", "DEDUCT_TRANSACT_ID" => "", "DEDUCT_DESCRIPTION" => $deductDescription));

                #Начислять бонусы всем пользователям по событию
                if ($arParams["BONUS"]["ENROL_USERS_ALL"] == "Y" && $sum > 0) {
                    self::userUpdateAccount($userId, $sum, $currency, $description, 0, $notes);
                }
            }
        }
    }

    /**
     * @brief Списание со счёта
     * @throws Main\LoaderException
     */
    public static function userDeductAccount()
    {
        global $DB;

        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        if ($arParams["BONUS"]["DEDUCT"] == "Y" && intval($arParams["BONUS"]["DEDUCT_DAY"]) > 0) {
            #Дата
            $deductMktime = $arParams["BONUS"]["DEDUCT_DAY"] * 86400;
            $arDate["DATE_ONLINE"] = date("d.m.Y 00:00:00");
            $arDate["DATE_ONLINE_MKTIME"] = mktime("00", "00", "00", date("n"), date("j"), date("Y"));

            #За сегодня
            $arDate["TRANSACT_DATE_MKTIME"] = mktime(date("H"), date("i"), date("s"), date("n"), date("j"), date("Y"));

            #За вчера
            //$arDate["TRANSACT_DATE_MKTIME"] = $arDate["DATE_ONLINE_MKTIME"] - 86400;

            #По счётчику
            //$arDate["TRANSACT_DATE_MKTIME"] = $arDate["DATE_ONLINE_MKTIME"] - $deductMktime;
            $arDate["TRANSACT_DATE_FROM"] = date("d.m.Y 00:00:00", $arDate["TRANSACT_DATE_MKTIME"]);
            $arDate["TRANSACT_DATE_TO"] = date("d.m.Y 24:59:59", $arDate["TRANSACT_DATE_MKTIME"]);

            #История начисленных транзакций за период 
            $arFilter = array(
                ">=TRANSACT_DATE" => $arDate["TRANSACT_DATE_FROM"],
                "<=TRANSACT_DATE" => $arDate["TRANSACT_DATE_TO"],
                "DEBIT" => "Y"
            );

            $obTransactList = \CSaleUserTransact::GetList(Array("ID" => "DESC"), $arFilter);
            while ($arTransactList = $obTransactList->Fetch()) {
                $transactParams = unserialize($arTransactList["NOTES"]);

                #Баланс пользователя
                $resUserAccount = self::userAccountPay($arTransactList["USER_ID"]);

                if ($resUserAccount["ACCOUNT_SUM"] > 0 && $transactParams["DEDUCT"] != "Y") {
                    $userId = $arTransactList["USER_ID"];
                    $sum = (($resUserAccount["ACCOUNT_SUM"] < $arTransactList["AMOUNT"]) ? $resUserAccount["ACCOUNT_SUM"] : $arTransactList["AMOUNT"]);
                    $currency = (($resUserAccount["CURRENCY"]) ? $resUserAccount["CURRENCY"] : "RUB");

                    $description = $transactParams["DEDUCT_DESCRIPTION"];
                    if (mb_strlen($description) <= 5) {
                        $description = Loc::getMessage('WEBTU_ES_MESSAGE_DEDUCT_DEFAULT', Array("#SUMM#" => $sum));
                    }

                    $notes = serialize(array("TRANSACT_ID" => $arTransactList["ID"]));

                    #Обновление аккаунта пользователя
                    $resUserUpdateAccount = self::userUpdateAccount($userId, -$sum, $currency, $description, 0, $notes);

                    if ($resUserUpdateAccount["TRANSACT_ID"]) {
                        $transactParams["DEDUCT"] = "Y";
                        $transactParams["DEDUCT_TRANSACT_ID"] = $resUserUpdateAccount["TRANSACT_ID"];
                        $transactParams = serialize($transactParams);

                        #Обновление текущей транзакции
                        \CSaleUserTransact::Update($arTransactList["ID"], array("NOTES" => $transactParams));
                    }
                }
            }#end while $obTransactList
        }
    }

    /**
     * @brief Обновление аккаунта пользователя
     * @param $userId
     * @param $sum
     * @param $currency
     * @param string $description
     * @param int $orderID
     * @param string $notes
     * @param null $paymentId
     * @return array
     * @throws Main\LoaderException
     */
    public static function userUpdateAccount($userId, $sum, $currency, $description = "", $orderID = 0, $notes = "", $paymentId = null)
    {
        global $USER, $DB, $APPLICATION;

        #Проверка и установка переменных
        if (!Loader::includeModule("sale")) {
            return false;
        }
        if (intval($sum) == 0) {
            return false;
        }

        $userId = (int)$userId;
        if ($userId <= 0) {
            array_push(self::$messageError, Loc::getMessage('WEBTU_ES_ERROR_CLASS', Array("#METOD#" => "userUpdateAccount", "MESSAGE" => "USER_ID")));
            return false;
        }

        $dbUser = \CUser::GetByID($userId);
        if (!$dbUser->Fetch()) {
            array_push(self::$messageError, Loc::getMessage('WEBTU_ES_ERROR_CLASS', Array("#METOD#" => "userUpdateAccount", "MESSAGE" => "USER_ID")));
            return false;
        }

        $sum = (double)str_replace(",", ".", $sum);

        $currency = trim($currency);
        if ($currency === '') {
            array_push(self::$messageError, Loc::getMessage('WEBTU_ES_ERROR_CLASS', Array("#METOD#" => "userUpdateAccount", "MESSAGE" => "EMPTY_CURRENCY")));
            return;
        }

        $orderID = (int)$orderID;

        $paymentId = (int)$paymentId;
        if (!\CSaleUserAccount::Lock($userId, $currency)) {
            array_push(self::$messageError, Loc::getMessage('WEBTU_ES_ERROR_CLASS', Array("#METOD#" => "userUpdateAccount", "MESSAGE" => "ACCOUNT_NOT_LOCKED")));
            return false;
        }

        $currentBudget = 0.0;
        $result = false;

        #Обновление счёта пользователя
        $dbUserAccount = \CSaleUserAccount::GetList(array(), array("USER_ID" => $userId, "CURRENCY" => $currency));
        if ($arUserAccount = $dbUserAccount->Fetch()) {
            $currentBudget = floatval($arUserAccount["CURRENT_BUDGET"]);
            $arFields = array("CURRENT_BUDGET" => $arUserAccount["CURRENT_BUDGET"] + $sum);
            $result = \CSaleUserAccount::Update($arUserAccount["ID"], $arFields);
        } else {
            $currentBudget = floatval($sum);
            $arFields = array(
                "USER_ID" => $userId,
                "CURRENT_BUDGET" => $sum,
                "CURRENCY" => $currency,
                "LOCKED" => "Y",
                "DATE_LOCKED" => date($DB->DateFormatToPHP(\CSite::GetDateFormat("FULL", SITE_ID)))
            );
            $result = \CSaleUserAccount::Add($arFields);
        }

        #Добавление транзакции
        if ($result) {
            $arFields = array(
                "USER_ID" => $userId,
                "TRANSACT_DATE" => date($DB->DateFormatToPHP(\CSite::GetDateFormat("FULL", SITE_ID))),
                "CURRENT_BUDGET" => $currentBudget,
                "AMOUNT" => $sum > 0 ? $sum : -$sum,
                "CURRENCY" => $currency,
                "DEBIT" => $sum > 0 ? "Y" : "N",
                "ORDER_ID" => $orderID > 0 ? $orderID : false,
                "PAYMENT_ID" => $paymentId > 0 ? $paymentId : false,
                "DESCRIPTION" => strlen($description) > 0 ? $description : false,
                "NOTES" => strlen($notes) > 0 ? $notes : false,
                "EMPLOYEE_ID" => $USER->IsAuthorized() ? $USER->GetID() : false
            );

            $resTransact = \CSaleUserTransact::Add($arFields);
        }

        #Разблокировать Счёт
        \CSaleUserAccount::UnLock($userId, $currency);

        #Проверка на отрицательный баланс (отправка почтового сообщения) - организатор квестов
        self::organizationBalanceReduction($userId, $currency);

        return array(
            "ACCOUNT_ID" => $result,
            "TRANSACT_ID" => $resTransact
        );
    }

    /**
     * @brief Баланс пользователя
     * @param $userId
     * @return array|void
     */
    public static function userAccountPay($userId)
    {
        if (intval($userId) <= 0) {
            return;
        }

        $arResult = array(
            "ACCOUNT_LIST" => array(),
            "ACCOUNT_SUM" => 0
        );

        #Баланс
        $accountList = \CSaleUserAccount::GetList(
            array("CURRENCY" => "ASC"),
            array("USER_ID" => (int)$userId),
            false,
            false,
            array("ID", "CURRENT_BUDGET", "CURRENCY", "TIMESTAMP_X")
        );
        while ($account = $accountList->Fetch()) {
            $account["SUM"] = round($account["CURRENT_BUDGET"]);
            $arResult["ACCOUNT_LIST"][] = $account;
            $arResult["ACCOUNT_SUM"] = $arResult["ACCOUNT_SUM"] + $account["SUM"];
        }

        return $arResult;
    }

    /**
     * @param $userId
     * @param $currency
     * @return bool
     * @throws Main\LoaderException
     */
    public static function organizationBalanceReduction($userId, $currency)
    {
        #Проверка и установка переменных
        if (!Loader::includeModule("sale")) {
            return false;
        }

        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        $rsUser = \CUser::GetByID($userId);
        $arUser = $rsUser->Fetch();

        #Если тип пользователь - организация
        if ($arUser[$arParams["PROPS"]["TYPE_USER"]["CODE"]] == $arParams["USERS"]["ACCESS"]["LEVEL_2"]) {
            $dbUserAccount = \CSaleUserAccount::GetList(array(), array("USER_ID" => $arUser["ID"], "CURRENCY" => $currency));

            if ($arUserAccount = $dbUserAccount->Fetch()) {
                $currentBudget = floatval($arUserAccount["CURRENT_BUDGET"]);

                if ($currentBudget < 0) {

                    if ($arUser["UF_NOT_NOTIFY_ONLINE"] != 1) {
                        $arResult["CITY_CODE"] = $arParams["GEOIP"]["CITY_DEFAULT"];

                        if (strlen($arUser["WORK_CITY"]) > 0 && $arParams["GEOIP"]["IBLOCK_ID"] > 0) {
                            $el = new \CIBlockElement();
                            $arFilter = array(
                                "IBLOCK_ID" => $arParams["GEOIP"]["IBLOCK_ID"],
                                "ACTIVE" => "Y",
                                "=NAME" => $arUser["WORK_CITY"]
                            );
                            $arSelect = Array(
                                "ID",
                                "CODE",
                            );

                            $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                            while ($ob = $res->GetNextElement()) {
                                $arFields = $ob->GetFields();
                                $arResult["CITY_CODE"] = $arFields["CODE"];
                            }
                        }

                        $http = (\CMain::IsHTTPS()) ? "https://" : "http://";

                        if ($arResult["CITY_CODE"] == $arParams["GEOIP"]["CITY_DEFAULT"]) {
                            $site = $http . SITE_SERVER_NAME;
                        } else {
                            $site = $http . $arResult["CITY_CODE"] . '.' . SITE_SERVER_NAME;
                        }

                        $arMailFields = array(
                            "EVENT_NAME" => "ORGANIZATION_BALANCE_REDUCTION",
                            "LID" => SITE_ID,
                            "C_FIELDS" => array(
                                "EMAIL_TO" => (strlen($arUser["WORK_MAILBOX"]) > 0) ? $arUser["WORK_MAILBOX"] : $arUser["EMAIL"],
                                "SITE_URL" => $site

                            ),
                        );

                        \Bitrix\Main\Mail\Event::send($arMailFields);
                    }
                }
            }
        }
    }

    /**
     * @param $userId
     * @throws Main\LoaderException
     */
    public static function resetQuestPopularityRating($userId)
    {
        if (!Loader::includeModule("iblock")) {
            return;
        }

        $arOptions = \Webtu\ExpansionSite\Handler::getOptions();
        $el = new \CIBlockElement();

        $arFilter = [
            'IBLOCK_ID' => $arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
            '=PROPERTY_ORGANIZATION_ID' => $userId,
            '>PROPERTY_BOOKING_COUNT' => 0,
            '=PROPERTY_SHOW_SCHEDULE_WITH_NEGATIVE_BALANCE' => false,
        ];

        $arSelect = [
            'ID',
            'PROPERTY_BOOKING_COUNT'
        ];


        $res = $el->GetList([], $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();

            $el->SetPropertyValuesEx(
                $arFields["ID"],
                $arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
                [
                    'BOOKING_COUNT_RESET' => $arFields['PROPERTY_BOOKING_COUNT_VALUE'],
                    'BOOKING_COUNT' => 0,
                ]
            );
        }
    }

    /**
     * @param $userId
     * @throws Main\LoaderException
     */
    public static function returnQuestPopularityRating($userId)
    {
        if (!Loader::includeModule("iblock")) {
            return;
        }

        $arOptions = \Webtu\ExpansionSite\Handler::getOptions();
        $el = new \CIBlockElement();

        $arFilter = [
            'IBLOCK_ID' => $arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
            '=PROPERTY_ORGANIZATION_ID' => $userId,
            '>PROPERTY_BOOKING_COUNT_RESET' => 0,
            '=PROPERTY_SHOW_SCHEDULE_WITH_NEGATIVE_BALANCE' => false,
        ];

        $arSelect = [
            'ID',
            'PROPERTY_BOOKING_COUNT_RESET'
        ];

        $res = $el->GetList([], $arFilter, false, false, $arSelect);
        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();
            
            $el->SetPropertyValuesEx(
                $arFields["ID"],
                $arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
                [
                    'BOOKING_COUNT' => $arFields['PROPERTY_BOOKING_COUNT_RESET_VALUE'],
                    'BOOKING_COUNT_RESET' => 0,
                ]
            );
        }
    }
}