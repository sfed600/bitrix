<?
$MESS["CQL_IBLOCK_DESC_ASC"] = "По возрастанию";
$MESS["CQL_IBLOCK_DESC_DESC"] = "По убыванию";
$MESS["CQL_IBLOCK_DESC_FID"] = "ID";
$MESS["CQL_IBLOCK_DESC_FNAME"] = "Название";
$MESS["CQL_IBLOCK_DESC_FACT"] = "Дата начала активности";
$MESS["CQL_IBLOCK_DESC_FSORT"] = "Сортировка";
$MESS["CQL_IBLOCK_DESC_FTSAMP"] = "Дата последнего изменения";
$MESS["CQL_IBLOCK_DESC_LIST_TYPE"] = "Тип информационного блока (используется только для проверки)";
$MESS["CQL_IBLOCK_DESC_LIST_ID"] = "Код информационного блока";
$MESS["CQL_IBLOCK_DESC_LIST_CONT"] = "Количество квестов на странице";
$MESS["CQL_IBLOCK_DESC_IBORD1"] = "Поле для первой сортировки квестов";
$MESS["CQL_IBLOCK_DESC_IBBY1"] = "Направление для первой сортировки квестов";
$MESS["CQL_IBLOCK_DESC_IBORD2"] = "Поле для второй сортировки квестов";
$MESS["CQL_IBLOCK_DESC_IBBY2"] = "Направление для второй сортировки квестов";
$MESS["CQL_IBLOCK_FILTER"] = "Фильтр";
$MESS["CQL_QUEST_LIST"] = "ID квестов";
$MESS["CQL_IBLOCK_DESC_PAGER_NEWS"] = "Квесты";

?>