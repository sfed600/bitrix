1) Закинуть модуль в папку \local\modules\
2) Зайти в админку сайта
3) Перейти во вкладку Marketplace->Установленные решения
4) Найти модуль Модуль расширяющий сайт (webtu.expansionsite)
5) Установить модуль
6) В шаблоне сайта вставить

<head prefix="og: http://ogp.me/ns# fb: http://ogp.me/ns/fb# article: http://ogp.me/ns/article#">
    <?/** --- OpenGraph + Schema.org --- **/
    $APPLICATION->ShowViewContent('MetaTag');?>
</head>

Пример вставки в компоненте news.list
<?/** --- SCHEMA ORG + OPEN GRAPH --- **/
if(\Bitrix\Main\Loader::includeModule('webtu.expansionsite'))
{
    /**
     * @brief Инициализация MetaTag
     * @param Type = Article - Статьи детальная
     * @param Type = ListItem - Статьи список
     * @param Type = ListItemLight - Статьи список упрощённый
     * @param Type = Product - Товар детальная
     * @param Type = ProductItem - Товар список
     * @param $arResult = массив данных
     * @return Буферизированный контент
     **/
    \Webtu\ExpansionSite\MetaTag::init('ListItem',$arResult);
}?>

Пример вставки в компоненте news.detail
<?/** --- SCHEMA ORG + OPEN GRAPH --- **/
if(\Bitrix\Main\Loader::includeModule('webtu.expansionsite'))
{
    /**
     * @brief Инициализация MetaTag
     * @param Type = Article - Статьи детальная
     * @param Type = ListItem - Статьи список
     * @param Type = ListItemLight - Статьи список упрощённый
     * @param Type = Product - Товар детальная
     * @param Type = ProductItem - Товар список
     * @param $arResult = массив данных
     * @return Буферизированный контент
     **/
    \Webtu\ExpansionSite\MetaTag::init('Article',$arResult);
}?>

Пример вставки Хлебные крошки
<?/** --- SCHEMA ORG + OPEN GRAPH --- **/
if(\Bitrix\Main\Loader::includeModule('webtu.expansionsite'))
{
    /**
     * @brief Инициализация MetaTag
     * @param Type = Breadcrumb - Хлебные крошки
     * @param Type = Article - Статьи детальная
     * @param Type = ListItem - Статьи список
     * @param Type = ListItemLight - Статьи список упрощённый
     * @param Type = Product - Товар детальная
     * @param Type = ProductItem - Товар список
     * @param $arResult = массив данных
     * @return Буферизированный контент
     **/
    \Webtu\ExpansionSite\MetaTag::init('Breadcrumb', $arResult);

}

/**
     * @brief Для хлебных крошек
     * @param $arResult = массив данных
     * @return Разметка
     **/
    public function MetaTagBreadcrumb($arResult)
    {

        $serverName = SITE_SERVER_NAME;
        $protocol = (\CMain::IsHTTPS()) ? "https://" : "http://";

        $html = '';

        if (count($arResult) > 0) {

            $sub_domain = preg_replace('/^(?:([^\.]+)\.)?'.str_replace('.','\.',$_SESSION["CITY_INFO"]["domain_main"]).'$/', '\1', $_SERVER['SERVER_NAME']);

            if ( strlen($sub_domain) > 0 ) $sub_domain .= '.';

            $jsMetaTag = array(
                "@context" => "https://schema.org",
                "@type" => "BreadcrumbList",
                "itemListElement" => array()
            );

            foreach ($arResult as $key => $arItem) {

                $shemaOrgCount = $key + 1;
                $shemaOrgPage = $protocol.$sub_domain.$serverName.$arItem["LINK"];

                $jsMetaTag["itemListElement"][] = array(
                    "@type" => "ListItem",
                    "position" => $shemaOrgCount,
                    "item" => array(
                        "@id" => $shemaOrgPage,
                        "name" => $arItem["TITLE"],
                    ),
                );
            }

            $jsMetaTag = json_encode($jsMetaTag,JSON_UNESCAPED_SLASHES | JSON_UNESCAPED_UNICODE);

            $html .= '<script type="application/ld+json">'.$jsMetaTag.'</script>';
        }

        return $html;
    }

?>
Пример вставки Организации
<?/** --- SCHEMA ORG + OPEN GRAPH --- **/
if(\Bitrix\Main\Loader::includeModule('webtu.expansionsite'))
{
    $seoDescription = ( (mb_strlen($arResult["DETAIL_TEXT"]) > 0) ? TruncateText(strip_tags($arResult["~DETAIL_TEXT"]), 150) : TruncateText(strip_tags($arResult["~PREVIEW_TEXT"]), 150)  );
    $seoDescription= preg_replace("/\s{2,}/"," ",$seoDescription);
    
    $arOrganization[] = array(
        "NAME" => "Системный софт",
        "DESCRIPTION" => $seoDescription,
        "PRICE_RANGE" => "100-1000000RUB",
        "FACRBOOK" => "",
        "GEO_LATITUDE" => "55.785379",
        "GEO_LONGITUDE" => "37.584614",
        "STREET_ADDRESS" => "ул. 5-я Ямского Поля, д. 5, стр.1, БЦ «Ямское Плаза»",
        "ADDRESS_LOCALITY" => "г. Москва",
        "POSTAL_CODE" => "100000",
        "TELEPHONE" => "+7 800 333 33 71",
        "FAX_NUMBER" => "",
        "EMAIL" => "info@syssoft.ru",
    );

    /**
     * @brief Инициализация MetaTag
     * @param Type = Article - Статьи детальная
     * @param Type = ListItem - Статьи список
     * @param Type = ListItemLight - Статьи список упрощённый
     * @param Type = Product - Товар детальная
     * @param Type = ProductItem - Товар список
     * @param Type = Organization - Организация
     * @param $arResult = массив данных
     * @return Буферизированный контент
     **/
    \Webtu\SystemSoft\MetaTag::init('Organization',$arOrganization);
}?>