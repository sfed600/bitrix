<?php

namespace Webtu\ExpansionSite;

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main;
use \Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Webtu\ExpansionSite\Handler;

Loc::loadMessages(__FILE__);

class Rating
{

    private $arResult = array();

    protected $arOptions = array();

    public function __construct(){}

    /**
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock'))
        {
            throw new Main\LoaderException( Loc::getMessage('RATING_NOT_INSTALLED_MODULE', Array ("#ID#" => "iblock")) );
        }

        if (!Loader::includeModule('im'))
        {
            throw new Main\LoaderException( Loc::getMessage('RATING_NOT_INSTALLED_MODULE', Array ("#ID#" => "im")) );
        }

        if (!Loader::includeModule('webtu.expansionsite'))
        {
            throw new Main\LoaderException(Loc::getMessage('RATING_NOT_INSTALLED_MODULE', Array ("#ID#" => "webtu.expansionsite")) );
        }
    }

    /**
     * @param array $arRaitingParams
     * @param int $quest_id
     * @return bool
     * @throws Main\LoaderException
     */
    public function updateQuestRaiting( array $arRaitingParams, int $quest_id )
    {
        $this->checkModules();

        $el = new \CIBlockElement();

        $this->arResult["QUEST_ID"] = $quest_id;
        $this->arResult["RAITING_PARAMS"] = $arRaitingParams;



        $this->arOptions = Handler::getOptions();

        if(
            $this->arResult["QUEST_ID"] <= 0 and
            count($this->arResult["RAITING_PARAMS"]) == 0 and
            $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"] <= 0
        )
        {
            return false;
        }

        $this->arResult["QUEST"]  = $this->getQuestInfo();

        if ( $this->arResult["QUEST"]["ID"] == $this->arResult["QUEST_ID"])
        {
            $this->getPropertyEnum();

            #Обновляем рейтинг народный/экспертов
            foreach ($this->arResult["RAITING_PARAMS"] as $prop_code => $prop_value)
            {
                $new_value = $this->arResult["QUEST"][$prop_code] + $prop_value;

                if ( $new_value < 0 ) $new_value = 0;

                if ($new_value >= 0)
                {
                    $el->SetPropertyValuesEx($this->arResult["QUEST"]["ID"], $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"], array($prop_code => $new_value));

                    $this->arResult["QUEST"][$prop_code] = $new_value;
                }
            }

            #Считаем общий народный рейтинг
            if ( $this->arResult["QUEST"]["OTZYV_CNT"] > 0 )
            {
                $this->arResult["QUEST"]["TOTAL_FOLK"] =  (
                    $this->arResult["QUEST"]["ENTOURAG_FOLK"] /  $this->arResult["QUEST"]["OTZYV_CNT"] +
                    $this->arResult["QUEST"]["LOGIC_FOLK"] /  $this->arResult["QUEST"]["OTZYV_CNT"] +
                    $this->arResult["QUEST"]["PLOT_FOLK"] /  $this->arResult["QUEST"]["OTZYV_CNT"] +
                    $this->arResult["QUEST"]["TEAMWORK_FOLK"] /  $this->arResult["QUEST"]["OTZYV_CNT"] +
                    $this->arResult["QUEST"]["STAFF_FOLK"] /  $this->arResult["QUEST"]["OTZYV_CNT"]
                ) / 5;
            }
            else $this->arResult["QUEST"]["TOTAL_FOLK"] = 0;



            #Считаем общий рейтинг экспертов
            if ( $this->arResult["QUEST"]["RETSENZIYA_CNT"] > 0 )
            {
                $this->arResult["QUEST"]["TOTAL_EXPERT"] =  (
                    $this->arResult["QUEST"]["ENTOURAG_EXPERT"]  /  $this->arResult["QUEST"]["RETSENZIYA_CNT"] +
                    $this->arResult["QUEST"]["LOGIC_EXPERT"]  /  $this->arResult["QUEST"]["RETSENZIYA_CNT"] +
                    $this->arResult["QUEST"]["PLOT_EXPERT"]  /  $this->arResult["QUEST"]["RETSENZIYA_CNT"] +
                    $this->arResult["QUEST"]["TEAMWORK_EXPERT"]  /  $this->arResult["QUEST"]["RETSENZIYA_CNT"] +
                    $this->arResult["QUEST"]["STAFF_EXPERT"]  /  $this->arResult["QUEST"]["RETSENZIYA_CNT"]
                ) / 5;
            }
            else $this->arResult["QUEST"]["TOTAL_EXPERT"] = 0;

            if ( $this->arResult["QUEST"]["TOTAL_FOLK"] >= 0 )
            {
                if ( $this->arResult["QUEST"]["TOTAL_FOLK"] > 0 )
                {
                    $this->arResult["QUEST"]["TOTAL_FOLK"] = round($this->arResult["QUEST"]["TOTAL_FOLK"], 1, PHP_ROUND_HALF_UP);
                }

                $el->SetPropertyValuesEx($this->arResult["QUEST"]["ID"], $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"], array("TOTAL_FOLK" => $this->arResult["QUEST"]["TOTAL_FOLK"]));
            }

            if ( $this->arResult["QUEST"]["TOTAL_EXPERT"] >= 0 )
            {
                if ( $this->arResult["QUEST"]["TOTAL_EXPERT"] > 0 )
                {
                    $this->arResult["QUEST"]["TOTAL_EXPERT"] = round($this->arResult["QUEST"]["TOTAL_EXPERT"], 1, PHP_ROUND_HALF_UP);
                }


                $el->SetPropertyValuesEx($this->arResult["QUEST"]["ID"], $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"], array("TOTAL_EXPERT" => $this->arResult["QUEST"]["TOTAL_EXPERT"]));
            }

            if (
                $this->arResult["QUEST"]["TOTAL_FOLK"] == 0
                || $this->arResult["QUEST"]["TOTAL_EXPERT"] == 0
            )
            {
                $denominator = 1;
            }
            else
            {
                $denominator = 2;
            }

            $this->arResult["QUEST"]["TOTAL_RAITING"] = $this->arResult["QUEST"]["TOTAL_FOLK"] + $this->arResult["QUEST"]["TOTAL_EXPERT"];

            if ( $this->arResult["QUEST"]["TOTAL_RAITING"] > 0 )
            {
                $this->arResult["QUEST"]["TOTAL_RAITING"] = round(( $this->arResult["QUEST"]["TOTAL_RAITING"]) / $denominator, 1, PHP_ROUND_HALF_UP);
            }
            else
            {
                $this->arResult["QUEST"]["TOTAL_RAITING"] = 0;
            }


            if ( $this->arResult["QUEST"]["TOTAL_RAITING"] >= 0 )
            {

                $el->SetPropertyValuesEx($this->arResult["QUEST"]["ID"], $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"], array("RAITING" => $this->arResult["QUEST"]["TOTAL_RAITING"]));

                $raitingFilter = false;

                foreach ($this->arResult["PROPS"]["RAITING_FILTER"] as $key => $arProp)
                {
                    if ( $key <= $this->arResult["QUEST"]["TOTAL_RAITING"] )
                    {
                        $raitingFilter[] = $arProp["ID"];
                    }
                }

                $el->SetPropertyValuesEx($this->arResult["QUEST"]["ID"], $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"], array("RAITING_FILTER" => $raitingFilter));

            }

            \Bitrix\Iblock\PropertyIndex\Manager::updateElementIndex($this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"], $this->arResult["QUEST"]["ID"]);

            return true;
        }
    }

    /**
     * @param int $quest_id
     * @return array
     */
    protected function getQuestInfo($quest_id = 0)
    {
        $arQuest = array();
        $el = new \CIBlockElement();

        if ($quest_id > 0)
        {
            $this->arResult["QUEST_ID"] = $quest_id;
        }

        #Получаем информацию по квесту
        $arFilter = Array(
            "IBLOCK_ID" => $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
            "ID" => $this->arResult["QUEST_ID"],
            "ACTIVE" => "Y",
        );

        $arSelect = Array(
            "ID",
            "NAME",
            "DETAIL_PAGE_URL",
            "PROPERTY_ENTOURAG_FOLK",
            "PROPERTY_LOGIC_FOLK",
            "PROPERTY_PLOT_FOLK",
            "PROPERTY_TEAMWORK_FOLK",
            "PROPERTY_STAFF_FOLK",
            "PROPERTY_SCORE_FOLK",
            "PROPERTY_ENTOURAG_EXPERT",
            "PROPERTY_LOGIC_EXPERT",
            "PROPERTY_PLOT_EXPERT",
            "PROPERTY_TEAMWORK_EXPERT",
            "PROPERTY_STAFF_EXPERT",
            "PROPERTY_SCORE_EXPERT",
            "PROPERTY_OTZYV_CNT",
            "PROPERTY_RETSENZIYA_CNT",
        );

        $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();

            $arQuest = array(
                "ID" => $arFields["ID"],
                "NAME" => $arFields["NAME"],
                "DETAIL_PAGE_URL" => $arFields["DETAIL_PAGE_URL"],
                "ENTOURAG_FOLK" =>  (int) $arFields["PROPERTY_ENTOURAG_FOLK_VALUE"],
                "LOGIC_FOLK" =>  (int) $arFields["PROPERTY_LOGIC_FOLK_VALUE"],
                "PLOT_FOLK" =>  (int) $arFields["PROPERTY_PLOT_FOLK_VALUE"],
                "TEAMWORK_FOLK" =>  (int) $arFields["PROPERTY_TEAMWORK_FOLK_VALUE"],
                "STAFF_FOLK" =>  (int) $arFields["PROPERTY_STAFF_FOLK_VALUE"],
                "SCORE_FOLK" =>  (float) $arFields["PROPERTY_SCORE_FOLK_VALUE"],
                "ENTOURAG_EXPERT" =>  (int) $arFields["PROPERTY_ENTOURAG_EXPERT_VALUE"],
                "LOGIC_EXPERT" =>  (int) $arFields["PROPERTY_LOGIC_EXPERT_VALUE"],
                "PLOT_EXPERT" =>  (int) $arFields["PROPERTY_PLOT_EXPERT_VALUE"],
                "TEAMWORK_EXPERT" =>  (int) $arFields["PROPERTY_TEAMWORK_EXPERT_VALUE"],
                "STAFF_EXPERT" =>  (int) $arFields["PROPERTY_STAFF_EXPERT_VALUE"],
                "SCORE_EXPERT" =>   (float) $arFields["PROPERTY_SCORE_EXPERT_VALUE"],
                "OTZYV_CNT" =>  (int) $arFields["PROPERTY_OTZYV_CNT_VALUE"],
                "RETSENZIYA_CNT" =>  (int) $arFields["PROPERTY_RETSENZIYA_CNT_VALUE"],
            );
        }

        return $arQuest;
    }


    protected function getPropertyEnum()
    {
        $arPropsEnum = array("RAITING_FILTER");

        foreach ( $arPropsEnum as $prop_code )
        {
            $property_enums = \CIBlockPropertyEnum::GetList(Array("DEF"=>"DESC", "SORT"=>"ASC"), Array("IBLOCK_ID" => $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"], "CODE" => $prop_code));

            while($enum_fields = $property_enums->GetNext())
            {
                $this->arResult["PROPS"][$prop_code][$enum_fields["XML_ID"]] = array(
                    "ID"    => $enum_fields["ID"],
                    "VALUE" => $enum_fields["VALUE"]
                );
            }
        }
    }

    /**
     * @param $element_id
     * @param $iblock_id
     * @return array|bool
     */
    protected function getReviewInfo($element_id, $iblock_id)
    {
        $el = new \CIBlockElement();

        $arUpdatePropRating = array();

        if (
            $element_id <= 0
            and $iblock_id <= 0
        )
        {
            return false;
        }

        $arFilter = Array(
            "IBLOCK_ID" => $iblock_id,
            "ID" => $element_id,
        );

        $arSelect = Array(
            "ACTIVE",
            "PROPERTY_ENTOURAGE",
            "PROPERTY_LOGIC",
            "PROPERTY_PLOT",
            "PROPERTY_TEAMWORK",
            "PROPERTY_STAFF",
            "PROPERTY_SCORE",
            "PROPERTY_QUEST_ID",
            "PROPERTY_USER_ID",
            "PROPERTY_ORGANIZATION_ID",
            "PROPERTY_ORDER_ID"
        );

        $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

        while($ob = $res->GetNextElement())
        {
            $arFields = $ob->GetFields();

            $arUpdatePropRating = array(
                "ACTIVE" => $arFields["ACTIVE"],
                "ENTOURAGE" => (int) $arFields["PROPERTY_ENTOURAGE_VALUE"],
                "LOGIC" => (int) $arFields["PROPERTY_LOGIC_VALUE"],
                "PLOT" => (int) $arFields["PROPERTY_PLOT_VALUE"],
                "TEAMWORK" => (int) $arFields["PROPERTY_TEAMWORK_VALUE"],
                "STAFF" => (int) $arFields["PROPERTY_STAFF_VALUE"],
                "QUEST_ID" => (int) $arFields["PROPERTY_QUEST_ID_VALUE"],
                "USER_ID" => (int) $arFields["PROPERTY_USER_ID_VALUE"],
                "ORGANIZATION_ID" => (int) $arFields["PROPERTY_ORGANIZATION_ID_VALUE"],
                "ORDER_ID" => (int) $arFields["PROPERTY_ORDER_ID_VALUE"],
            );
        }

        $SCORE = (
                $arUpdatePropRating["ENTOURAGE"] +
                $arUpdatePropRating["LOGIC"] +
                $arUpdatePropRating["PLOT"] +
                $arUpdatePropRating["TEAMWORK"] +
                $arUpdatePropRating["STAFF"]
            ) / 5;

        $arUpdatePropRating["SCORE"] = round( $SCORE, 1, PHP_ROUND_HALF_UP);

        return $arUpdatePropRating;
    }

    /**
     * @param $arFields
     * @throws Main\LoaderException
     */
    public function adminRatingReviewAdd(&$arFields)
    {
        global $USER;

        $el = new \CIBlockElement();

        $arOptions = Handler::getOptions();

        $tag = '';

        if ( $arFields["IBLOCK_ID"] == $arOptions["REVIEW"]["IBLOCK_ID"]  )
        {
            $tag = 'FOLK';

            $arUpdatePropRating["OTZYV_CNT"] = 1;
        }
        else if ( $arFields["IBLOCK_ID"] == $arOptions["CRITIQUE"]["IBLOCK_ID"] )
        {
            $tag = 'EXPERT';

            $arUpdatePropRating["RETSENZIYA_CNT"] = 1;
        }

        if (
            $arFields["RESULT"] > 0
            and strlen($tag) > 0
            and $USER->IsAdmin()
            and $arFields["ID"] > 0
        )
        {
            $arRating = self::getReviewInfo($arFields["ID"], $arFields["IBLOCK_ID"]);

            $el->SetPropertyValuesEx($arFields["ID"], $arFields["IBLOCK_ID"], array("SCORE" => $arRating["SCORE"]));

            $arUpdatePropRating["ENTOURAG_".$tag] = $arRating["ENTOURAGE"];
            $arUpdatePropRating["LOGIC_".$tag] = $arRating["LOGIC"];
            $arUpdatePropRating["PLOT_".$tag] = $arRating["PLOT"];
            $arUpdatePropRating["TEAMWORK_".$tag] = $arRating["TEAMWORK"];
            $arUpdatePropRating["STAFF_".$tag] = $arRating["STAFF"];
            $arUpdatePropRating["SCORE_".$tag] = $arRating["SCORE"];

            $raiting = new \Webtu\ExpansionSite\Rating();

            $raiting->updateQuestRaiting($arUpdatePropRating, $arRating["QUEST_ID"]);

        }

    }

    /**
     * @param $arFields
     * @throws Main\LoaderException
     */
    public function adminRatingReviewUpdateBefore(&$arFields)
    {
        global $USER;

        $el = new \CIBlockElement();

        $arOptions = Handler::getOptions();

        $tag = '';

        if ( $arFields["IBLOCK_ID"] == $arOptions["REVIEW"]["IBLOCK_ID"]  )
        {
            $tag = 'FOLK';

            $arUpdatePropRating["OTZYV_CNT"] = -1;
        }
        else if ( $arFields["IBLOCK_ID"] == $arOptions["CRITIQUE"]["IBLOCK_ID"] )
        {
            $tag = 'EXPERT';

            $arUpdatePropRating["RETSENZIYA_CNT"] = -1;
        }

        if (
            strlen($tag) > 0
            and $USER->IsAdmin()
            and $arFields["ID"] > 0
        )
        {
            $arRating = self::getReviewInfo($arFields["ID"], $arFields["IBLOCK_ID"]);

            $arUpdatePropRating["ENTOURAG_".$tag] = 0 - $arRating["ENTOURAGE"];
            $arUpdatePropRating["LOGIC_".$tag] = 0 - $arRating["LOGIC"];
            $arUpdatePropRating["PLOT_".$tag] = 0 - $arRating["PLOT"];
            $arUpdatePropRating["TEAMWORK_".$tag] = 0 - $arRating["TEAMWORK"];
            $arUpdatePropRating["STAFF_".$tag] = 0 - $arRating["STAFF"];
            $arUpdatePropRating["SCORE_".$tag] = 0 - $arRating["SCORE"];

            if ( $arRating["ACTIVE"] == "Y" )
            {
                $raiting = new \Webtu\ExpansionSite\Rating();

                $raiting->updateQuestRaiting($arUpdatePropRating, $arRating["QUEST_ID"]);
            }


        }
    }

    /**
     * @param $arFields
     * @throws Main\LoaderException
     */
    public function adminRatingReviewUpdateAfter(&$arFields)
    {
        global $USER;

        $el = new \CIBlockElement();

        $arOptions = Handler::getOptions();

        $tag = '';

        $arQuestComplete = array();

        if ( $arFields["IBLOCK_ID"] == $arOptions["REVIEW"]["IBLOCK_ID"]  )
        {
            $tag = 'FOLK';

            $arUpdatePropRating["OTZYV_CNT"] = 1;
        }
        else if ( $arFields["IBLOCK_ID"] == $arOptions["CRITIQUE"]["IBLOCK_ID"] )
        {
            $tag = 'EXPERT';

            $arUpdatePropRating["RETSENZIYA_CNT"] = 1;
        }

        if (
            strlen($tag) > 0
            and $USER->IsAdmin()
            and $arFields["ID"] > 0
            and $arFields["ACTIVE"] == "Y"
        )
        {
            $arRating = self::getReviewInfo($arFields["ID"], $arFields["IBLOCK_ID"]);

            $el->SetPropertyValuesEx($arFields["ID"], $arFields["IBLOCK_ID"], array("SCORE" => $arRating["SCORE"]));

            $arUpdatePropRating["ENTOURAG_".$tag] = $arRating["ENTOURAGE"];
            $arUpdatePropRating["LOGIC_".$tag] = $arRating["LOGIC"];
            $arUpdatePropRating["PLOT_".$tag] = $arRating["PLOT"];
            $arUpdatePropRating["TEAMWORK_".$tag] = $arRating["TEAMWORK"];
            $arUpdatePropRating["STAFF_".$tag] = $arRating["STAFF"];
            $arUpdatePropRating["SCORE_".$tag] = $arRating["SCORE"];

            $raiting = new \Webtu\ExpansionSite\Rating();

            $res = $raiting->updateQuestRaiting($arUpdatePropRating, $arRating["QUEST_ID"]);

            if ($res)
            {
                $arQuest = $raiting->getQuestInfo($arRating["QUEST_ID"]);

                #Начисление бонусов за оставленный отзыв/рецензию
                if (
                    $arOptions["BONUS"]["EVENT_REVIEW_ADD"] == "Y"
                    && intval($arOptions["BONUS"]["EVENT_REVIEW_ADD_SUM"]) > 0
                    && $arRating["ORDER_ID"] > 0
                    && $arRating["USER_ID"] > 0
                    && $arOptions["ORDER"]["QUEST_COMPLETED_IBLOCK_ID"] > 0
                )
                {
                    #Проверяем начислены ли бонусы за отзыв к этому заказу
                    $arFilter = Array(
                        "IBLOCK_ID" => $arOptions["ORDER"]["QUEST_COMPLETED_IBLOCK_ID"],
                        "ACTIVE" => "Y",
                        "PROPERTY_ORDER_ID" => $arRating["ORDER_ID"],
                        "PROPERTY_USER_ID"  => $arRating["USER_ID"],
                    );

                    $arSelect = Array(
                        "ID",
                        "PROPERTY_BONUS_ADD",
                    );

                    $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                    while($ob = $res->GetNextElement())
                    {
                        $arFieldsComplete = $ob->GetFields();

                        $arQuestComplete = array(
                            "ID" => $arFieldsComplete["ID"],
                            "BONUS_ADD" => ( $arFieldsComplete["PROPERTY_BONUS_ADD_ENUM_ID"] ==  143 ) ? "Y" : "N"
                        );
                    }

                    if (
                        $arQuestComplete["ID"] > 0
                        && $arQuestComplete["BONUS_ADD"] == "N"
                    )
                    {
                        $sum = $arOptions["BONUS"]["EVENT_REVIEW_ADD_SUM"];

                        if ( $arFields["IBLOCK_ID"] == $arOptions["CRITIQUE"]["IBLOCK_ID"] )
                        {
                            $name = "оставленную рецензию №".$arFields["ID"];
                        }
                        else
                        {
                            $name = "оставленный отзыв №".$arFields["ID"];
                        }

                        $description = Loc::getMessage('RATING_BONUS_DESCRIPTION', Array("#SUMM#" => $sum, "#NAME#" => $name, "#QUEST_NAME#" => $arQuest["NAME"]));
                        $deductDescription = Loc::getMessage('RATING_BONUS_DEDUCT_DESCRIPTION', Array ("#SUMM#" => $sum,"#NAME#" => $name, "#QUEST_NAME#" => $arQuest["NAME"]));
                        $notes = serialize(array("DEDUCT"=>"N","DEDUCT_TRANSACT_ID"=>"","DEDUCT_DESCRIPTION"=>$deductDescription));

                        if (
                            $sum > 0
                            && $arQuest["ID"] > 0
                        )
                        {
                            #Зачисляем бонусы за пройденный квест в размере кэшбека от суммы бронирования
                            $result_transact = \Webtu\ExpansionSite\Bonus::userUpdateAccount(
                                $arRating["USER_ID"],
                                $sum,
                                "RUB",
                                $description,
                                $arRating["ORDER_ID"],
                                $notes
                            );

                            if ( intval($result_transact["TRANSACT_ID"]) > 0 )
                            {
                                $el->SetPropertyValuesEx($arQuestComplete["ID"], $arOptions["ORDER"]["QUEST_COMPLETED_IBLOCK_ID"], array("BONUS_ADD" => 143));
                            }
                        }

                        if ( $arFields["IBLOCK_ID"] == $arOptions["CRITIQUE"]["IBLOCK_ID"] )
                        {

                            $title_name = "Оставлена новая рецензия";
                        }
                        else
                        {
                            $title_name = "Оставлен новый отзыв";
                        }

                        if (
                            $arRating["ORGANIZATION_ID"] > 0
                            && $arQuest["ID"] > 0
                        )
                        {
                            $arMessageFieldsUser = array(
                                "TO_USER_ID" => $arRating["ORGANIZATION_ID"],
                                "FROM_USER_ID" => 0,
                                "NOTIFY_TYPE" => IM_NOTIFY_SYSTEM,
                                "NOTIFY_MODULE" => "im",
                                "NOTIFY_MESSAGE" => $title_name.' на квест <a href="'.$arQuest["DETAIL_PAGE_URL"].'">'.$arQuest["NAME"].'</a>',
                            );

                            \CIMNotify::Add($arMessageFieldsUser);
                        }
                    }
                }
            }
        }

    }

    /**
     * @param $ID
     * @throws Main\LoaderException
     */
    public function adminRatingReviewRemove($ID)
    {
        global $USER;

        $el = new \CIBlockElement();

        $arOptions = Handler::getOptions();

        if (
            Loader::IncludeModule("iblock")
            and $ID > 0
        )
        {
            $res = $el->GetByID($ID);

            if ($ar_res = $res->GetNext())
            {
                $tag = '';

                if ( $ar_res["IBLOCK_ID"] == $arOptions["REVIEW"]["IBLOCK_ID"]  )
                {
                    $tag = 'FOLK';

                    $arUpdatePropRating["OTZYV_CNT"] = -1;
                }
                else if ( $ar_res["IBLOCK_ID"] == $arOptions["CRITIQUE"]["IBLOCK_ID"] )
                {
                    $tag = 'EXPERT';

                    $arUpdatePropRating["RETSENZIYA_CNT"] = -1;
                }

                if (
                    strlen($tag) > 0
                    and $USER->IsAdmin()
                )
                {
                    $arRating = self::getReviewInfo($ar_res["ID"], $ar_res["IBLOCK_ID"]);

                    $arUpdatePropRating["ENTOURAG_".$tag] = 0 - $arRating["ENTOURAGE"];
                    $arUpdatePropRating["LOGIC_".$tag] = 0 - $arRating["LOGIC"];
                    $arUpdatePropRating["PLOT_".$tag] = 0 - $arRating["PLOT"];
                    $arUpdatePropRating["TEAMWORK_".$tag] = 0 - $arRating["TEAMWORK"];
                    $arUpdatePropRating["STAFF_".$tag] = 0 - $arRating["STAFF"];
                    $arUpdatePropRating["SCORE_".$tag] = 0 - $arRating["SCORE"];

                    $raiting = new \Webtu\ExpansionSite\Rating();

                    $raiting->updateQuestRaiting($arUpdatePropRating, $arRating["QUEST_ID"]);

                }
            }
        }
    }
}