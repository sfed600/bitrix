<?php
namespace Webtu\ExpansionSite\GeoIp;

use \Bitrix\Main;
use \Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Main\Text\Encoding;
use \Webtu\ExpansionSite\Handler;

Loc::loadMessages(__FILE__);

class GeoIpBase
{
    private static $fhandleCIDR, $fhandleCities, $fSizeCIDR, $fsizeCities;
    
    private static $arResult = array("MESSAGE" => array("ERROR"=>array(),"OK"=>array()));

    /**
     * @brief Получение гео-информации по IP
     * @param $nearestDotCode - код ближайшей точки ()фиксированая)
     * @return массив или false, если не найдено
     **/
    public function getResult($nearestDotCode = "")
    {
        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        if( 
            !$arParams["GEOIP"]["DATA_SOURCE_IP"] and 
            !$arParams["GEOIP"]["NEAREST_DOT"] and 
            !$arParams["GEOIP"]["IBLOCK_ID"] and 
            !$arParams["GEOIP"]["PROPERTY_MAP_CODE"]
        )
        { 
            return false; 
        }

        self::$arResult["PATH_ROOT"] = dirname(__FILE__);

        #Определим ip
        $ip = ( (!empty($_SERVER["HTTP_X_REAL_IP"])) ? $_SERVER["HTTP_X_REAL_IP"] : $_SERVER["REMOTE_ADDR"]  );
        
        if($arParams["GEOIP"]["DATA_SOURCE_IP"] == "LOCAL")
        {
            self::$arResult["ITEM"] = self::getDataLocal($ip);
        }
        else
        {
            self::$arResult["ITEM"] = self::getDataCurl($ip);
        }

        if ($arParams["GEOIP"]["CITY_DEFAULT"] && !$nearestDotCode) {
            self::$arResult["NEAREST_DOT"] = self::getElement($arParams["GEOIP"]["CITY_DEFAULT"]);
        }
        else {
            #Вычисляем Ближайшую точку
            if($nearestDotCode)
            {
                self::$arResult["NEAREST_DOT"] = self::getElement($nearestDotCode);
            }
            else
            {
                if(
                    $arParams["GEOIP"]["NEAREST_DOT"] === "Y" and
                    intval($arParams["GEOIP"]["IBLOCK_ID"]) > 0 and
                    mb_strlen($arParams["GEOIP"]["PROPERTY_MAP_CODE"])> 0
                )
                {
                    self::$arResult["NEAREST_DOT"] = self::getNearestDot(self::$arResult["ITEM"],$arParams["GEOIP"]);
                }
            }
        }



        return self::$arResult;
    }
    
	////////////////////////
	// LOCAL functions
	////////////////////////
    protected function getDataLocal($ip)
    {
        if(!$ip)
        { 
            return false; 
        }

        #Определение путей к файлам
        if(!self::getDataPath())
        { 
            return false; 
        }

        #Получение гео-информации по IP
        $result = self::getRecord($ip);
        
        if (is_array($result)) {
            foreach ($result as $key => $value) {
                $result[$key] = Encoding::convertEncoding($value, 'windows-1251', SITE_CHARSET);
            }
        }

        return $result;
    }

    /**
     * @brief Определение путей к файлам
     * @return возарваем false в случае ошибки
     **/
    protected function getDataPath()
    {
        $CIDRFile = self::$arResult["PATH_ROOT"].'/data/cidr_optim.txt';
        if( file_exists($CIDRFile) )
        {
            self::$fhandleCIDR = fopen($CIDRFile, 'r');
            self::$fSizeCIDR = filesize($CIDRFile);
        }
        else
        {
            array_push(self::$arResult["MESSAGE"]["ERROR"], Loc::getMessage('GEOIP_ERROR_FILE_EXISTS') );

            return false;
        }

        $CitiesFile = self::$arResult["PATH_ROOT"].'/data/cities.txt';
        if( file_exists($CitiesFile) )
        {
            self::$fhandleCities = fopen($CitiesFile, 'r');
            self::$fsizeCities = filesize($CitiesFile);
        }
        else
        {
            array_push(self::$arResult["MESSAGE"]["ERROR"], Loc::getMessage('GEOIP_ERROR_FILE_EXISTS') );
            
            return false;
        }
        
        return true;
    }

    /**
     * @brief Получение гео-информации по IP
     * @param ip IPv4-адрес
     * @return массив или false, если не найдено
     **/
    protected function getRecord($ip)
    {
        if(!$ip){ return false; }
        
        $ip = sprintf('%u', ip2long($ip));

        rewind(self::$fhandleCIDR);
        $rad = floor(self::$fSizeCIDR / 2);
        $pos = $rad;
        while(fseek(self::$fhandleCIDR, $pos, SEEK_SET) != -1)
        {
            if($rad)
            {
                $str = fgets(self::$fhandleCIDR);
            }
            else
            {
                rewind(self::$fhandleCIDR);
            }

            $str = fgets(self::$fhandleCIDR);

            if(!$str)
            {
                return false;
            }

            $arRecord = explode("\t", trim($str));

            $rad = floor($rad / 2);
            if(!$rad && ($ip < $arRecord[0] || $ip > $arRecord[1]))
            {
                return false;
            }

            if($ip < $arRecord[0])
            {
                $pos -= $rad;
            }
            elseif($ip > $arRecord[1])
            {
                $pos += $rad;
            }
            else
            {
                $result = array('inetnum' => $arRecord[2], 'country' => $arRecord[3]);

                if($arRecord[4] != '-' && $cityResult = self::getCityByIdx($arRecord[4]))
                {
                    $result += $cityResult;
                }

                return $result;
            }
        }
        return false;
    }

    /**
     * @brief Получение информации о городе по индексу
     * @param idx индекс города
     * @return массив или false, если не найдено
     **/
    protected function getCityByIdx($idx)
    {
        rewind(self::$fhandleCities);
        while(!feof(self::$fhandleCities))
        {
            $str = fgets(self::$fhandleCities);
            $arRecord = explode("\t", trim($str));
            if($arRecord[0] == $idx)
            {
                return array(	
                    'city' => $arRecord[1],
                    'region' => $arRecord[2],
                    'district' => $arRecord[3],
                    'lat' => $arRecord[4],
                    'lng' => $arRecord[5]);
            }
        }
        
        return false;
    }

	////////////////////////
	// CURL functions
	////////////////////////
    protected function getDataCurl($ip)
    {
        if(!$ip)
        { 
            return false; 
        }
 
		if(!function_exists('curl_init'))
		{
            array_push(self::$arResult["MESSAGE"]["ERROR"], Loc::getMessage('GEOIP_ERROR_CURL_NONE_INSTALLER') );
			return false;;
		}
        
        $arData = self::GetGeoData($ip);

        return $arData;
    }

	protected function GetGeoData($ip)
	{
		if(self::InitBots())
        {
		   return false;
		}

		if(!$arData = self::GetGeoDataIpgeobase_ru($ip))
		{
            array_push(self::$arResult["MESSAGE"]["ERROR"], Loc::getMessage('GEOIP_ERROR_NONE_DATA_IPGEOBASE') );
		}
        
		return $arData;	
	}
    
	protected function InitBots()
	{
		$bots = array(
			'rambler', 'googlebot', 'ia_archiver', 'Wget', 'WebAlta', 'MJ12bot', 'aport', 'yahoo', 'msnbot', 'mail.ru', 
			'alexa.com', 'Baiduspider', 'Speedy Spider', 'abot', 'Indy Library'
		);

		foreach($bots as $bot)
		{
			if(stripos($_SERVER['HTTP_USER_AGENT'], $bot) !== false)
			{
				return $bot;
			}
		}
        
		return false;
	}

	protected function GetGeoDataIpgeobase_ru($ip)
	{
        $url = "http://ipgeobase.ru:7020/geo/?ip=".$ip;
       
		if(empty($ip))
        {  
            return;
        }

		if(!function_exists('curl_init'))
		{
			if(!$text = file_get_contents($url))
            {
                return false;
            }
		}
		else
		{
            $handle = curl_init();
            curl_setopt($handle, CURLOPT_URL, $url);
            curl_setopt($handle, CURLOPT_HEADER, TRUE); 
            curl_setopt($handle, CURLOPT_SSL_VERIFYPEER, false);
            curl_setopt($handle, CURLOPT_SSL_VERIFYHOST, false);
            curl_setopt($handle, CURLOPT_RETURNTRANSFER, true);
            $resData = curl_exec($handle);
            $codeData = curl_getinfo($handle, CURLINFO_HTTP_CODE);
            $errno = curl_errno($handle);
            curl_close($handle);

            if($codeData == 200)
            {
                $resData = Encoding::convertEncoding($resData, 'windows-1251', SITE_CHARSET);

                $arData = self::ParseXML($resData);
            }
            else
            {
                array_push(self::$arResult["MESSAGE"]["ERROR"], Loc::getMessage('GEOIP_ERROR_CODE',array("#CODE#"=>$codeData)) );
                return false;
            }
		}
        
        return $arData;
	}

	protected function ParseXML($text)
	{
		if(strlen($text) > 0)
		{
			require_once($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/classes/general/xml.php");
			$objXML = new \CDataXML();
			$res = $objXML->LoadString($text);
			if($res !== false)
			{
				$arRes = $objXML->GetArray();
			}
		}

		$arRes = current($arRes);
		$arRes = $arRes["#"];
		$arRes = current($arRes);

		$ar = Array();

		foreach($arRes as $key => $arVal)
		{
			foreach($arVal["#"] as $title => $Tval)
			{
				$ar[$key][$title] = $Tval["0"]["#"];
			}
		}
		return ($ar[0]);
	}
	////////////////////////
	// END CURL functions
	////////////////////////

    /**
     * @brief Вычисляем Ближайшую точку
     * @param $item - ГЕО данные
     * @param $params - параметры
     * @return массив данных
     **/
    protected function getNearestDot($item,$params)
    {
        if(!Loader::IncludeModule("iblock")){ return; }
        
        $arResult = array();
        $matResult = array();

        #Формула вычисления  длины от точки А до точки Б
        #$mat_result = sqrt(pow((x2-x1),2)+pow((y2-y1),2));
        #lat
        //$coordinatX = '54.181381';
        #lng
        //$coordinatY = '45.175690';   

        $coordinatX = $item["lat"];
        $coordinatY = $item["lng"]; 

        $array_list = \CIBlockElement::GetList(
            array("ID"=>"ASC"), 
            array("IBLOCK_ID"=>$params["IBLOCK_ID"]), 
            false, 
            false,
            array("IBLOCK_ID","ID","NAME","CODE") 
        );
    	while($ob = $array_list->GetNextElement())
        {
    		$arFields = $ob->GetFields();
            $arProps = $ob->GetProperties(array("SORT" => "ASC"),array("ID" => $params["PROPERTY_MAP_CODE"], "CODE" => "PHONE_CITY"));
            $coordinatMap = explode(",", $arProps[$params["PROPERTY_MAP_CODE"]]["VALUE"]);
            $coordinatMap = array_diff($coordinatMap, array('', NULL, false));

            if( mb_strlen($arProps[$params["PROPERTY_MAP_CODE"]]["VALUE"]) > 0 and count($coordinatMap) == 2 )
            {
                $matResult["ITEM"][$arFields["ID"]] = $arFields;
                $matResult["ITEM"][$arFields["ID"]]["PHONE"] = $arProps["PHONE_CITY"]["VALUE"];
                
                //Формула вычисления длины от точки А до точки Б
                $matResult["DOT"][$arFields["ID"]] = sqrt(pow(((int)$coordinatMap[0]-(int)$coordinatX),2)+pow(((int)$coordinatMap[1]-(int)$coordinatY),2));
            }
    	}

        //Вычисляем минимальное значение тоесть ближайшую точку к нам
        $dotMinIndex = array_search(min($matResult["DOT"]),$matResult["DOT"]);
        $arResult = $matResult["ITEM"][$dotMinIndex];   

        return $arResult;
    }


    /**
     * @brief Вычисляем Ближайшую точку
     * @param $code - код элемента
     * @return массив данных
     **/

    static public function getElement($code)
    {
        if(!Loader::IncludeModule("iblock")){ return; }

		if(empty($code))
        {  
            return;
        }
        
        #Получить все настройки модуля
        $arParams = Handler::getOptions();

        $array_list = \CIBlockElement::GetList(
            array("ID"=>"ASC"), 
            array("IBLOCK_ID"=>$arParams["GEOIP"]["IBLOCK_ID"],"=CODE"=>$code), 
            false, 
            false,
            array("IBLOCK_ID","ID","NAME","CODE") 
        );
    	while($ob = $array_list->GetNextElement())
        {
    		$arResult = $ob->GetFields();
    		$arProps = $ob->GetProperties();

    		$arResult["PHONE"] = $arProps["PHONE_CITY"]["VALUE"];
            $arResult["SOC_LINKS"] = array(
                "vk" => $arProps["VK_LINK"]["VALUE"],
                "fb" => $arProps["FACEBOOK_LINK"]["VALUE"],
                "tw" => $arProps["TWITTER_LINK"]["VALUE"],
                "yt" => $arProps["YOUTUBE_LINK"]["VALUE"],
                "in" => $arProps["INSTAGRAM_LINK"]["VALUE"],
                "tg" => $arProps["TELEGRAM_LINK"]["VALUE"],
            );
    	}

        return $arResult;
    }
    
};