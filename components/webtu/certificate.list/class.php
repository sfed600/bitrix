<?php
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();

/** @var CBitrixComponent $this */
/** @var array $arParams */
/** @var array $arResult */
/** @var string $componentPath */
/** @var string $componentName */
/** @var string $componentTemplate */
/** @global CDatabase $DB */
/** @global CUser $USER */
/** @global CMain $APPLICATION */

/** @global CIntranetToolbar $INTRANET_TOOLBAR */
global $INTRANET_TOOLBAR;

use \Bitrix\Main;
use \Bitrix\Main\Application;
use \Bitrix\Main\Context;
use \Bitrix\Main\DateTime;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Bitrix\Iblock;
use \Webtu\ExpansionSite\Handler;

class CertificateList extends CBitrixComponent
{
    public $MessageError = array();
    public $MessageSend = array();
    public $arrFilter = array();

    protected $arOptions = array();

    /**
     * @brief Перезаписать параметры
     * @param $arParams
     * @return array
     */
    public function onPrepareComponentParams($arParams)
    {
        $arParams["IBLOCK_ID"] = IntVal($arParams["IBLOCK_ID"]);
        $arParams["IBLOCK_TYPE"] = trim($arParams["IBLOCK_TYPE"]);

        $result = array(
            "IBLOCK_ID" => $arParams['IBLOCK_ID'] ? $arParams['IBLOCK_ID'] : '',
            "IBLOCK_TYPE" => $arParams['IBLOCK_TYPE'] ? $arParams['IBLOCK_TYPE'] : '',
            "AJAX_MODE" => $arParams['AJAX_MODE'] ? $arParams['AJAX_MODE'] : 'N',
            "AJAX_OPTION_ADDITIONAL" => $arParams['AJAX_OPTION_ADDITIONAL'] ? $arParams['AJAX_OPTION_ADDITIONAL'] : '',
            "AJAX_OPTION_HISTORY" => $arParams['AJAX_OPTION_HISTORY'] ? $arParams['AJAX_OPTION_HISTORY'] : 'N',
            "AJAX_OPTION_JUMP" => $arParams['AJAX_OPTION_JUMP'] ? $arParams['AJAX_OPTION_JUMP'] : 'N',
            "AJAX_OPTION_STYLE" => $arParams['AJAX_OPTION_STYLE'] ? $arParams['AJAX_OPTION_STYLE'] : 'Y',
        );

        return $result;

    }

    /**
     * @brief Проверка подключаемых модулей
     **/
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new Main\LoaderException(Loc::getMessage('CL_NOT_INSTALLED_IBLOCK', Array("#ID#" => "iblock")));
        }

        if (!Loader::includeModule('catalog')) {
            throw new Main\LoaderException(Loc::getMessage('CL_NOT_INSTALLED_IBLOCK', Array("#ID#" => "catalog")));
        }

        if (!Loader::includeModule('webtu.expansionsite')) {
            throw new Main\LoaderException(Loc::getMessage('CL_NOT_INSTALLED_IBLOCK', Array("#ID#" => "webtu.expansionsite")));
        }

    }

    /**
     * @brief Проверка обязательных параметров
     **/
    protected function checkOptions()
    {
        if (intval($this->arParams["IBLOCK_ID"]) <= 0) {
            array_push($this->MessageError, Loc::getMessage('CL_NONE_OPTIONS', Array("#ID#" => "IBLOCK_ID")));
        } else {
            $rsIBlock = CIBlock::GetList(array(), array(
                "ACTIVE" => "Y",
                "ID" => $this->arParams["IBLOCK_ID"],
            ));

            $this->arResult = $rsIBlock->GetNext();
            if (!$this->arResult["ID"]) {
                array_push($this->MessageError, Loc::getMessage('CL_NONE_IBLOCK_ID'));
            }
        }

        $this->arOptions = Handler::getOptions();
    }

    /**
     * @brief Основная логика
     **/
    protected function getResult()
    {
        #Получаем данные о шаблоне
        $this->InitComponentTemplate();
        //инициализируем шаблон, а затем получаем название и расположение
        $template = &$this->GetTemplate();
        $this->arResult["TEMPLATE_FILE"] = $template->GetFile();
        $this->arResult["TEMPLATE_FOLDER"] = $template->GetFolder();
        $this->arResult["PATH"] = $this->GetPath();

        if (count($this->MessageError) == 0) {
            $this->getCity();
            $this->getCertificateList();
            $this->getQuestList();
        }

    }

    protected function getCity()
    {
        $el = new CIBlockElement();

        $arFilter = Array(
            "IBLOCK_ID" => $this->arOptions["GEOIP"]["IBLOCK_ID"],
            "ID" => $_SESSION["CITY_INFO"]["iCurCityID"],
        );

        $arSelect = Array(
            "ID",
            "NAME",
            "PROPERTY_CERTIFICATE_QUEST",
            "PROPERTY_CERTIFICATE_PERFORMANCE",
            "PROPERTY_CERTIFICATE_ACTION_GAME",
        );

        $res = $el->GetList(Array(), $arFilter, false, Array(), $arSelect);

        while ($arFields = $res->Fetch()) {
            $this->arResult["CITY"] = Array(
                "ID" => $arFields["ID"],
                "CERTIFICATE_QUEST" => $arFields["PROPERTY_CERTIFICATE_QUEST_VALUE"] > 0  ? $arFields["PROPERTY_CERTIFICATE_QUEST_VALUE"] : 0,
                "CERTIFICATE_PERFORMANCE" => $arFields["PROPERTY_CERTIFICATE_PERFORMANCE_VALUE"] > 0 ? $arFields["PROPERTY_CERTIFICATE_PERFORMANCE_VALUE"] : 0,
                "CERTIFICATE_ACTION_GAME" => $arFields["PROPERTY_CERTIFICATE_ACTION_GAME_VALUE"] > 0 ? $arFields["PROPERTY_CERTIFICATE_ACTION_GAME_VALUE"] : 0,
            );
        }
    }

    protected function getCertificateList()
    {
        $el = new CIBlockElement();

        $arFilter = Array(
            "IBLOCK_ID" => $this->arResult["ID"],
            "ACTIVE" => "Y",
            "!PROPERTY_QUEST_TYPE" => false,
            "!PROPERTY_PAGE_BUY" => false,
            "!PROPERTY_PRICE_CODE" => false,
            "PROPERTY_CITIES" => $_SESSION["CITY_INFO"]["iCurCityID"],
        );

        $arSelect = Array(
            "ID",
            "NAME",
            "PROPERTY_TYPE",
            "PROPERTY_QUEST_TYPE",
            "PROPERTY_PAGE_BUY",
            "PROPERTY_PRICE_CODE",
            "PROPERTY_CERTIFICATE_ABILITY_COST",
            "PROPERTY_CERTIFICATE_MIN_COST"
        );

        $res = $el->GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arSelect);

        while ($ob = $res->GetNextElement()) {
            $arFields = $ob->GetFields();

            $this->arResult["ITEMS"][] = array(
                "ID" => $arFields["ID"],
                "NAME" => $arFields["NAME"],
                "TYPE" => $arFields["PROPERTY_TYPE_VALUE"],
                "QUEST_TYPE" => $arFields["PROPERTY_QUEST_TYPE_VALUE"],
                "PAGE_BUY" => $arFields["PROPERTY_PAGE_BUY_VALUE"],
                "PRICE" => isset($this->arResult["CITY"][$arFields["PROPERTY_PRICE_CODE_VALUE"]]) ? $this->arResult["CITY"][$arFields["PROPERTY_PRICE_CODE_VALUE"]] : 0 ,
                "CERTIFICATE_ABILITY_COST" => $arFields["PROPERTY_CERTIFICATE_ABILITY_COST_VALUE"],
                "CERTIFICATE_MIN_COST" => $arFields["PROPERTY_CERTIFICATE_MIN_COST_VALUE"],
            );
        }

    }

    protected function getQuestList()
    {
        if ($this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"] > 0) {
            $el = new CIBlockElement();

            $arFilter = Array(
                "IBLOCK_ID" => $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
                "ACTIVE" => "Y",
            );


            $arFilter = array_merge($arFilter, $GLOBALS['arrFilterQuest']);

            $arSelect = array(
                "IBLOCK_ID",
                "ID",
                "PROPERTY_BASE_PRICE"
            );

            $date_unix = strtotime(date("d.m.Y H:i:s"));

            foreach ($this->arResult["ITEMS"] as $key => $arItem) {
                if ($arItem["QUEST_TYPE"] > 0) {
                    $arFilter["=PROPERTY_QUEST_TYPE"] = $arItem["QUEST_TYPE"];

                    if ($arItem["PRICE"] > 0) {
                        $arFilter["<=PROPERTY_BASE_PRICE"] = $arItem["PRICE"];
                    }

                    $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                    while ($arFields = $res->Fetch()) {
                        //var_dump($arFields['PROPERTY_BASE_PRICE_VALUE']);
                        //var_dump($arFilter);

                        #Проверяем на активные торговые предложения (расписания)
                        if ($this->arOptions["QUEST"]["OFFERS_IBLOCK_ID"] > 0) {
                            $arSelectOffer = Array();
                            $arFilterOffer = Array(
                                "IBLOCK_ID" => $this->arOptions["QUEST"]["OFFERS_IBLOCK_ID"],
                                "ACTIVE_DATE" => "Y",
                                "=PROPERTY_CML2_LINK" => $arFields["ID"],
                                ">=CODE" => $date_unix,
                                "=PROPERTY_RESERVED" => false,
                            );


                            $resOffers = $el->GetList(Array(), $arFilterOffer, false, false, $arSelectOffer);

                            if ($resOffers->SelectedRowsCount() > 0) {
                                if ($arItem["PRICE"] <= 0) {
                                    while ($arFieldsOffers = $resOffers->Fetch()) {
                                        $arPriceOffer = CPrice::GetBasePrice($arFieldsOffers["ID"]);

                                        if ($arPriceOffer["PRICE"] > $this->arResult["ITEMS"][$key]["PRICE"]) {
                                            $this->arResult["ITEMS"][$key]["PRICE"] = $arPriceOffer["PRICE"];
                                        }
                                    }
                                }

                                $this->arResult["ITEMS"][$key]["QUEST_LIST"][] = $arFields["ID"];
                                $this->arResult["ITEMS"][$key]["BASE_PRICE"][$arFields["ID"]] = $arFields['PROPERTY_BASE_PRICE_VALUE'];
                            }
                        }
                    }
                }
            }
        }
    }

    /**
     * @brief Обработка ошибок
     **/
    protected function actionMessage()
    {
        $this->arResult["MESSAGE_ERROR"] = $this->MessageError;
        $this->arResult["MESSAGE_SEND"] = $this->MessageSend;
    }

    public function executeComponent()
    {
        try {
            $this->arResult["COMPONENT_ID"] = 'CL';
            $this->includeComponentLang("class.php");
            $this->checkModules();
            $this->checkOptions();
            $this->getResult();
            $this->actionMessage();
            $this->includeComponentTemplate();

            return $this->arResult["ITEMS"][0]["QUEST_LIST"];

        } catch (Exception $e) {
            ShowError($e->getMessage());
        }
    }

}

;