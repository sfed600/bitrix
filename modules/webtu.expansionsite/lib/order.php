<?php

namespace Webtu\ExpansionSite;

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

use Bitrix\Currency\CurrencyManager;
use Bitrix\Main;
use Bitrix\Main\Context;
use Bitrix\Main\Loader;
use Bitrix\Main\Localization\Loc;
use Bitrix\Sale\Delivery;
use Bitrix\Sale\PaySystem;

Loc::loadMessages(__FILE__);


class Order
{
    private $arResult = array("MESSAGE" => array("ERROR" => array(), "OK" => array()));
    public $order;
    private $arOptions;

    public function __construct()
    {
    }

    /**
     * @throws Main\LoaderException
     */
    protected function checkModules()
    {
        if (!Loader::includeModule('iblock')) {
            throw new Main\LoaderException(Loc::getMessage('ORDER_NOT_INSTALLED_MODULE', Array("#ID#" => "iblock")));
        }

        if (!Loader::includeModule('webtu.expansionsite')) {
            throw new Main\LoaderException(Loc::getMessage('ORDER_NOT_INSTALLED_MODULE', Array("#ID#" => "webtu.expansionsite")));
        }

        if (!Loader::includeModule('sale')) {
            throw new Main\LoaderException(Loc::getMessage('ORDER_NOT_INSTALLED_MODULE', Array("#ID#" => "sale")));
        }
        if (!Loader::includeModule('catalog')) {
            throw new Main\LoaderException(Loc::getMessage('ORDER_NOT_INSTALLED_MODULE', Array("#ID#" => "catalog")));
        }
    }

    /**
     * @param $orderId
     * @param $comment
     * @param $booking_id
     * @param bool $checkIsDate
     * @param bool $is_user
     * @return array|bool
     * @throws Main\ArgumentException
     * @throws Main\ArgumentNullException
     * @throws Main\ArgumentOutOfRangeException
     * @throws Main\LoaderException
     * @throws Main\NotImplementedException
     */
    public function cancel($orderId, $comment, $booking_id, $checkIsDate = false, $is_user = false)
    {
        $this->checkModules();

        $el = new \CIBlockElement();

        if ($orderId <= 0) {
            return false;
        }

        $isTransactPay = true;

        $this->arOptions = Handler::getOptions();

        $this->order = \Bitrix\Sale\Order::load($orderId);

        if ($this->order) {
            if (\Bitrix\Sale\Order::isLocked($orderId)) {
                \Bitrix\Sale\Order::unlock($orderId);
            }

            $this->order->setField('CANCELED', 'Y');

            if (strlen($comment) > 0) {
                $this->order->setField('REASON_CANCELED', $comment);
            }

            $this->order->setField('STATUS_ID', 'O');

            $res = $this->order->save();
            //$res = true;
            if ($res) {
                if (
                    $booking_id > 0
                    && $this->arOptions["QUEST"]["OFFERS_IBLOCK_ID"] > 0
                ) {
                    $el->SetPropertyValuesEx($booking_id, $this->arOptions["QUEST"]["OFFERS_IBLOCK_ID"], array("RESERVED" => false));
                }

                if ($checkIsDate) {
                    #Получение значение свойства Дата бронирования
                    $this->arResult["ORDER"]["DATE_BOOKING"] = $this->getOrderProperty('DATE_BOOKING');
                    #Дата бронирования unix формат
                    $date_booking_unix = strtotime($this->arResult["ORDER"]["DATE_BOOKING"]);

                    #Текущая дата и время
                    $current_date = date('d.m.Y H:i:s');
                    #Текущая дата и время unix формат
                    $current_date_unix = strtotime($current_date);

                    if ($current_date_unix > $date_booking_unix) {
                        $isTransactPay = false;
                    }
                }

                if ($isTransactPay) {
                    #Получение значение свойства ID организатора
                    $this->arResult["ORDER"]["ID_ORGANIZATION"] = $this->getOrderProperty('ID_ORGANIZATION');
                    #Получение значение свойства Списанная сумма у организатора
                    $this->arResult["ORDER"]["DEBIT_SUM"] = $this->getOrderProperty('DEBIT_SUM');

                    if ($this->arResult["ORDER"]["ID_ORGANIZATION"] > 0) {
                        $rsUser = \CUser::GetByID($this->arResult["ORDER"]["ID_ORGANIZATION"]);
                        $arUserOrg = $rsUser->Fetch();

                        if ($this->arResult["ORDER"]["DEBIT_SUM"] <= 0) {
                            $this->arResult["ORDER"]["PRICE"] = $this->order->getField("PRICE");

                            if ($arUserOrg["UF_PERCENT_DEBIT"] > 0) {
                                $PERCENT_DEBIT = $arUserOrg["UF_PERCENT_DEBIT"];
                            } else {
                                $PERCENT_DEBIT = $this->arOptions["ORDER"]["PERCENT_DEBIT"];
                            }

                            if (
                                intval($PERCENT_DEBIT) > 0
                                && intval($PERCENT_DEBIT) < 100
                            ) {
                                $this->arResult["ORDER"]["DEBIT_SUM"] = $this->arResult["ORDER"]["PRICE"] * $PERCENT_DEBIT / 100;
                            }
                        }

                        $this->payOrganizationSum($this->arResult["ORDER"]["ID_ORGANIZATION"], $orderId, $this->arResult["ORDER"]["DEBIT_SUM"]);

                        #Если отменил пользователь, то оповещаем организатора
                        if ($is_user) {
                            if (strlen($arUserOrg["EMAIL"]) > 0) {
                                $this->arResult["ORDER"]["PRICE"] = $this->order->getField("PRICE");
                                $this->arResult["ORDER"]["PLAYERS_COUNT"] = $this->getOrderProperty("PLAYERS_COUNT");
                                $this->arResult["ORDER"]["DATE_BOOKING"] = $this->getOrderProperty("DATE_BOOKING");
                                $this->arResult["ORDER"]["ID_QUEST"] = $this->getOrderProperty("ID_QUEST");

                                #Получение значение ID пользователя
                                $USER_ID = $this->order->getUserId();

                                if ($USER_ID == $this->arOptions["ORDER"]["AUTH_USER_ID"]) {
                                    $this->arResult["ORDER"]["NAME"] = $this->getOrderProperty('NAME');
                                    $this->arResult["ORDER"]["LAST_NAME"] = $this->getOrderProperty('LAST_NAME');
                                    $this->arResult["ORDER"]["EMAIL"] = $this->getOrderProperty('EMAIL');
                                    $this->arResult["ORDER"]["PERSONAL_PHONE"] = $this->getOrderProperty('PHONE');
                                } else {
                                    $rsUser = \CUser::GetByID($USER_ID);
                                    $arUser = $rsUser->Fetch();

                                    $this->arResult["ORDER"]["USER_ID"] = $arUser["ID"];
                                    $this->arResult["ORDER"]["NAME"] = $arUser["NAME"];
                                    $this->arResult["ORDER"]["LAST_NAME"] = $arUser["LAST_NAME"];
                                    $this->arResult["ORDER"]["EMAIL"] = $arUser["EMAIL"];
                                    $this->arResult["ORDER"]["PERSONAL_PHONE"] = $arUser["PERSONAL_PHONE"];

                                }


                                if (
                                    $this->arResult["ORDER"]["ID_QUEST"] > 0
                                    && $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"] > 0
                                ) {
                                    $arFilter = Array(
                                        "IBLOCK_ID" => $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
                                        "ID" => $this->arResult["ORDER"]["ID_QUEST"],
                                    );

                                    $arSelect = Array(
                                        "ID",
                                        "NAME",
                                        "CODE",
                                    );

                                    $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                                    $this->arResult["QUEST"] = $res->Fetch();
                                }

                                $arMailFields = array(
                                    "EVENT_NAME" => "WEBTU_BOOKING_CANCEL",
                                    "LID" => SITE_ID,
                                    "C_FIELDS" => array(
                                        "EMAIL_TO" => $arUserOrg["EMAIL"],
                                        "ORDER_ID" => $orderId,
                                        "ORDER_PRICE_FORMAT" => \CCurrencyLang::CurrencyFormat($this->arResult["ORDER"]["PRICE"], "RUB", true),
                                        "NAME" => $this->arResult["ORDER"]["NAME"],
                                        "LAST_NAME" => $this->arResult["ORDER"]["LAST_NAME"],
                                        "PERSONAL_PHONE" => $this->arResult["ORDER"]["PERSONAL_PHONE"],
                                        "EMAIL" => $this->arResult["ORDER"]["EMAIL"],
                                        "COMMENT" => $comment,
                                        "COUNT_PLAYERS" => ($this->arResult["ORDER"]["PLAYERS_COUNT"] > 0) ? $this->arResult["ORDER"]["PLAYERS_COUNT"] : 'не указано',
                                        "QUEST_URL" => '/poisk-kvestov/' . $this->arResult["QUEST"]["CODE"] . '/',
                                        "QUEST_NAME" => ($this->arResult["QUEST"]["NAME"]) ? $this->arResult["QUEST"]["NAME"] : 'Не указано',
                                        "BOOKING_DATETIME" => $this->arResult["ORDER"]["DATE_BOOKING"],

                                    ),
                                );

                                \Bitrix\Main\Mail\Event::send($arMailFields);
                            }
                        }
                    }
                }
            } else {
                array_push($this->arResult["MESSAGE"]["ERROR"], Loc::getMessage('ORDER_ERROR_CANCEL_ORDER'));
            }
        }

        return $this->arResult;

    }

    /**
     * @param $user_id
     * @param $order_id
     * @param $sum
     * @return bool|int
     * @throws Main\LoaderException
     */
    public function payOrganizationSum($user_id, $order_id, $sum)
    {
        if ($user_id <= 0 || $sum == 0) {
            return false;
        }

        if ($sum > 0) {
            $description = Loc::getMessage('ORDER_PAY_SUM_DESCRIPTION', Array("#SUM#" => $sum, "#ORDER_ID#" => $order_id));
        } else {
            $description = Loc::getMessage('ORDER_DEDUCT_SUM_DESCRIPTION', Array("#SUM#" => -1 * $sum, "#ORDER_ID#" => $order_id));
        }

        $notes = "";

        #Списание суммы $SUM
        $result_transact = \Webtu\ExpansionSite\Bonus::userUpdateAccount(
            $user_id,
            $sum,
            "RUB",
            $description,
            $order_id,
            $notes
        );

        return $result_transact["TRANSACT_ID"];
    }

    /**
     * @brief создание заказа
     * @param array $arParams
     * @return bool|int
     * @throws Main\ArgumentException
     * @throws Main\ArgumentNullException
     * @throws Main\ArgumentOutOfRangeException
     * @throws Main\ArgumentTypeException
     * @throws Main\LoaderException
     * @throws Main\NotSupportedException
     * @throws Main\ObjectNotFoundException
     * @throws Main\SystemException
     */
    public function create($arParams = array())
    {
        $this->checkModules();

        $this->arOptions = Handler::getOptions();

        $siteId = Context::getCurrent()->getSite();
        $currencyCode = CurrencyManager::getBaseCurrency();

        #Анонимный пользователь (для заказов неавторизованных пользователей)
        if (intval($this->arOptions["ORDER"]["AUTH_USER_ID"]) > 0) {
            $anonymous_user_id = $this->arOptions["ORDER"]["AUTH_USER_ID"];
        } else {
            $anonymous_user_id = 1;
        }

        #Создаёт новый заказ
        $this->order = \Bitrix\Sale\Order::create($siteId, $arParams["ORDER"]["USER_ID"] > 0 ? $arParams["ORDER"]["USER_ID"] : $anonymous_user_id);
        $this->order->setPersonTypeId(1);
        $this->order->setField('CURRENCY', $currencyCode);

        #Устанавливаем поля комментария покупателя
        if (strlen($arParams["ORDER"]["COMMENT"]) > 0) {
            $this->order->setField('USER_DESCRIPTION', $arParams["ORDER"]["COMMENT"]);
        }

        #Создаём корзину с одним товаром
        $basket = \Bitrix\Sale\Basket::create($siteId);

        #Передаем в корзину наше торговое предложение (нужное расписание)
        $item = $basket->createItem('catalog', $arParams["BOOKING"]["ID"]);

        #Установим параметры расписания
        $item->setFields(array(
            'QUANTITY' => 1,
            'NAME' => $arParams["QUEST"]["NAME"] . ' ' . $arParams["BOOKING"]["DATE"] . ' ' . $arParams["BOOKING"]["TIME"],
            'PRICE' => $arParams["ORDER"]["PRICE"],
            'CUSTOM_PRICE' => 'Y',
            'CURRENCY' => $currencyCode,
            'LID' => $siteId,
            'PRODUCT_PROVIDER_CLASS' => '\CCatalogProductProvider',
        ));

        $this->order->setBasket($basket);

        #Создаём одну отгрузку и устанавливаем способ доставки - "Без доставки" (он служебный)
        $shipmentCollection = $this->order->getShipmentCollection();
        $shipment = $shipmentCollection->createItem();
        $service = Delivery\Services\Manager::getById(Delivery\Services\EmptyDeliveryService::getEmptyDeliveryServiceId());
        $shipment->setFields(array(
            'DELIVERY_ID' => $service['ID'],
            'DELIVERY_NAME' => $service['NAME'],
        ));
        $shipmentItemCollection = $shipment->getShipmentItemCollection();
        $shipmentItem = $shipmentItemCollection->createItem($item);
        $shipmentItem->setQuantity($item->getQuantity());

        #Создаём оплату со способом из настроек модуля
        if ($arParams["ORDER"]["PAYMENT"] <= 0) {
            $arParams["ORDER"]["PAYMENT"] = 1;
        }

        $paySystemService = PaySystem\Manager::getObjectById($arParams["ORDER"]["PAYMENT"]);

        $paymentCollection = $this->order->getPaymentCollection();

        $payment = $paymentCollection->createItem($paySystemService);

        $payment->setField("SUM", $arParams["ORDER"]["PRICE"]);
        $payment->setField("CURRENCY", $currencyCode);

        #Устанавливаем свойства
        $propertyCollection = $this->order->getPropertyCollection();

        #Email
        $emailProperty = $this->getPropertyByCode($propertyCollection, 'EMAIL');
        $emailProperty->setValue($arParams["ORDER"]["EMAIL"]);

        #Телефон
        $phoneProperty = $this->getPropertyByCode($propertyCollection, 'PHONE');
        $phoneProperty->setValue($arParams["ORDER"]["PERSONAL_PHONE"]);

        #Имя
        $nameProperty = $this->getPropertyByCode($propertyCollection, 'NAME');
        $nameProperty->setValue($arParams["ORDER"]["NAME"]);

        #Фамилия
        $lastNameProperty = $this->getPropertyByCode($propertyCollection, 'LAST_NAME');
        $lastNameProperty->setValue($arParams["ORDER"]["LAST_NAME"]);

        #Дата бронирования
        $dateBookingProperty = $this->getPropertyByCode($propertyCollection, 'DATE_BOOKING');
        $dateBookingProperty->setValue(date('d.m.Y H:i:s', strtotime($arParams["BOOKING"]["DATE"] . ' ' . $arParams["BOOKING"]["TIME"])));

        $dateBookingTimestampProperty = $this->getPropertyByCode($propertyCollection, 'DATE_BOOKING_TIMESTAMP');
        $dateBookingTimestampProperty->setValue(strtotime($arParams["BOOKING"]["DATE"] . ' ' . $arParams["BOOKING"]["TIME"]));

        $notifyReviewProperty = $this->getPropertyByCode($propertyCollection, 'NOTIFY_REVIEW');
        $notifyReviewProperty->setValue('N');

        #ID Расписания
        $idBookingProperty = $this->getPropertyByCode($propertyCollection, 'ID_BOOKING');
        $idBookingProperty->setValue($arParams["BOOKING"]["ID"]);

        #ID квеста
        $idQuestProperty = $this->getPropertyByCode($propertyCollection, 'ID_QUEST');
        $idQuestProperty->setValue($arParams["QUEST"]["ID"]);

        #Количество игроков
        $playersCountProperty = $this->getPropertyByCode($propertyCollection, 'PLAYERS_COUNT');
        $playersCountProperty->setValue($arParams["ORDER"]["PLAYERS_COUNT"]);

        #ID организатора квеста
        $playersCountProperty = $this->getPropertyByCode($propertyCollection, 'ID_ORGANIZATION');
        $playersCountProperty->setValue($arParams["QUEST"]["ORGANIZATION_ID"]);

        #ID заказа в системе
        if ($arParams["ORDER"]["BOOK_ID_SYSTEM"] > 0) {
            $bookIdSystemProperty = $this->getPropertyByCode($propertyCollection, 'BOOK_ID_SYSTEM');
            $bookIdSystemProperty->setValue($arParams["ORDER"]["BOOK_ID_SYSTEM"]);
        }
        
        #Цена по умолчанию за квест
        $defaultPriceProperty = $this->getPropertyByCode($propertyCollection, 'DEFAULT_PRICE');
        $defaultPriceProperty->setValue($arParams["ORDER"]["DEFAULT_PRICE"]);

        #Списанная сумма у организатора
        $debitSumProperty = $this->getPropertyByCode($propertyCollection, 'DEBIT_SUM');
        $debitSumProperty->setValue($arParams["ORDER"]["DEBIT_SUM"]);

        #Группа цен
        $priceGroupsProperty = $this->getPropertyByCode($propertyCollection, 'PRICE_GROUPS');
        $priceGroupsProperty->setValue($arParams["ORDER"]["PRICE_GROUPS"]);

        $this->order->doFinalAction(true);
        $result = $this->order->save();

        if ($result) {
            $orderId = $this->order->getId();

            return $orderId;
        } else {
            return false;
        }

    }

    /**
     * @brief Получение значение свойства ID заказа в системе бронирования
     * @param $prop_code
     * @return mixed
     */
    public function getOrderProperty($prop_code)
    {
        if (!$this->order) {
            return false;
        }

        $propertyCollection = $this->order->getPropertyCollection();

        $propertyObject = $this->getPropertyByCode($propertyCollection, $prop_code);

        $propertyValue = $propertyObject->getValue();

        return $propertyValue;

    }

    /**
     * @param $orderId
     * @throws Main\ArgumentNullException
     * @throws Main\ArgumentOutOfRangeException
     * @throws Main\LoaderException
     * @throws Main\NotImplementedException
     */
    public function sendMessageToReview($orderId)
    {
        $this->checkModules();
        $this->arOptions = Handler::getOptions();

        $this->order = \Bitrix\Sale\Order::load($orderId);

        $this->arResult["ORDER"]["ID_QUEST"] = $this->getOrderProperty('ID_QUEST');
        $this->arResult["ORDER"]["NOTIFY_REVIEW"] = $this->getOrderProperty('NOTIFY_REVIEW');

        if (
            $this->arResult["ORDER"]["NOTIFY_REVIEW"] == 'N'
            && $this->arResult["ORDER"]["ID_QUEST"] > 0
            && $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"] > 0
        ) {
            $el = new \CIBlockElement();

            $arFilter = Array(
                "IBLOCK_ID" => $this->arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
                "ID" => $this->arResult["ORDER"]["ID_QUEST"],
                "ACTIVE" => "Y",
            );
            $arSelect = Array(
                "ID",
                "NAME",
                "CODE",
                "LIST_PAGE_URL",
                "DETAIL_PAGE_URL",
                "PROPERTY_CITY"
            );

            $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);
            while ($ob = $res->GetNextElement()) {
                $arFields = $ob->GetFields();
                $this->arResult["QUEST"] = array(
                    "ID" => $arFields["ID"],
                    "NAME" => $arFields["NAME"],
                    "CODE" => $arFields["CODE"],
                    "CITY_ID" => $arFields["PROPERTY_CITY_VALUE"],
                    "LIST_PAGE_URL" => $arFields["LIST_PAGE_URL"],
                    "DETAIL_PAGE_URL" => $arFields["DETAIL_PAGE_URL"],
                );
            }

            if (
                $this->arResult["QUEST"]["CITY_ID"] > 0
                && $this->arOptions["GEOIP"]["IBLOCK_ID"] > 0
            ) {
                $arFilter = array(
                    "IBLOCK_ID" => $this->arOptions["GEOIP"]["IBLOCK_ID"],
                    "ACTIVE" => "Y",
                    "ID" => $this->arResult["QUEST"]["CITY_ID"]
                );
                $arSelect = Array(
                    "ID",
                    "CODE",
                    "PROPERTY_CITY_NAME_GENITIVE"
                );

                $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                while ($ob = $res->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $this->arResult["CITY"] = array(
                        "ID" => $arFields["ID"],
                        "CODE" => $arFields["CODE"],
                        'CITY_NAME_GENITIVE' => $arFields["PROPERTY_CITY_NAME_GENITIVE_VALUE"],
                    );
                }
            }

            if ($this->order->getUserId() == $this->arOptions["ORDER"]["AUTH_USER_ID"]) {
                $this->messageNoAuthUser();
            } else {
                $this->messageAuthUser();
            }

            $propertyCollection = $this->order->getPropertyCollection();
            $notifyReviewProperty = $this->getPropertyByCode($propertyCollection, 'NOTIFY_REVIEW');
            $notifyReviewProperty->setValue('Y');
            $this->order->save();
        }
    }

    /**
     *
     */
    protected function messageAuthUser()
    {
        if (
            $this->arResult["QUEST"]["ID"] > 0
            && $this->arResult["CITY"]["ID"] > 0
        ) {
            $rsUser = \CUser::GetByID($this->order->getUserId());
            $arUser = $rsUser->Fetch();

            $http = (\CMain::IsHTTPS()) ? "https://" : "http://";

            if ($this->arResult["CITY"]["CODE"] == $this->arOptions["GEOIP"]["CITY_DEFAULT"]) {
                $site = $http . SITE_SERVER_NAME;
            } else {
                $site = $http . $this->arResult["CITY"]["CODE"] . '.' . SITE_SERVER_NAME;
            }

            $arMailFields = array(
                "EVENT_NAME" => "QUEST_REVIEW_AUTH",
                "LID" => SITE_ID,
                "C_FIELDS" => array(
                    "USER_NAME" => $arUser["NAME"],
                    "EMAIL_TO" => $arUser["EMAIL"],
                    "QUEST_NAME" => $this->arResult["QUEST"]["NAME"],
                    "CITY_NAME" => $this->arResult["CITY"]["CITY_NAME_GENITIVE"],
                    "REVIEW_URL" => $site . $this->arResult["QUEST"]["DETAIL_PAGE_URL"] . 'otzyvy/?show=Y&order=' . $this->order->getId(),
                    "CATALOG_URL" => $site . $this->arResult["QUEST"]["LIST_PAGE_URL"],
                    "SITE_URL" => $site

                ),
            );

            \Bitrix\Main\Mail\Event::send($arMailFields);
        }
    }

    /**
     * @throws \Exception
     */
    protected function messageNoAuthUser()
    {
        if (
            $this->arResult["QUEST"]["ID"] > 0
            && $this->arResult["CITY"]["ID"] > 0
        ) {

            $http = (\CMain::IsHTTPS()) ? "https://" : "http://";

            if ($this->arResult["CITY"]["CODE"] == $this->arOptions["GEOIP"]["CITY_DEFAULT"]) {
                $site = $http . SITE_SERVER_NAME;
            } else {
                $site = $http . $this->arResult["CITY"]["CODE"] . '.' . SITE_SERVER_NAME;
            }

            $arMailFields = array(
                "EVENT_NAME" => "QUEST_REVIEW_NO_AUTH",
                "LID" => SITE_ID,
                "C_FIELDS" => array(
                    "USER_NAME" => $this->getOrderProperty('NAME'),
                    "EMAIL_TO" => $this->getOrderProperty('EMAIL'),
                    "QUEST_NAME" => $this->arResult["QUEST"]["NAME"],
                    "CITY_NAME" => $this->arResult["CITY"]["CITY_NAME_GENITIVE"],
                    "REVIEW_URL" => $site . $this->arResult["QUEST"]["DETAIL_PAGE_URL"] . 'otzyvy/?code=' . $this->generateToken(),
                    "CATALOG_URL" => $site . $this->arResult["QUEST"]["LIST_PAGE_URL"],
                    "SITE_URL" => $site

                ),
            );

            \Bitrix\Main\Mail\Event::send($arMailFields);
        }
    }

    /**
     * @return string
     * @throws \Exception
     */
    protected function generateToken()
    {
        $token = md5(time());

        \Webtu\ExpansionSite\Mysql\ReviewTokenListTable::add(array(
            "ORDER_ID" => $this->order->getId(),
            "TOKEN" => $token,
        ));

        return $token;
    }

    /**
     * @param $propertyCollection
     * @param $code
     * @return mixed
     */
    protected function getPropertyByCode($propertyCollection, $code)
    {
        foreach ($propertyCollection as $property) {
            if ($property->getField('CODE') == $code)
                return $property;
        }
    }

    /**
     * @param $orderId
     * @throws Main\ArgumentNullException
     * @throws Main\LoaderException
     * @throws Main\NotImplementedException
     * @throws Main\ObjectNotFoundException
     */
    public function overpaymentBooking($orderId)
    {
        $this->checkModules();
        $this->arOptions = Handler::getOptions();

        $this->order = \Bitrix\Sale\Order::load($orderId);

        $this->arResult["ORDER"]["ID_QUEST"] = $this->getOrderProperty('ID_QUEST');
        $this->arResult["ORDER"]["ID_ORGANIZATION"] = $this->getOrderProperty('ID_ORGANIZATION');

        $paymentIds = $this->order->getPaymentSystemId();
        $this->arResult["ORDER"]["PAYMENT_ID"] = (int)$paymentIds[0];

        if (
            $this->arResult["ORDER"]["ID_QUEST"] > 0
            && $this->arResult["ORDER"]["ID_ORGANIZATION"] > 0
            && ($this->arResult["ORDER"]["PAYMENT_ID"] == 9 || $this->arResult["ORDER"]["PAYMENT_ID"] == 11)
        ) {
            $rsUser = \CUser::GetByID($this->arResult["ORDER"]["ID_ORGANIZATION"]);
            $arUser = $rsUser->Fetch();

            $this->arResult["CITY_CODE"] = $this->arOptions["GEOIP"]["CITY_DEFAULT"];

            if (strlen($arUser["WORK_CITY"]) > 0 && $this->arOptions["GEOIP"]["IBLOCK_ID"] > 0) {
                $el = new \CIBlockElement();
                $arFilter = array(
                    "IBLOCK_ID" => $this->arOptions["GEOIP"]["IBLOCK_ID"],
                    "ACTIVE" => "Y",
                    "=NAME" => $arUser["WORK_CITY"]
                );
                $arSelect = Array(
                    "ID",
                    "CODE",
                );

                $res = $el->GetList(Array(), $arFilter, false, false, $arSelect);

                while ($ob = $res->GetNextElement()) {
                    $arFields = $ob->GetFields();
                    $this->arResult["CITY_CODE"] = $arFields["CODE"];
                }
            }

            $http = (\CMain::IsHTTPS()) ? "https://" : "http://";


            if ($this->arResult["CITY_CODE"] == $this->arOptions["GEOIP"]["CITY_DEFAULT"] || empty($this->arResult["CITY_CODE"])) {
                $site = $http . 'kvest-battle.ru';
            } else {
                $site = $http . $this->arResult["CITY_CODE"] . '.kvest-battle.ru';
            }

            $arMailFields = array(
                "EVENT_NAME" => "OVERPAYMENT_BOOKING",
                "LID" => SITE_ID,
                "C_FIELDS" => array(
                    "EMAIL_TO" => (strlen($arUser["WORK_MAILBOX"]) > 0) ? $arUser["WORK_MAILBOX"] : $arUser["EMAIL"],
                    "SITE_URL" => $site,
                    "ORDER_ID" => $this->order->getId(),
                ),
            );

            \Bitrix\Main\Mail\Event::send($arMailFields);
        }
    }

}