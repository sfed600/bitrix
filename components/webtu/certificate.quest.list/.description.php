<?
if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();
use Bitrix\Main\Localization\Loc;
Loc::loadMessages(__FILE__);

$arComponentDescription = array(
    "NAME" => Loc::getMessage("CQL_LIST_NAME"),
    "DESCRIPTION" => Loc::getMessage("CQL_LIST_DESCRIPTION"),
    "ICON" => "/images/news_list.gif",
    "SORT" => 200,
    "PATH" => array(
        "ID"   => "WEBTU",
        "NAME" => Loc::getMessage("WEBTU_PATH_ADDITIONAL"),
        "CHILD" => array(
            "ID" => "certificate",
            "NAME" => Loc::getMessage("CQL_PATH_ADDITIONAL"),
            "SORT" => 500,
            "CHILD" => array(
                "ID" => "certificate_cmpx",
            ),
        ),
    )
);
?>
