<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

use \Bitrix\Main;
use \Bitrix\Main\Application;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;
use \Webtu\ExpansionSite\Handler;

Loc::loadMessages(__FILE__);

class CLAjax extends CBitrixComponent
{
    public $messageError = array();
    public $messageSend = array();

    /**
     * @brief Проверка подключаемых модулей
     **/
    protected function checkModules()
    {

        if (!Loader::includeModule("iblock"))
        {
            throw new Main\LoaderException(Loc::getMessage("CL_NOT_INSTALLED_IBLOCK", Array ("#ID#" => "iblock")) );
        }

        if (!Loader::includeModule("webtu.expansionsite"))
        {
            throw new Main\LoaderException(Loc::getMessage("CL_NOT_INSTALLED_IBLOCK", Array ("#ID#" => "webtu.expansionsite")) );
        }
    }


    /**
     * @brief Основная логика
     **/
    protected function getResult()
    {
        global $APPLICATION;

        $arOptions = Handler::getOptions();

        $request = Application::getInstance()->getContext()->getRequest();
        $this->arResult["REQUEST"]["PROP"] = $request->getPost("PROP");

        if($request->isAjaxRequest())
        {
            $this->getPropsRequired();

            if( count($this->messageError) == 0 )
            {
                if ($this->arResult["REQUEST"]["PROP"]["ACTION"] == "QUEST_LIST")
                {
                    if ( !is_array($this->arResult["REQUEST"]["PROP"]["QUEST_LIST"]) )
                    {
                        $this->arResult["REQUEST"]["PROP"]["QUEST_LIST"] = array();
                    }

                    $arFilterUserPrice = [];
                    if (isset($this->arResult['REQUEST']['PROP']['CERTIFICATE_MIN_COST'])) {
                        $arFilterUserPrice['CERTIFICATE_MIN_COST'] = $this->arResult['REQUEST']['PROP']['CERTIFICATE_MIN_COST'];
                    }

                    if (isset($this->arResult['REQUEST']['PROP']['CERTIFICATE_MIN_COST'])) {
                        $arFilterUserPrice['USER_PRICE'] = $this->arResult['REQUEST']['PROP']['USER_PRICE'];
                    }

                    ob_start();
                        $APPLICATION->IncludeComponent(
                            'webtu:certificate.quest.list',
                            "",
                            Array(
                                "FILTER_NAME" => "arrFilterQuest",
                                "FILTER_DATA" => array_merge($GLOBALS['arrFilterQuest'], $arFilterUserPrice),

                                "AJAX_MODE" => "N",
                                "AJAX_OPTION_ADDITIONAL" => "",
                                "AJAX_OPTION_HISTORY" => "N",
                                "AJAX_OPTION_JUMP" => "N",
                                "AJAX_OPTION_STYLE" => "Y",
                                "DISPLAY_BOTTOM_PAGER" => "Y",
                                "DISPLAY_TOP_PAGER" => "N",
                                "IBLOCK_ID" => $arOptions["QUEST"]["CATALOG_IBLOCK_ID"],
                                "IBLOCK_TYPE" => "quest",
                                "MESSAGE_404" => "",
                                "NEWS_COUNT" => "10",
                                "PAGER_BASE_LINK_ENABLE" => "N",
                                "PAGER_DESC_NUMBERING" => "N",
                                "PAGER_DESC_NUMBERING_CACHE_TIME" => "36000",
                                "PAGER_SHOW_ALL" => "N",
                                "PAGER_SHOW_ALWAYS" => "N",
                                "PAGER_TEMPLATE" => ".default",
                                "PAGER_TITLE" => "Квесты",
                                "QUEST_LIST" => $this->arResult["REQUEST"]["PROP"]["QUEST_LIST"],
                                //"BASE_PRICE" => $this->arResult["REQUEST"]["PROP"]["BASE_PRICE"],
                                "SET_STATUS_404" => "N",
                                "SHOW_404" => "N",
                                "SORT_BY1" => "ACTIVE_FROM",
                                "SORT_BY2" => "SORT",
                                "SORT_ORDER1" => "DESC",
                                "SORT_ORDER2" => "ASC",
                                'CERTIFICATE_MIN_COST' => (isset($this->arResult['REQUEST']['PROP']['CERTIFICATE_MIN_COST'])) ? $this->arResult['REQUEST']['PROP']['CERTIFICATE_MIN_COST'] : null,
                                'USER_PRICE' => (isset($this->arResult['REQUEST']['PROP']['USER_PRICE'])) ? $this->arResult['REQUEST']['PROP']['USER_PRICE'] : null,
                            )

                        );
                        $res_html = ob_get_contents();
                    ob_end_clean();

                    $this->arResult["RES_HTML"] = $res_html;
                }
            }
        }
    }

    /**
     * @brief Проверка данных на обязательные поля
     * @return массив
     **/
    protected function getPropsRequired()
    {
        $this->arResult["REQUEST"]["PROP"]["ACTION"] = trim($this->arResult["REQUEST"]["PROP"]["ACTION"]);

        $propRequired = array("ACTION");

        foreach($propRequired as $prop_item)
        {
            if(empty($this->arResult["REQUEST"]["PROP"][$prop_item]))
            {
                array_push($this->messageError,Loc::getMessage('CL_NONE_PARAMETR', Array ("#ID#" => $prop_item)));
            }
        }
    }

    /**
     * @brief Обработка ошибок
     * @return массив
     **/
    protected function actionMessage()
    {
        $this->arResult["MESSAGE_ERROR"] = $this->messageError;
        $this->arResult["MESSAGE_SEND"] = $this->messageSend;
    }

    public function myExecuteComponent()
    {
        try{
            $this->checkModules();
            $this->getResult();
            $this->actionMessage();

            return $this->arResult;
        }catch (Exception $e){
            ShowError($e->getMessage());
        }
    }
}

$CLAjax = new CLAjax();
$arResult =  $CLAjax->myExecuteComponent();

unset($arResult["REQUEST"]);

echo json_encode($arResult);
?>
