<?php
namespace Webtu\ExpansionSite;

use \Bitrix\Main;
use \Bitrix\Main\Config\Option;
use \Bitrix\Main\Loader;
use \Bitrix\Main\Localization\Loc;

Loc::loadMessages(__FILE__);

class Handler
{
    public function __construct(){}

    #Получить все настройки модуля
    public static function getOptions()
	{
        $resOptions = array();

        #GEOIP
        $resOptions["GEOIP"]["DATA_SOURCE_IP"] = Option::get("webtu.expansionsite", "tab1_geoip_data_source_ip");
        $resOptions["GEOIP"]["CITY_DEFAULT"] = Option::get("webtu.expansionsite", "tab1_geoip_city_default");
        $resOptions["GEOIP"]["NEAREST_DOT"] = Option::get("webtu.expansionsite", "tab1_geoip_nearest_dot");
        $resOptions["GEOIP"]["IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab1_geoip_iblock_id");
        $resOptions["GEOIP"]["PROPERTY_MAP_CODE"] = Option::get("webtu.expansionsite", "tab1_geoip_propperty_map_code");

        #Квесты
        $resOptions["QUEST"]["CATALOG_IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab2_quest_catalog_iblock_id");
        $resOptions["QUEST"]["OFFERS_IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab2_quest_offers_iblock_id");
        $resOptions["QUEST"]["COUNT_WEEKS"] = Option::get("webtu.expansionsite", "tab2_quest_count_weeks");
        $resOptions["QUEST"]["STATUS"]["ACTIVE"] = Option::get("webtu.expansionsite", "tab2_quest_status_active_value");
        $resOptions["QUEST"]["STATUS"]["DISABLED"] = Option::get("webtu.expansionsite", "tab2_quest_status_disabled_value");
        $resOptions["QUEST"]["STATUS"]["DRAFT"] = Option::get("webtu.expansionsite", "tab2_quest_status_draft_value");
        $resOptions["QUEST"]["INTEGRATION_SYSTEM"]["MIRKVESTOV"] = Option::get("webtu.expansionsite", "tab2_quest_integration_system_mirkvestov_value");
        $resOptions["QUEST"]["INTEGRATION_SYSTEM"]["QUESTCOMPASS"] = Option::get("webtu.expansionsite", "tab2_quest_integration_system_questcompass_value");
        $resOptions["QUEST"]["SEARCH_TEAM_BUTTON"] = Option::get("webtu.expansionsite", "tab2_search_team_button");
        $resOptions["TEAM"]["IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab2_team_iblock_id");
        $resOptions["TEAM"]["NOVICE"] = Option::get("webtu.expansionsite", "tab2_team_novice");
        $resOptions["TEAM"]["SPECIALIST"] = Option::get("webtu.expansionsite", "tab2_team_specialist");
        $resOptions["TEAM"]["EXPERT"] = Option::get("webtu.expansionsite", "tab2_team_expert");
        $resOptions["TEAM"]["MAX_COUNT_MEMBERS"] = Option::get("webtu.expansionsite", "tab2_team_max_count_members");
        $resOptions["TEAM"]["MAX_COUNT"] = Option::get("webtu.expansionsite", "tab2_team_max_count");
        $resOptions["PART"]["IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab2_part_iblock_id");
        $resOptions["CRITIQUE"]["IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab2_critique_iblock_id");
        $resOptions["REVIEW"]["IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab2_review_iblock_id");
        $resOptions["REVIEW"]["MODERATION"] = Option::get("webtu.expansionsite", "tab2_review_moderation");
        $resOptions["PROMO"]["IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab2_promo_iblock_id");
        $resOptions["CERTIFICATE"]["IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab2_certificate_iblock_id");
        $resOptions["GIFT_LINK"] = Option::get("webtu.expansionsite", "tab2_gift_link");
        


        #Пользователи
        $resOptions["USERS"]["ACCESS"]["LEVEL_1"] =  Option::get("webtu.expansionsite", "tab3_users_access_1");
        $resOptions["USERS"]["ACCESS"]["LEVEL_2"] =  Option::get("webtu.expansionsite", "tab3_users_access_2");
        #Временный пароль
        $resOptions["USERS"]["PASSWORD"] =  Option::get("webtu.expansionsite", "tab3_users_password");
        #Чат с администратором сайта
        $resOptions["USERS"]["ADMIN_MESSAGE"] =  Option::get("webtu.expansionsite", "tab3_admin_message");
        #Отсроченный платеж
        $resOptions["DEFERRED_PAYMENT"]["IBLOCK_ID"] =  Option::get("webtu.expansionsite", "tab3_deferred_payment_iblock_id");
        $resOptions["DEFERRED_PAYMENT"]["COUNT_DAYS"] =  Option::get("webtu.expansionsite", "tab3_deferred_payment_count_days");
        $resOptions["DEFERRED_PAYMENT"]["SUM"] =  Option::get("webtu.expansionsite", "tab3_deferred_payment_sum");
        $resOptions["DEFERRED_PAYMENT"]["SUM_NOT_AVAILABLE"] =  Option::get("webtu.expansionsite", "tab3_deferred_payment_sum_not_available");

        #Свойства
        $resOptions["PROPS"]["TYPE_USER"] = array(
            "ID" => Option::get("webtu.expansionsite", "tab4_props_type_user_id"),
            "CODE" => Option::get("webtu.expansionsite", "tab4_props_type_user_code"),
        );
        $resOptions["PROPS"]["CASHBACK"] = array(
            "ID" => Option::get("webtu.expansionsite", "tab4_props_cashback_id"),
            "CODE" => Option::get("webtu.expansionsite", "tab4_props_cashback_code"),
        );
        $resOptions["PROPS"]["QUEST_RAITING"] = array(
            "ID" => Option::get("webtu.expansionsite", "tab4_props_quest_raiting_id"),
            "CODE" => Option::get("webtu.expansionsite", "tab4_props_quest_raiting_code"),
        );
        $resOptions["PROPS"]["STATUS"] = array(
            "ID" => Option::get("webtu.expansionsite", "tab4_props_quest_status_id"),
            "CODE" => Option::get("webtu.expansionsite", "tab4_props_quest_status_code"),
        );
        $resOptions["PROPS"]["INTEGRATION_SYSTEM"] = array(
            "ID" => Option::get("webtu.expansionsite", "tab4_props_quest_integration_system_id"),
            "CODE" => Option::get("webtu.expansionsite", "tab4_props_quest_integration_system_code"),
        );

        #Бонусы
        $resOptions["BONUS"]["TYPE_USER_VALUE"] = Option::get("webtu.expansionsite", "tab5_bonus_type_user_value");
        $resOptions["BONUS"]["PARTNER_HASH"] = Option::get("webtu.expansionsite", "tab5_bonus_partner_hash");
        $resOptions["BONUS"]["CURRENCY"] = Option::get("webtu.expansionsite", "tab5_bonus_currency");
        $resOptions["BONUS"]["ENROL_USERS_ALL"] = Option::get("webtu.expansionsite", "tab5_bonus_enrol_users_all");
        //$resOptions["BONUS"]["DEDUCT"] = Option::get("webtu.expansionsite", "tab5_bonus_deduct");
        //$resOptions["BONUS"]["DEDUCT_DAY"] = Option::get("webtu.expansionsite", "tab5_bonus_deduct_day");
        $resOptions["BONUS"]["CASHBACK"] = Option::get("webtu.expansionsite", "tab5_bonus_cashback");
        $resOptions["BONUS"]["CASHBACK_MIN"] = Option::get("webtu.expansionsite", "tab5_bonus_cashback_min");
        $resOptions["BONUS"]["CASHBACK_MAX"] = Option::get("webtu.expansionsite", "tab5_bonus_cashback_max");
        $resOptions["BONUS"]["CASHBACK_TOP15"] = Option::get("webtu.expansionsite", "tab5_bonus_cashback_top15");
        $resOptions["BONUS"]["CASHBACK_TOP30"] = Option::get("webtu.expansionsite", "tab5_bonus_cashback_top30");
        $resOptions["BONUS"]["CASHBACK_TOP40"] = Option::get("webtu.expansionsite", "tab5_bonus_cashback_top40");
        $resOptions["BONUS"]["CASHBACK_TOP50"] = Option::get("webtu.expansionsite", "tab5_bonus_cashback_top50");
        $resOptions["BONUS"]["CASHBACK_TOP70"] = Option::get("webtu.expansionsite", "tab5_bonus_cashback_top70");
        $resOptions["BONUS"]["EVENT_REGISTER"] = Option::get("webtu.expansionsite", "tab5_bonus_event_register");
        $resOptions["BONUS"]["EVENT_REGISTER_SUM"] = Option::get("webtu.expansionsite", "tab5_bonus_event_register_sum");
        $resOptions["BONUS"]["EVENT_REGISTER_SUM_2"] = Option::get("webtu.expansionsite", "tab5_bonus_event_register_sum_2");
        $resOptions["BONUS"]["EVENT_REGISTER_INVITE"] = Option::get("webtu.expansionsite", "tab5_bonus_event_register_invite");
        $resOptions["BONUS"]["EVENT_REGISTER_INVITE_SUM"] = Option::get("webtu.expansionsite", "tab5_bonus_event_register_invite_sum");
        $resOptions["BONUS"]["EVENT_REVIEW_ADD"] = Option::get("webtu.expansionsite", "tab5_bonus_event_review_add");
        $resOptions["BONUS"]["EVENT_REVIEW_ADD_SUM"] = Option::get("webtu.expansionsite", "tab5_bonus_event_review_add_sum");
        $resOptions["BONUS"]["EVENT_TEAM_ADD"] = Option::get("webtu.expansionsite", "tab5_bonus_event_team_add");
        $resOptions["BONUS"]["EVENT_TEAM_ADD_SUM"] = Option::get("webtu.expansionsite", "tab5_bonus_event_team_add_sum");
        $resOptions["BONUS"]["EVENT_SUNC_SOCIAL_NETWORK"] = Option::get("webtu.expansionsite", "tab5_bonus_event_sunc_social_network");
        $resOptions["BONUS"]["EVENT_SUNC_SOCIAL_NETWORK_SUM"] = Option::get("webtu.expansionsite", "tab5_bonus_event_sunc_social_network_sum");

        #Бронирование
        $resOptions["ORDER"]["AUTH_USER_ID"] =  Option::get("webtu.expansionsite", "tab6_order_auth_user_id");
        $resOptions["ORDER"]["CANCEL_IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab6_order_cancel_iblock_id");
        $resOptions["ORDER"]["QUEST_COMPLETED_IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab6_order_quest_completed_iblock_id");
        $resOptions["ORDER"]["PERCENT_DEBIT"] = Option::get("webtu.expansionsite", "tab6_order_percent_debit");
        $resOptions["ORDER"]["DIFF"]["LEVEL_1"] = Option::get("webtu.expansionsite", "tab6_order_level_diff_1");
        $resOptions["ORDER"]["DIFF"]["LEVEL_2"] = Option::get("webtu.expansionsite", "tab6_order_level_diff_2");
        $resOptions["ORDER"]["DIFF"]["LEVEL_3"] = Option::get("webtu.expansionsite", "tab6_order_level_diff_3");
        $resOptions["ORDER"]["DIFF"]["LEVEL_4"] = Option::get("webtu.expansionsite", "tab6_order_level_diff_4");
        $resOptions["ORDER"]["TIME_PROMPT"] = Option::get("webtu.expansionsite", "tab6_order_time_prompt");

        #МетаТеги
        $resOptions["META_TAG"]["LOGO"] = Option::get("webtu.expansionsite", "tab7_metatag_logo");

        #МетаДанные для SEO
        $resOptions["META_SEO_FILTER"]["IBLOCK_ID"] = Option::get("webtu.expansionsite", "tab8_seo_filter_iblock_id");
        $resOptions["META_SEO_FILTER"]["PAGE"] = Option::get("webtu.expansionsite", "tab8_seo_filter_page");
        $resOptions["META_SEO_FILTER"]["TAG_COUNT"] = Option::get("webtu.expansionsite", "tab8_seo_tag_count");

        #Оплата
        $resOptions["PAYMENT"]["CERTIFICATE_PAYMENT_ID"] = Option::get("webtu.expansionsite", "tab9_certificate_payment");
        $resOptions["PAYMENT"]["BOOKING_PAYMENTS_ID"] = Option::get("webtu.expansionsite", "tab9_booking_payment");
        #Платрон (данные для выплат партнерам)
        $resOptions["PLATRON"]["TRANSFER"] = Option::get("webtu.expansionsite", "tab9_platron_transfer");
        $resOptions["PLATRON"]["MERCHANT_ID"] = Option::get("webtu.expansionsite", "tab9_platron_merchant_id");
        $resOptions["PLATRON"]["SECRET_KEY"] = Option::get("webtu.expansionsite", "tab9_platron_secret_key");
        $resOptions["PLATRON"]["CONTRACT_ID"] = Option::get("webtu.expansionsite", "tab9_platron_contract_id");
        $resOptions["PLATRON"]["PERCENT_PAY"] = Option::get("webtu.expansionsite", "tab9_platron_percent_value");
        $resOptions["PLATRON"]["RUB_REMITTANCE"] = Option::get("webtu.expansionsite", "tab9_platron_rub_value");
        $resOptions["PLATRON"]["COMMISSION_PAYMENT"] = Option::get("webtu.expansionsite", "tab9_commission_payment");

        #Награждения
        $resOptions["AWARDS"]["WINTER_SEASON"]["DATE_FROM"] = Option::get("webtu.expansionsite", "tab10_awards_winter_season_date_from");
        $resOptions["AWARDS"]["WINTER_SEASON"]["DATE_TO"] = Option::get("webtu.expansionsite", "tab10_awards_winter_season_date_to");
        $resOptions["AWARDS"]["WINTER_SEASON"]["ELEMENTS_ID"]["TOTAL_RATING"] = Option::get("webtu.expansionsite", "tab10_awards_winter_season_total_rating");
        $resOptions["AWARDS"]["WINTER_SEASON"]["ELEMENTS_ID"]["RATING_POINTS_SEASON"] = Option::get("webtu.expansionsite", "tab10_awards_winter_season_rating_points_season");
        $resOptions["AWARDS"]["WINTER_SEASON"]["ELEMENTS_ID"]["RATING_QUALITY_GAME"] = Option::get("webtu.expansionsite", "tab10_awards_winter_season_rating_quality_game");

        $resOptions["AWARDS"]["SPRING_SEASON"]["DATE_FROM"] = Option::get("webtu.expansionsite", "tab10_awards_spring_season_date_from");
        $resOptions["AWARDS"]["SPRING_SEASON"]["DATE_TO"] = Option::get("webtu.expansionsite", "tab10_awards_spring_season_date_to");
        $resOptions["AWARDS"]["SPRING_SEASON"]["ELEMENTS_ID"]["TOTAL_RATING"] = Option::get("webtu.expansionsite", "tab10_awards_spring_season_total_rating");
        $resOptions["AWARDS"]["SPRING_SEASON"]["ELEMENTS_ID"]["RATING_POINTS_SEASON"] = Option::get("webtu.expansionsite", "tab10_awards_spring_season_rating_points_season");
        $resOptions["AWARDS"]["SPRING_SEASON"]["ELEMENTS_ID"]["RATING_QUALITY_GAME"] = Option::get("webtu.expansionsite", "tab10_awards_spring_season_rating_quality_game");

        $resOptions["AWARDS"]["SUMMER_SEASON"]["DATE_FROM"] = Option::get("webtu.expansionsite", "tab10_awards_summer_season_date_from");
        $resOptions["AWARDS"]["SUMMER_SEASON"]["DATE_TO"] = Option::get("webtu.expansionsite", "tab10_awards_summer_season_date_to");
        $resOptions["AWARDS"]["SUMMER_SEASON"]["ELEMENTS_ID"]["TOTAL_RATING"] = Option::get("webtu.expansionsite", "tab10_awards_summer_season_total_rating");
        $resOptions["AWARDS"]["SUMMER_SEASON"]["ELEMENTS_ID"]["RATING_POINTS_SEASON"] = Option::get("webtu.expansionsite", "tab10_awards_summer_season_rating_points_season");
        $resOptions["AWARDS"]["SUMMER_SEASON"]["ELEMENTS_ID"]["RATING_QUALITY_GAME"] = Option::get("webtu.expansionsite", "tab10_awards_summer_season_rating_quality_game");

        $resOptions["AWARDS"]["AUTUMN_SEASON"]["DATE_FROM"] = Option::get("webtu.expansionsite", "tab10_awards_autumn_season_date_from");
        $resOptions["AWARDS"]["AUTUMN_SEASON"]["DATE_TO"] = Option::get("webtu.expansionsite", "tab10_awards_autumn_season_date_to");
        $resOptions["AWARDS"]["AUTUMN_SEASON"]["ELEMENTS_ID"]["TOTAL_RATING"] = Option::get("webtu.expansionsite", "tab10_awards_autumn_season_total_rating");
        $resOptions["AWARDS"]["AUTUMN_SEASON"]["ELEMENTS_ID"]["RATING_POINTS_SEASON"] = Option::get("webtu.expansionsite", "tab10_awards_autumn_season_rating_points_season");
        $resOptions["AWARDS"]["AUTUMN_SEASON"]["ELEMENTS_ID"]["RATING_QUALITY_GAME"] = Option::get("webtu.expansionsite", "tab10_awards_autumn_season_rating_quality_game");

        return $resOptions;
	}

    public static function isUserAgentPageSpeed()
    {

        $pageSpeedUserAgent = 'Chrome-Lighthouse';
        
        if (!isset($_SERVER['HTTP_USER_AGENT']) || stripos($_SERVER['HTTP_USER_AGENT'], $pageSpeedUserAgent) === false)
        {
            return true;
        }

        return false;
    }

	/*
	 * изменяет окончание слов в зависимости от числа
	 */
	public static function getNumEnding( $number = 0, $endingArray = array() )
	{
		$number = $number % 100;

		if ($number>=11 && $number<=19) {
			$ending=$endingArray[2];
		} else 	{
			$i = $number % 10;
			switch ($i)
			{
				case (1): $ending = $endingArray[0]; break;
				case (2):	case (3):	case (4): $ending = $endingArray[1]; break;
				default: $ending=$endingArray[2];
			}
		}

		return $ending;
	}

    /**
     * @brief Установим значение сессии
     * @param $params - Маасив параметров
     **/
    public function setCitySession($params)
    {
        #запишем название города (позже пригодится где-нибудь)
        $_SESSION["CITY_INFO"]["sCurCityName"] = $params["NEAREST_DOT"]["NAME"];
        #это самое главное, id нам нужен будет для фильтров
        $_SESSION["CITY_INFO"]["iCurCityID"] = $params["NEAREST_DOT"]["ID"];
        #Телефон КвестБатл для данного города
        $_SESSION["CITY_INFO"]["PHONE"] = $params["NEAREST_DOT"]["PHONE"];
        #Соц сети КвестБатл для данного города
        $_SESSION["CITY_INFO"]["SOC_LINKS"] = $params["NEAREST_DOT"]["SOC_LINKS"];
        #установим фильтр, который будем повсеместно применять
        $_SESSION["CITY_INFO"]['arrFilter'] = array('CITY'=>$params["NEAREST_DOT"]["ID"]);

        #Фильтр по городам
        if($_SESSION["CITY_INFO"]["arrFilter"])
        {
            foreach( $_SESSION["CITY_INFO"]["arrFilter"] as $keyFilter=>$itemFilter )
            {
                $_SESSION["CITY_INFO"]["ARRAY_MARGE"]["PROPERTY"][$keyFilter] = $itemFilter;
            }
        }

        $_SESSION["CITY_INFO"]["GEOIP"]["CURRENT"] = $params["ITEM"];
        $_SESSION["CITY_INFO"]["GEOIP"]["NEAREST_DOT"] = $params["NEAREST_DOT"];

        //LocalRedirect( "https://".$_SESSION["CITY_INFO"]["GEOIP"]["NEAREST_DOT"]["CODE"].".".$_SESSION["CITY_INFO"]["domain_main"] );
    }

    /**
     * @brief Установим доступ пользователя
     **/
    public function setAccessUser()
    {
        global $USER;
        global $USER_FIELD_MANAGER;

        if($USER->IsAuthorized())
        {
            #Получить все настройки модуля
            $arParams = Handler::getOptions();

            #данные пользователя
            $userId = $USER->GetID();
            $resultUser = Main\UserTable::getById($userId);
            $arUser = $resultUser->fetch();

            #Пользовательские свойства
            $arUserFields = $USER_FIELD_MANAGER->GetUserFields("USER", $arUser["ID"], LANGUAGE_ID);
            $propsTypeUser = $arUserFields[$arParams["PROPS"]["TYPE_USER"]["CODE"]];

            $propsTypeUser["USER_TYPE"]["CLASS_NAME"] = "\\".$propsTypeUser["USER_TYPE"]["CLASS_NAME"];
            if (is_callable(array($propsTypeUser["USER_TYPE"]["CLASS_NAME"], 'getlist')))
            {
                $rsEnum = call_user_func_array(
                    array($propsTypeUser["USER_TYPE"]["CLASS_NAME"], "getlist"),
                    array($propsTypeUser,)
                );
                while($arEnum = $rsEnum->GetNext())
                {
                    $propsTypeUser["ENUM_LIST"][$arEnum["ID"]] = $arEnum["VALUE"];
                }
            }

            #Данные
            $acessUser = array(
                "ID" => $arUser["ID"],
                "LOGIN" => $arUser["LOGIN"],
                "NAME" => $arUser["NAME"],
                "TYPE_USER" => array(
                    "ACCESS_LEVEL" => array(),
                    "ACCESS_MAX_LEVEL" => "0",
                    "PROPS_NAME" =>  $propsTypeUser["EDIT_FORM_LABEL"],
                    "PROPS_VALUE" => $propsTypeUser["VALUE"],
                    "PROPS_VALUE_NAME" => $propsTypeUser["ENUM_LIST"][$propsTypeUser["VALUE"]],
                ),
            );

            #Установка доступов
            if( $propsTypeUser["VALUE"] == $arParams["USERS"]["ACCESS"]["LEVEL_1"] )
            {
                array_push($acessUser["TYPE_USER"]["ACCESS_LEVEL"], "1");
            }

            if( $propsTypeUser["VALUE"] == $arParams["USERS"]["ACCESS"]["LEVEL_2"] )
            {
                array_push($acessUser["TYPE_USER"]["ACCESS_LEVEL"], "2");
            }

            $acessUser["TYPE_USER"]["ACCESS_MAX_LEVEL"] = max($acessUser["TYPE_USER"]["ACCESS_LEVEL"]);

            $_SESSION["USER_ACCESS"] = $acessUser;

        }#end $USER->IsAuthorized()
    }

    /**
     * @brief Удалим доступ пользователя
     **/
    public function onSetAccessUser()
    {
        #Установим права текущего пользователя
        global $USER;

        if(!$USER->IsAuthorized())
        {
            unset( $_SESSION["USER_ACCESS"] );
        }
    }
}