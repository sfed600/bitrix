<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true)die();
/** @var array $arParams */
/** @var array $arResult */
/** @global CMain $APPLICATION */
/** @global CUser $USER */
/** @global CDatabase $DB */
/** @var CBitrixComponentTemplate $this */
/** @var string $templateName */
/** @var string $templateFile */
/** @var string $templateFolder */
/** @var string $componentPath */
/** @var CBitrixComponent $component */
$this->setFrameMode(true);

use \Webtu\ExpansionSite\Handler;

echo '<div class="page-container">';
    echo '<h2 class="page-title page-title--h2">Квесты, которые можно забронировать</h2>';

    if ( count($arResult["ITEMS"]) > 0 )
    {
        /** --- Ajax --- **/
        if (!empty($arResult['NAV_RESULT'])) {
            $navParams = array(
                'NavPageCount' => $arResult['NAV_RESULT']->NavPageCount,
                'NavPageNomer' => $arResult['NAV_RESULT']->NavPageNomer,
                'NavNum' => $arResult['NAV_RESULT']->NavNum
            );
        } else {
            $navParams = array(
                'NavPageCount' => 1,
                'NavPageNomer' => 1,
                'NavNum' => $this->randString()
            );
        }

        $showTopPager = false;
        $showBottomPager = false;
        $showLazyLoad = false;

        if ($arParams['NEWS_COUNT'] > 0 && $navParams['NavPageCount'] > 1) {
            $showTopPager = $arParams['DISPLAY_TOP_PAGER'];
            $showBottomPager = $arParams['DISPLAY_BOTTOM_PAGER'];
            $showLazyLoad = $arParams['LAZY_LOAD'] === 'Y' && $navParams['NavPageNomer'] != $navParams['NavPageCount'];
        }

        $obName = 'ob' . preg_replace('/[^a-zA-Z0-9_]/', 'x', $this->GetEditAreaId($navParams['NavNum']));
        $containerName = 'container-' . $navParams['NavNum'];
        /** --- end Ajax --- **/
        ?>

        <div class="quest-items quest-items--5x" data-entity="<?= $containerName ?>">
        <!-- items-container -->
            <?
            foreach($arResult["ITEMS"] as $key => $arItem)
            {
                $this->AddEditAction($arItem['ID'], $arItem['EDIT_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_EDIT"));
                $this->AddDeleteAction($arItem['ID'], $arItem['DELETE_LINK'], CIBlock::GetArrayByID($arItem["IBLOCK_ID"], "ELEMENT_DELETE"), array("CONFIRM" => GetMessage('CT_BNL_ELEMENT_DELETE_CONFIRM')));

                $mainId = $this->GetEditAreaId($arItem['ID']);

                ?>
                <div class="quest-item quest-item--vertical quest-item--no-actions" id="<?=$mainId;?>" data-entity="items-row">

                    <div class="quest-item__top">
                        <? if (!empty($arItem['PROPERTIES']['NEW']['VALUE'])) { ?>
                            <div class="quest-item__new">new</div>
                        <? } ?>
                        <?
                        #Возрастное ограничение
                        if ( strlen($arItem["PROPERTIES"]["AGE"]["VALUE"]) > 0 )
                        {
                            echo '<div class="quest-item__age quest-age">'.$arItem["PROPERTIES"]["AGE"]["VALUE"].'</div>';
                        }

                        #Превью картинка
                        if ($arItem["ACTIVE"] == "Y")
                        {
                            echo '<a href="'.$arItem["DETAIL_PAGE_URL"].'" class="quest-item__image">';
                                if ( strlen($arItem["PREVIEW_PICTURE"]["SRC"]) > 0)
                                {
                                    echo '<img src="'.$arItem["PREVIEW_PICTURE"]["SRC"].'" alt="'.$arItem["PREVIEW_PICTURE"]["ALT"].'" title="'.$arItem["PREVIEW_PICTURE"]["TITLE"].'">';
                                }
                                else
                                {
                                    echo '<img src="/local/templates/.default/img/quest-image-placeholder.jpg" >';
                                }
                            echo '</a>';
                        }
                        else
                        {
                            echo '<a class="quest-item__image">';
                                if ( strlen($arItem["PREVIEW_PICTURE"]["SRC"]) > 0)
                                {
                                    echo '<img src="'.$arItem["PREVIEW_PICTURE"]["SRC"].'" alt="'.$arItem["PREVIEW_PICTURE"]["ALT"].'" title="'.$arItem["PREVIEW_PICTURE"]["TITLE"].'">';
                                }
                                else
                                {
                                    echo '<img src="/local/templates/.default/img/quest-image-placeholder.jpg">';
                                }
                            echo '</a>';
                        }
                        ?>

                        <div class="quest-item__overlay">
                            <div class="quest-item__aligner aligner">
                                <?
                                #Сложность
                                if ( empty($arItem["PROPERTIES"]["LEVEL_DIFF"]["VALUE_XML_ID"]) ) $arItem["PROPERTIES"]["LEVEL_DIFF"]["VALUE_XML_ID"] = 0;

                                echo '<div class="quest-item__scale quest-stat quest-stat--difficulty-1">';
                                    echo '<div class="aligner">';
                                        echo '<div class="quest-stat__caption">Сложность</div>';
                                        echo '<div class="clearfix">';
                                            ?>
                                            <div class="quest-stat__segment <?=($arItem["PROPERTIES"]["LEVEL_DIFF"]["VALUE_XML_ID"] >= 1) ? 'is-full' : ''?>"></div>
                                            <div class="quest-stat__segment <?=($arItem["PROPERTIES"]["LEVEL_DIFF"]["VALUE_XML_ID"] >= 2) ? 'is-full' : ''?>"></div>
                                            <div class="quest-stat__segment <?=($arItem["PROPERTIES"]["LEVEL_DIFF"]["VALUE_XML_ID"] >= 3) ? 'is-full' : ''?>"></div>
                                            <div class="quest-stat__segment <?=($arItem["PROPERTIES"]["LEVEL_DIFF"]["VALUE_XML_ID"] == 4) ? 'is-full' : ''?>"></div>
                                            <?
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';

                                #Сртрах
                                if ( empty($arItem["PROPERTIES"]["LEVEL_FEAR"]["VALUE_XML_ID"]) ) $arItem["PROPERTIES"]["LEVEL_FEAR"]["VALUE_XML_ID"] = 0;

                                echo '<div class="quest-item__scale quest-stat quest-stat--horror-1">';
                                    echo '<div class="aligner">';
                                        echo '<div class="quest-stat__caption">Страх</div>';
                                        echo '<div class="clearfix">';
                                            ?>
                                            <div class="quest-stat__segment <?=($arItem["PROPERTIES"]["LEVEL_FEAR"]["VALUE_XML_ID"] >= 1) ? 'is-full' : ''?>"></div>
                                            <div class="quest-stat__segment <?=($arItem["PROPERTIES"]["LEVEL_FEAR"]["VALUE_XML_ID"] >= 2) ? 'is-full' : ''?>"></div>
                                            <div class="quest-stat__segment <?=($arItem["PROPERTIES"]["LEVEL_FEAR"]["VALUE_XML_ID"] >= 3) ? 'is-full' : ''?>"></div>
                                            <div class="quest-stat__segment <?=($arItem["PROPERTIES"]["LEVEL_FEAR"]["VALUE_XML_ID"] == 4) ? 'is-full' : ''?>"></div>
                                            <?
                                        echo '</div>';
                                    echo '</div>';
                                echo '</div>';
                                ?>
                            </div>
                        </div>

                    </div>

                    <div class="quest-item__mid">

                        <div class="clearfix">
                            <div class="quest-item__type"><?=$arItem["PROPERTIES"]["QUEST_TYPE"]["VALUE"]?></div>
                            <?
                            if ( !empty($arItem["PROPERTIES"]["RAITING"]["VALUE"]) )
                            {
                                echo '<div class="quest-item__rating quest-stat quest-stat--small quest-stat--rating">'.$arItem["PROPERTIES"]["RAITING"]["VALUE"].'</div>';
                            }
                            ?>
                        </div>

                        <h3 class="quest-item__title page-title">
                            <?
                            if ($arItem["ACTIVE"] == "Y")
                            {
                                echo '<a href="'.$arItem["DETAIL_PAGE_URL"].'">'.$arItem["NAME"].'</a>';
                            }
                            else
                            {
                                echo '<a>'.$arItem["NAME"].'</a>';
                            }
                            ?>
                        </h3>

                        <div class="clearfix">
                            <? if (!empty($arItem['PROPERTIES']['DURACTION']['VALUE'])) { ?>
                                <div class="quest-item__stat quest-stat quest-stat--small quest-stat--time-1">
                                    <?= $arItem['PROPERTIES']['DURACTION']['VALUE']; ?> минут
                                </div>
                            <? } ?>
                            <? if (!empty($arItem['PROPERTIES']['PLAYERS']['VALUE'])) { ?>
                                <? if (count($arItem['PROPERTIES']['PLAYERS']['VALUE']) > 1) { ?>
                                    <?
                                    if ( intval($arItem['PROPERTIES']['ADDITIONAL_PLAYERS_COUNT']['VALUE']) > 0 )
                                    {
                                        $players_max = end($arItem['PROPERTIES']['PLAYERS']['VALUE']) + intval($arItem['PROPERTIES']['ADDITIONAL_PLAYERS_COUNT']['VALUE']);
                                    }
                                    else
                                    {
                                        $players_max = end($arItem['PROPERTIES']['PLAYERS']['VALUE']);
                                    }

                                    ?>
                                    <div class="quest-item__stat quest-stat quest-stat--small quest-stat--size-1">
                                        <?= reset($arItem['PROPERTIES']['PLAYERS']['VALUE']) . '-' .$players_max ; ?>
                                        <?= Handler::getNumEnding($players_max, array('игрок', 'игрока', 'игроков')); ?>
                                    </div>
                                <? } else { ?>
                                    <div class="quest-item__stat quest-stat quest-stat--small quest-stat--size-1"><?= $arItem['PROPERTIES']['PLAYERS']['VALUE'][0]; ?>
                                        <?= Handler::getNumEnding($arItem['PROPERTIES']['PLAYERS']['VALUE'][0], array('игрок', 'игрока', 'игроков')); ?>
                                    </div>
                                <? } ?>
                            <? } ?>
                        </div>

                        <? if (!empty($arItem['PREVIEW_TEXT'])) { ?>
                            <p class="quest-item__brief"><?= $arItem['PREVIEW_TEXT']; ?></p>
                        <? } ?>

                    </div>
                </div>
            <? } ?>
        <!-- items-container -->
        </div>

        <? if ($showLazyLoad) { ?>
            <div class="show-more" data-use="show-more-<?= $navParams['NavNum']; ?>">
                <a href="javascript:void(0);" class="button button--large button--yellow">Показать ещё</a>
            </div>
        <? } ?>

        <?
        /** --- Ajax --- **/
        $signer = new \Bitrix\Main\Security\Sign\Signer;
        $signedTemplate = $signer->sign($templateName, 'certificate.quest.list');
        $signedParams = $signer->sign(base64_encode(serialize($arParams)), 'certificate.quest.list');
        ?>
        <script>
            BX.message({
                BTN_MESSAGE_LAZY_LOAD_WAITER: '<?=GetMessageJS('BTN_MESSAGE_LAZY_LOAD_WAITER')?>',
            });
            var <?=$obName?> = new JCIblockElementListLight2({
                siteId: '<?=CUtil::JSEscape(SITE_ID)?>',
                componentPath: '<?=CUtil::JSEscape($componentPath)?>',
                templateFolder: '<?=CUtil::JSEscape($templateFolder)?>',
                navParams:      <?=CUtil::PhpToJSObject($navParams)?>,
                template: '<?=CUtil::JSEscape($signedTemplate)?>',
                parameters: '<?=CUtil::JSEscape($signedParams)?>',
                container: '<?=$containerName?>',
                lazyLoad: !!'<?=$showLazyLoad?>',
            });
        </script>
        <? /** --- end Ajax --- **/?>
    <? } #end if($arResult["ITEMS"])
    else
    {
        echo '<h2 class="page-title page-title--h2">Список квестов пуст</h2>';
    }
echo '</div>';
